# Bingo Start 核心库

BingoStart 是基于laravel 11 为核心的前后端分离的核心库，用于后台模块化开发。

## 安装

```shell
composer require bingostart/core
```
