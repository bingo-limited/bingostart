<?php

use Bingo\Core\Input\Format;
use Bingo\Exceptions\BingoException;
use Bingo\Exceptions\BizException;
use Bingo\Exceptions\FailedException;
use Illuminate\Auth\AuthenticationException;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Validation\ValidationException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

return [
    /*
    |--------------------------------------------------------------------------
    | Set the http status code when the response fails
    |--------------------------------------------------------------------------
    |
    | the reference options are false, 200, 500
    |
    | false, stricter http status codes such as 404, 401, 403, 500, etc. will be returned
    | 200, All failed responses will also return a 200 status code
    | 500, All failed responses return a 500 status code
    */

    'error_code' => false,

    // lang/zh_CN/enums.php
    'locale' => 'enums', // enums

    //  You can set some attributes (eg:code/message/header/options) for the exception, and it will override the default attributes of the exception
    'exception' => [
        ValidationException::class => [
            'code' => 422,
        ],
        AuthenticationException::class => [
        ],
        NotFoundHttpException::class => [
            'message' => '',
        ],
        ModelNotFoundException::class => [
            'message' => '',
        ],
        BizException::class => [
        ],
        BingoException::class => [
        ],
        FailedException::class => [
        ],
    ],

    // Any key that returns data exists supports custom aliases and display.
    'format' => [
        'class' => Format::class,
        'config' => [
            // key => config
            'status' => ['alias' => 'status', 'show' => true],
            'code' => ['alias' => 'code', 'show' => true],
            'message' => ['alias' => 'message', 'show' => true],
            'error' => ['alias' => 'error', 'show' => true],
            'data' => ['alias' => 'data', 'show' => true],
            'data.data' => ['alias' => 'data.data', 'show' => true], // rows/items/list
        ],
    ],
];
