{!! \Bingo\BingoStart::js('asset/vendor/echarts/echarts.all.js') !!}
{!! \Bingo\BingoStart::script("
var ele = document.getElementById(".\Bingo\Core\Util\SerializeUtil::jsonEncode($id).");
var chart = echarts.init(ele);
chart.setOption(".\Bingo\Core\Util\SerializeUtil::jsonEncode(empty($option)?new \stdClass():$option).");
MS.ui.onResize( ele, chart.resize );
$(ele).data('chart',chart);
") !!}
<div id="{{$id}}" style="height:{{empty($height)?300:$height}}px;"></div>
