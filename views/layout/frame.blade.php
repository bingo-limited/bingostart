<!DOCTYPE html>
<html @yield('htmlProperties','') lang="en">
<head>
    @section('headPrepend')@show
    <meta charset="UTF-8">
    <link rel="shortcut icon" href="@yield('pageFavIco','')"/>
    <meta name="viewport"
          content="width=device-width, initial-scale=1.0,minimum-scale=0.5, user-scalable=no">
    <title>@yield('pageTitle','')</title>
    <meta name="keywords" content="@yield('pageKeywords','')">
    <meta name="description" content="@yield('pageDescription','')">

    <link rel="stylesheet" href="@asset('asset/vendor/utils/bootstrap.min.css')">
    <link rel="stylesheet" href=".@asset('asset/theme/iconfont/iconfont.css')">

    <link rel="stylesheet" href="@asset('asset/theme/global.css')">
    <link rel="stylesheet" href="@asset('asset/theme/headerFooter/header.css')">
    <link rel="stylesheet" href="@asset('asset/theme/headerFooter/footer.css')">

    <script>window.__msCDN = "{{\Bingo\Core\Assets\AssetsUtil::cdn()}}";
        window.__msRoot = "{{bingostart_web_url()}}";</script>

    <script src="@asset('asset/vendor/utils/jquery.js')"></script>
    {!! \Bingo\BingoStart::js() !!}
    {!! \Bingo\BingoStart::css() !!}
    {!! \Bingo\BingoStart::style() !!}

    @section('headAppend')@show
</head>

<body @yield('bodyProperties','')>
@section('body')@show
{!! \Bingo\BingoStart::script() !!}
@section('bodyAppend')@show
</body>
</html>
