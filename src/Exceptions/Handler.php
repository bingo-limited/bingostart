<?php

namespace Bingo\Exceptions;

use Illuminate\Auth\AuthenticationException;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Illuminate\Http\JsonResponse;
use Illuminate\Validation\ValidationException;
use Illuminate\View\View;
use Symfony\Component\HttpFoundation\Response;
use Throwable;

class Handler extends ExceptionHandler
{
    /**
     * A list of exception types with their corresponding custom log levels.
     *
     * @var array<class-string<\Throwable>, \Psr\Log\LogLevel::*>
     */
    protected $levels = [
        //
    ];

    /**
     * A list of the exception types that are not reported.
     *
     * @var array<int, class-string<\Throwable>>
     */
    protected $dontReport = [
        //
    ];

    /**
     * A list of the inputs that are never flashed to the session on validation exceptions.
     *
     * @var array<int, string>
     */
    protected $dontFlash = [
        'current_password',
        'password',
        'password_confirmation',
    ];

    /**
     * Register the exception handling callbacks for the application.
     *
     * @return void
     */
    public function register(): void
    {
        $this->reportable(function (Throwable $e) {
            // 自定义异常报告逻辑
        });
    }

    /**
     * Render an exception into an HTTP response.
     *
     * @param \Illuminate\Http\Request $request
     * @param \Throwable $e
     * @return \Symfony\Component\HttpFoundation\Response
     * @throws \Throwable
     */
    public function render($request, Throwable $e): JsonResponse|Response
    {
        $message = $e->getMessage();
        if ($e instanceof BizException) {
            $exceptionParam = $e->param;
            if (!empty($exceptionParam)) {
                $statusCode = 200;
                if (empty($exceptionParam['view']) && !empty($exceptionParam['statusCode']) && in_array($exceptionParam['statusCode'], [403, 404, 500])) {
                    $exceptionParam['view'] = 'bingo::core.msg.' . $exceptionParam['statusCode'];
                    $statusCode = $exceptionParam['statusCode'];
                }
                if (!empty($exceptionParam['view'])) {
                    if (!isset($exceptionParam['viewData'])) {
                        $exceptionParam['viewData'] = [];
                    }
                    $ret = view($exceptionParam['view'], array_merge([
                        '_viewFrame' => 'theme.default.pc.frame',
                        'msg' => $e->getMessage(),
                        'code' => $e->getCode(),
                    ], $exceptionParam['viewData']));
                }
                if ($ret instanceof View) {
                    return response()->make($ret, $statusCode);
                }
            }
        }

        if ($e instanceof BizException || $e instanceof BingoException) {
            $e = new FailedException($message ?: 'Server Error', $e->getCode(), $this->convertExceptionToArray($e));
        } elseif ($e instanceof AuthenticationException) {
            $e = new FailedException($message ?: 'Unauthenticated', $e->getCode(), ['error' => $e->getMessage()]);
        } elseif ($e instanceof ValidationException) {
            $e = new FailedException($message ?: 'Server Error', $e->getCode(), $e->errors());
        } else {
            $e = new FailedException($message ?: 'Server Error');
        }

        $response = parent::render($request, $e);
        $response->header('Access-Control-Allow-Origin', '*');
        $response->header('Access-Control-Allow-Methods', '*');
        $response->header('Access-Control-Allow-Headers', '*');
        return $response;
    }
}
