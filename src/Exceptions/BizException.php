<?php

namespace Bingo\Exceptions;

use BackedEnum;
use Bingo\Enums\Code;
use Illuminate\Support\Facades\Lang;
use Illuminate\Support\Str;
use Throwable;

class BizException extends \Exception
{
    public array $param = [];
    protected BackedEnum $errorCode = Code::FAILED; // 預設的業務邏輯錯誤碼

    /**
     * 构造函数现在接受任何类型的枚举。
     * @param string $message 异常消息
     * @param BackedEnum $code 错误码枚举，任何枚举类型
     * @param Throwable|null $previous 上一个异常
     */
    public function __construct($code, string $message = "", Throwable $previous = null)
    {
        // 确保 $code 是枚举类型
        if (! $code instanceof BackedEnum) {
            throw new \InvalidArgumentException('Code must be an instance of a UnitEnum');
        }

        $this->errorCode = $code;
        parent::__construct($message, $code->value, $previous);
    }

    /**
     * 拋出業務異常，系統會統一處理
     * @param string $message 異常訊息
     * @param BackedEnum $code 错误码枚举，任何枚举类型
     * @param array $param 異常參數
     * @throws BizException
     * @example
     * // 拋出異常訊息
     * BizException::throws('參數錯誤', [], 400);
     * // 使用自定義模板
     * BizException::throws('參數錯誤', ['view' => 'theme.default.customView', 'viewData' => ['foo' => 'bar']], 400);
     * // 使用自定義狀態碼，返回 404
     * BizException::throws('參數錯誤', ['statusCode' => 404], 404);
     */
    public static function throws(BackedEnum $code, string $message = "", array $param = [])
    {
        $message = self::description($code, $message);
        $e = new self($code, $message);
        $e->param = $param;
        $e->errorCode = $code;
        throw $e;
    }

    /**
     * 條件拋出業務異常，參數參考 throws 方法
     * @param string $message 異常訊息
     * @param BackedEnum $code 错误码枚举，任何枚举类型
     * @param bool $condition 條件
     * @param array $param 異常參數
     * @throws BizException
     */
    public static function throwsIf(bool $condition, BackedEnum $code, string $message = "", array $param = []): void
    {
        if ($condition) {
            $message = self::description($code, $message);
            $e = new self($code, $message);
            $e->param = $param;
            $e->errorCode = $code;
            throw $e;
        }
    }

    /**
     * 如果 object 為空，拋出業務異常，參數參考 throws 方法
     * @param string $message 異常訊息
     * @param BackedEnum $code 错误码枚举，任何枚举类型
     * @param mixed $object 對象
     * @param array $param 異常參數
     * @throws BizException
     */
    public static function throwsIfEmpty(mixed $object, BackedEnum $code, string $message = "", array $param = []): void
    {
        if (empty($object)) {
            $message = self::description($code, $message);
            $e = new self($code, $message);
            $e->param = $param;
            $e->errorCode = $code;
            throw $e;
        }
    }

    /**
     * 如果 object 不為空，拋出業務異常，參數參考 throws 方法
     * @param string $message 異常訊息
     * @param BackedEnum $code 错误码枚举，任何枚举类型
     * @param mixed $object 對象
     * @param array $param 異常參數
     * @throws BizException
     */
    public static function throwsIfNotEmpty(mixed $object, BackedEnum $code, string $message = "", array $param = []): void
    {
        if (! empty($object)) {
            $message = self::description($code, $message);
            $e = new self($code, $message);
            $e->param = $param;
            $e->errorCode = $code;
            throw $e;
        }
    }

    /**
     * 如果 response 有錯誤，拋出業務異常，參數參考 throws 方法
     * @param array $response 標準響應 ['code'=>0, 'message'=>'', 'data'=>[]]
     * @param BackedEnum $code 错误码枚举，任何枚举类型
     * @param array $param 異常參數
     * @param string $prefix 異常訊息前綴
     * @throws BizException
     */
    public static function throwsIfResponseError(array $response, BackedEnum $code, array $param = [], string $prefix = ''): void
    {
        if ($prefix) {
            $prefix .= ':';
        }

        if (empty($response)) {
            $e = new self($code, $prefix.'Response Empty');
            $e->param = $param;
            $e->errorCode = $code;
            throw $e;
        } elseif ($response['code'] != Code::SUCCESS->value) {
            $message = $response['message'] ?? ($response["msg"] ?? '');
            $e = new self($code, $prefix.$message);
            $e->param = $param;
            $e->errorCode = $code;
            throw $e;
        }
    }

    /**
     * 獲取業務邏輯錯誤碼
     * @return BackedEnum|Code|int
     */
    public function getErrorCode(): BackedEnum|Code|int
    {
        return $this->errorCode;
    }

    private static function description(BackedEnum $code, string $message): string
    {
        if (empty($message)) {
            $modules = getModuleNameFromClassName(get_class($code));
            if ($modules) {
                $key = "$modules::enums.".get_class($code).'.'.$code->name;
            } else {
                $key = "enums.".static::class.'.'.$code->name;
            }
            $message = Lang::has($key) ? T($key) : Str::of($code->name)->replace('_', ' ')->lower();
        }
        return $message;
    }
}
