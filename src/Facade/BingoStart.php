<?php

namespace Bingo\Facade;

use Illuminate\Support\Facades\Facade;

/**
 * Class BingoStart
 * @method static string cacheKey($key)
 */
class BingoStart extends Facade
{
    protected static function getFacadeAccessor(): string
    {
        return \Bingo\BingoStart::class;
    }
}
