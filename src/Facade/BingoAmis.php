<?php

namespace Bingo\Facade;

use Illuminate\Support\Facades\Facade;

class BingoAmis extends Facade
{
    /**
     * Get the registered name of the component.
     *
     * @return string
     */
    protected static function getFacadeAccessor(): string
    {
        return 'bingo-amis';
    }
}
