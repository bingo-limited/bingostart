<?php

namespace Bingo\Facade;

use Illuminate\Support\Facades\Facade;

/**
 * Yaml
 *
 * @method static array parse(string $contents)
 * @method static array parseFile(string $fileName)
 * @method static array parseFileCached(string $fileName)
 * @method static string render(array $vars, array $options = [])
 *
 * @see \Bingo\Core\Parse\Yaml
 */
class Yaml extends Facade
{
    /**
     * getFacadeAccessor returns the registered name of the component
     * @return string
     */
    protected static function getFacadeAccessor(): string
    {
        return 'parse.yaml';
    }
}
