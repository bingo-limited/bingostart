<?php

namespace Bingo\Facade;

use Illuminate\Support\Facades\Facade;

/**
 * Resizer
 *
 * @see \Bingo\Core\Resize\Resizer
 */
class Resizer extends Facade
{
    /**
     * getFacadeAccessor returns the registered name of the component
     * @return string
     */
    protected static function getFacadeAccessor(): string
    {
        return 'resizer';
    }
}
