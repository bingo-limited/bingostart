<?php

namespace Bingo\Enums;

use Bingo\Enums\Traits\EnumEnhance;

enum Code: int
{
    use EnumEnhance;
    case SUCCESS = 200; // 成功
    case LOST_LOGIN = 10001; //  登录失效
    case VALIDATE_FAILED = 10002; // 验证错误
    case PERMISSION_FORBIDDEN = 10003; // 权限禁止
    case LOGIN_FAILED = 10004; // 登录失败
    case FAILED = 10005; // 操作失败
    case LOGIN_EXPIRED = 10006; // 登录失效
    case LOGIN_BLACKLIST = 10007; // 黑名单
    case USER_FORBIDDEN = 10008; // 账户被禁
    case PARAM_INVALID = 10009; // 参数无效
    case NOT_FOUND = 10010; // 未找到
}
