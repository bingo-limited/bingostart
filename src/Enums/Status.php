<?php

declare(strict_types=1);

namespace Bingo\Enums;

use Bingo\Enums\Traits\EnumEnhance;

enum Status: int
{
    use EnumEnhance;
    case Enable = 1;

    case Disable = 2;

    //    /**
    //     * @desc name
    //     *
    //     */
    //    public function name(): string
    //    {
    //        return match ($this) {
    //            Status::Enable => '启用',
    //
    //            Status::Disable => '禁用'
    //        };
    //    }
    //
    //    /**
    //     * get value
    //     *
    //     * @return int
    //     */
    //    public function value(): int
    //    {
    //        return match ($this) {
    //            Status::Enable => 1,
    //
    //            Status::Disable => 2,
    //        };
    //    }
}
