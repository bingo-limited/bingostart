<?php


declare(strict_types=1);

namespace Bingo;

use Bingo\Support\Manager\FieldManager;
use Bingo\Support\Manager\WidgetManager;
use Illuminate\Contracts\Auth\Authenticatable;
use Illuminate\Contracts\Auth\Guard;
use Illuminate\Contracts\Auth\StatefulGuard;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Foundation\Application;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Str;

class BingoStart
{
    public const VERSION = '0.22';

    public static array $css = [];
    public static array $js = [];
    public static array $style = [];
    public static array $script = [];
    public static array $baseJs = [];

    /**
     * version
     *
     */
    public static function version(): string
    {
        return static::VERSION;
    }

    public static function cacheKey($key): string
    {
        static $hash = null;
        if (null === $hash) {
            $hash = md5(__DIR__);
        }
        return join(':', [$key, $hash]);
    }

    /**
     * 获取当前运行环境
     * @return string|null laravel10|laravel11
     */
    public static function env(): ?string
    {
        static $env = null;
        if (null === $env) {
            $pcs = explode('.', Application::VERSION);
            $env = 'laravel'.$pcs[0];
        };
        return $env;
    }

    public static function moduleRoot()
    {
        return config('bingo.module.root', 'modules');
    }

    /**
     * module root path
     *
     * @return string
     */
    public static function moduleRootPath(): string
    {
        return self::makeDir(base_path(self::moduleRoot()).DIRECTORY_SEPARATOR);
    }

    /**
     * make dir
     *
     * @param string $dir
     * @return string
     */
    public static function makeDir(string $dir): string
    {
        if (! File::isDirectory($dir) && ! File::makeDirectory($dir, 0777, true)) {
            throw new \RuntimeException(sprintf('Directory %s created Failed', $dir));
        }

        return $dir;
    }

    /**
     * module dir
     *
     * @param string $module
     * @param bool $make
     * @return string
     */
    public static function getModulePath(string $module, bool $make = true): string
    {
        if ($make && $module !== 'app') {
            return self::makeDir(self::moduleRootPath().ucfirst($module).DIRECTORY_SEPARATOR);
        }

        return self::moduleRootPath().ucfirst($module).DIRECTORY_SEPARATOR;
    }

    /**
     * delete module path
     *
     * @param string $module
     * @return bool
     */
    public static function deleteModulePath(string $module): bool
    {
        if (self::isModulePathExist($module)) {
            File::deleteDirectory(self::getModulePath($module));
        }

        return true;
    }

    /**
     * module path exists
     *
     * @param string $module
     * @return bool
     */
    public static function isModulePathExist(string $module): bool
    {
        return File::isDirectory(self::moduleRootPath().ucfirst($module).DIRECTORY_SEPARATOR);
    }

    /**
     * module migration dir
     *
     * @param string $module
     * @return string
     */
    public static function getModuleMigrationPath(string $module): string
    {
        return self::makeDir(self::getModulePath($module).'database'.DIRECTORY_SEPARATOR.'migrations'.DIRECTORY_SEPARATOR);
    }

    /**
     * module seeder dir
     *
     * @param string $module
     * @return string
     */
    public static function getModuleSeederPath(string $module): string
    {
        return self::makeDir(self::getModulePath($module).'database'.DIRECTORY_SEPARATOR.'seeder'.DIRECTORY_SEPARATOR);
    }

    /**
     * get modules dir
     *
     * @return array
     */
    public static function getModulesPath(): array
    {
        return File::directories(self::moduleRootPath());
    }

    /**
     * get module root namespace
     *
     * @return string
     */
    public static function getModuleRootNamespace(): string
    {
        return config('bingo.module.namespace', 'Modules').'\\';
    }

    /**
     * get module root namespace
     *
     * @param $moduleName
     * @return string
     */
    public static function getModuleNamespace($moduleName): string
    {
        return self::getModuleRootNamespace().ucfirst($moduleName).'\\';
    }

    /**
     * model namespace
     *
     * @param $moduleName
     * @return string
     */
    public static function getModuleModelNamespace($moduleName): string
    {
        return self::getModuleNamespace($moduleName).'Models\\';
    }

    /**
     * getServiceProviders
     *
     * @param $moduleName
     * @return string
     */
    public static function getModuleServiceProviderNamespace($moduleName): string
    {
        return self::getModuleNamespace($moduleName).'Providers\\';
    }

    /**
     *
     * @param $moduleName
     * @return string
     */
    public static function getModuleServiceProvider($moduleName): string
    {
        return self::getModuleServiceProviderNamespace($moduleName).ucfirst($moduleName).'ServiceProvider';
    }

    /**
     * controller namespace
     *
     * @param $moduleName
     * @return string
     */
    public static function getModuleControllerNamespace($moduleName): string
    {
        return self::getModuleNamespace($moduleName).'Admin\\Controllers\\';
    }

    /**
     * getModuleRequestNamespace
     *
     * @param $moduleName
     * @return string
     */
    public static function getModuleRequestNamespace($moduleName): string
    {
        return self::getModuleNamespace($moduleName).'Admin\\Requests\\';
    }

    /**
     * getModuleRequestNamespace
     *
     * @param $moduleName
     * @return string
     */
    public static function getModuleEventsNamespace($moduleName): string
    {
        return self::getModuleNamespace($moduleName).'Events\\';
    }

    /**
     * getModuleRequestNamespace
     *
     * @param $moduleName
     * @return string
     */
    public static function getModuleListenersNamespace($moduleName): string
    {
        return self::getModuleNamespace($moduleName).'Listeners\\';
    }


    /**
     * module provider dir
     *
     * @param string $module
     * @return string
     */
    public static function getModuleProviderPath(string $module): string
    {
        return self::makeDir(self::getModulePath($module).'Providers'.DIRECTORY_SEPARATOR);
    }

    /**
     * module model dir
     *
     * @param string $module
     * @return string
     */
    public static function getModuleModelPath(string $module): string
    {
        return self::makeDir(self::getModulePath($module).'Models'.DIRECTORY_SEPARATOR);
    }

    /**
     * module controller dir
     *
     * @param string $module
     * @return string
     */
    public static function getModuleControllerPath(string $module): string
    {
        return self::makeDir(self::getModulePath($module).'Admin'.DIRECTORY_SEPARATOR.'Controllers'.DIRECTORY_SEPARATOR);
    }

    /**
     * module request dir
     *
     * @param string $module
     * @return string
     */
    public static function getModuleRequestPath(string $module): string
    {
        return self::makeDir(self::getModulePath($module).'Admin'.DIRECTORY_SEPARATOR.'Requests'.DIRECTORY_SEPARATOR);
    }

    /**
     * module request dir
     *
     * @param string $module
     * @return string
     */
    public static function getModuleEventPath(string $module): string
    {
        return self::makeDir(self::getModulePath($module).'Events'.DIRECTORY_SEPARATOR);
    }

    /**
     * module request dir
     *
     * @param string $module
     * @return string
     */
    public static function getModuleListenersPath(string $module): string
    {
        return self::makeDir(self::getModulePath($module).'Listeners'.DIRECTORY_SEPARATOR);
    }

    /**
     * commands path
     *
     * @param string $module
     * @return string
     */
    public static function getCommandsPath(string $module): string
    {
        return self::makeDir(self::getModulePath($module).'Commands'.DIRECTORY_SEPARATOR);
    }

    /**
     * commands namespace
     *
     * @param string $module
     * @return string
     */
    public static function getCommandsNamespace(string $module): string
    {
        return self::getModuleNamespace($module).'Commands\\';
    }


    /**
     * module route
     *
     * @param string $module
     * @param string $routeName
     * @return string
     */
    public static function getModuleRoutePath(string $module, string $routeName = 'route.php'): string
    {
        $path = self::getModulePath($module).'routes'.DIRECTORY_SEPARATOR;

        self::makeDir($path);

        return $path.$routeName;
    }

    /**
     * module route multiple
     *
     * @param string $module
     * @param string $apiType api 目录 如:Web,Api,Admin,OpenApi
     * @param string $routeName
     * @return string
     */
    public static function getModuleRoutePathMultiple(string $module, string $apiType, string $routeName = 'route.php'): string
    {
        $path = self::getModulePath($module).$apiType.DIRECTORY_SEPARATOR;
        return $path.$routeName;
    }

    public static function getAppRoutePath(string $apiType, string $routeName = 'route.php'): string
    {
        $path = app_path().DIRECTORY_SEPARATOR.$apiType.DIRECTORY_SEPARATOR;
        return $path.$routeName;
    }

    /**
     * module route.php exists
     *
     * @param string $module
     * @return bool
     */
    public static function isModuleRouteExists(string $module): bool
    {
        return File::exists(self::getModuleRoutePath($module));
    }

    /**
     * module views path
     *
     * @param string $module
     * @return string
     */
    public static function getModuleViewsPath(string $module): string
    {
        return self::makeDir(self::getModulePath($module).'views'.DIRECTORY_SEPARATOR);
    }

    /**
     * relative path
     *
     * @param $path
     * @return string
     */
    public static function getModuleRelativePath($path): string
    {
        return Str::replaceFirst(base_path(), '.', $path);
    }


    /**
     * get module
     *
     * @return array
     */
    public static function parseFromRouteAction(): array
    {
        [$controllerNamespace, $action] = explode('@', Route::currentRouteAction());

        $controllerNamespace = Str::of($controllerNamespace)->lower()->remove('controller')->explode('\\');


        $controller = $controllerNamespace->pop();


        $module = $controllerNamespace->get(1);

        return [$module, $controller, $action];
    }


    /**
     *
     * @param string $module
     * @param string $controller
     * @return array
     * @throws \ReflectionException
     */
    public static function getControllerActions(string $module, string $controller): array
    {
        $controller = self::getModuleControllerNamespace($module).Str::of($controller)->ucfirst()->append('Controller')->toString();

        $reflectionClass = new \ReflectionClass($controller);

        $actions = [];

        foreach ($reflectionClass->getMethods() as $method) {
            if ($method->isPublic() && ! $method->isConstructor()) {
                $actions[] = $method->getName();
            }
        }

        return $actions;
    }


    /**
     * get route cache path
     *
     * @return string
     */
    public static function getRouteCachePath(): string
    {
        return config('bingo.route.cache_path', base_path('bootstrap/cache').DIRECTORY_SEPARATOR.'admin_route_cache.php');
    }


    public static function css($css = null)
    {
        if (! isset(self::$css)) {
            self::$css = [];
        }

        if (! is_null($css)) {
            $css = (array) $css;
            self::$css = array_merge(self::$css, $css);
            return "";
        }
        $css = array_unique(array_filter(self::$css, function ($item) {
            return is_string($item) && trim($item) !== '';
        }));
        return view('bingo::part.css', compact('css'));
    }

    public static function js($js = null): View|Application|Factory|string|\Illuminate\Contracts\Foundation\Application
    {
        if (! is_null($js)) {
            // 确保只添加不存在的新脚本
            foreach ((array) $js as $script) {
                if (is_string($script) && trim($script) !== '' && ! in_array($script, self::$js)) {
                    self::$js[] = $script;
                }
            }
            return "";
        }

        // 使用 array_filter 和 array_unique 清理数组
        $js = array_unique(array_filter(self::$js, function ($item) {
            return is_string($item) && trim($item) !== '';
        }));

        return view('bingo::part.js', compact('js'));
    }


    public static function baseJs($baseJs = null): View|Application|Factory|string|\Illuminate\Contracts\Foundation\Application
    {
        if (! is_null($baseJs)) {
            self::$baseJs = array_merge(self::$baseJs, (array) $baseJs);
            return "";
        }
        $baseJs = array_merge(static::$baseJs, []);
        $baseJs = array_unique(array_filter($baseJs, function ($item) {
            return is_string($item) && trim($item) !== '';
        }));
        return view('bingo::part.baseJs', compact('baseJs'));
    }


    /**
     * CSS 样式代码，显示在head头部
     * @param string $style
     * @return Application|Factory|\Illuminate\Contracts\Foundation\Application|string|View
     */
    public static function style(string $style = '')
    {
        $style = trim($style);
        if (! empty($style)) {
            self::$style = array_merge(self::$style, (array) $style);
            return "";
        }
        $additionalStyles = array_merge(
            FieldManager::collectFieldAssets('style'),
            WidgetManager::collectWidgetAssets('style')
        );
        self::$style = array_merge(static::$style, $additionalStyles);
        self::$style = array_unique(array_filter(self::$style, function ($item) {
            return is_string($item) && trim($item) !== '';
        }));
        return view('bingo::part.style', ['style' => self::$style]);
    }


    /**
     * JS 脚本代码，显示在body底部
     * @param string $script
     * @return Application|Factory|\Illuminate\Contracts\Foundation\Application|string|View
     */
    public static function script(string $script = '')
    {
        $script = trim($script);
        if (! empty($script)) {
            self::$script = array_merge(self::$script, (array) $script);
            return "";
        }
        $script = array_unique(array_filter(self::$script, function ($item) {
            return is_string($item) && trim($item) !== '';
        }));
        return view('bingo::part.script', ['script' => $script]);
    }

    public static function guard(): Guard|StatefulGuard
    {
        return Auth::guard(getGuardName());
    }

    public static function user(): ?Authenticatable
    {
        return static::guard()->user();
    }


}
