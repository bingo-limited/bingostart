<?php

namespace Bingo\Foundation\Providers;

use Illuminate\Support\AggregateServiceProvider;

/**
 * AppSupportServiceProvider supplies eager providers
 */
class AppSupportServiceProvider extends AggregateServiceProvider
{
    /**
     * provides gets the services provided by the provider
     */
    protected $providers = [
        //        \Bingo\Database\DatabaseServiceProvider::class,
        //        \Bingo\Halcyon\HalcyonServiceProvider::class,
        \Bingo\Core\Filesystem\FilesystemServiceProvider::class,
        \Bingo\Core\Html\UrlServiceProvider::class,
        //        \Bingo\Argon\ArgonServiceProvider::class
    ];
}
