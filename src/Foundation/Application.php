<?php

namespace Bingo\Foundation;

use Illuminate\Contracts\Container\BindingResolutionException;
use Illuminate\Contracts\Debug\ExceptionHandler;
use Illuminate\Foundation\Application as ApplicationBase;
use RuntimeException;
use Closure;
use Error;

/**
 * 作為 Laravel 擴展的應用程序基礎類
 */
class Application extends ApplicationBase
{
    /**
     * before 在路由運行之前調用邏輯。
     * @param string|Closure $callback
     * @return void
     */
    public function before(string|Closure $callback): void
    {
        $this['router']->before($callback);
    }

    /**
     * after 在路由完成後調用邏輯。
     * @param string|Closure $callback
     * @return void
     */
    public function after(string|Closure $callback): void
    {
        $this['router']->after($callback);
    }

    /**
     * error 註冊應用程序錯誤處理程序。
     * @param callable $callback
     * @return void
     * @throws BindingResolutionException
     */
    public function error(callable $callback): void
    {
        $this->make(ExceptionHandler::class)->renderable($callback);
    }

    /**
     * @deprecated 使用帶有 Error 異常類型的 App::error
     */
    public function fatal(callable $callback): void
    {
        $this->error(function (Error $e) use ($callback) {
            return $callback($e);
        });
    }

    /**
     * setLocale 設置應用程序的區域設置。
     * @param string $locale
     * @return void
     */
    public function setLocale($locale): void
    {
        parent::setLocale($locale);

        $this['events']->dispatch('locale.changed', [$locale]);
    }


    /**
     * getNamespace 返回應用程序命名空間。
     * @return string
     * @throws RuntimeException
     */
    public function getNamespace(): string
    {
        return 'App\\';
    }

    public function extendInstance($abstract, Closure $callback): void
    {
        $this->afterResolving($abstract, $callback);

        if ($this->resolved($abstract)) {
            $callback($this->make($abstract), $this);
        }
    }
}
