<?php

namespace Bingo\Foundation\Bootstrap;

use Illuminate\Contracts\Foundation\Application;
use Illuminate\Foundation\Bootstrap\LoadConfiguration as LoadConfigurationBase;
use Illuminate\Contracts\Config\Repository as RepositoryContract;
use Illuminate\Config\Repository;
use Exception;

/**
 * LoadConfiguration 引導配置實例
 */
class LoadConfiguration extends LoadConfigurationBase
{
    /**
     * 引導給定的應用程序。
     */
    public function bootstrap(Application $app): void
    {
        $items = [];

        // 首先我們將檢查是否有緩存的配置文件。如果有，我們將從該文件加載
        // 配置項，這樣會非常快。否則，我們需要遍歷每個配置文件並加載它們。
        if (file_exists($cached = $app->getCachedConfigPath())) {
            $items = require $cached;

            $loadedFromCache = true;
        }

        // 接下來，我們將遍歷配置目錄中的所有配置文件，
        // 並將每個文件加載到存儲庫中。這將使所有選項
        // 可供開發人員在應用程序的各個部分使用。
        $app->instance('config', $config = new Repository($items));

        if (! isset($loadedFromCache)) {
            $this->loadConfigurationFiles($app, $config);
        }

        // 最後，我們將根據加載的配置值設置應用程序的環境。
        // 我們將傳遞一個回調函數，該函數將用於在沒有 "--env"
        // 開關的 Web 上下文中獲取環境。
        $app->detectEnvironment(function () use ($config): string {
            return $config->get('app.env', 'production');
        });

        date_default_timezone_set($config->get('app.timezone', 'UTC'));

        mb_internal_encoding('UTF-8');

        // 修復 XDebug 在嵌套深度超過 100 時中止線程的問題
        ini_set('xdebug.max_nesting_level', '1000');
    }

    /**
     * 從所有文件加載配置文件。
     *
     * @param Application $app
     * @param RepositoryContract $repository
     * @return void
     *
     * @throws Exception
     */
    protected function loadConfigurationFiles(Application $app, RepositoryContract $repository): void
    {
        $files = $this->getConfigurationFiles($app);

        if (! isset($files['app'])) {
            throw new Exception('無法加載 "app" 配置文件。');
        }

        foreach ($files as $key => $path) {
            // 文件名為 config.php 的文件被視為根節點
            if (basename($path) === 'config.php') {
                $key = substr($key, 0, -7);
            }

            $repository->set($key, require $path);
        }
    }
}
