<?php

namespace Bingo\Events;

use Illuminate\Support\Testing\Fakes\EventFake as EventFakeBase;

/**
 * FakeDispatcher
 */
class FakeDispatcher extends EventFakeBase
{
    /**
     * __construct a new event fake instance.
     */
    public function __construct($dispatcher, $eventsToFake = [])
    {
        parent::__construct(
            $dispatcher instanceof PriorityDispatcher ? $dispatcher->getLaravelDispatcher() : $dispatcher,
            $eventsToFake
        );
    }

    /**
     * fire proxies to dispatch
     */
    public function fire(...$args): ?array
    {
        return parent::dispatch(...$args);
    }
}
