<?php

declare(strict_types=1);

namespace Bingo\Base;

use Illuminate\Http\Request;

class AdminController extends BingoController
{
    /**
     * @var bool 是否创建界面
     */
    protected bool $isCreate = false;
    /**
     * @var bool 是否编辑界面
     */
    protected bool $isEdit = false;
    /**
     * @var bool 是否新增提交
     */
    protected bool $isStore = false;
    /**
     * @var bool 是否修改提交
     */
    protected bool $isUpdate = false;
    /**
     * @var mixed|null 当前更新的id
     */
    protected mixed $resourceKey = null;

    protected ?string $formRequestClass = null;

    protected function setFormRequestClass(string $formRequestClass): void
    {
        $this->formRequestClass = $formRequestClass;
    }

    public function index(Request $request)
    {
        return $this->grid();
    }

    public function show(Request $request)
    {
        return $this->grid();
    }

    public function create()
    {
        $this->isCreate = true;
        return $this->form();
    }

    public function edit($id)
    {
        $this->isEdit = true;
        $this->resourceKey = $id;
        return $this->form()->edit($id);
    }

    public function update(Request $request, $id)
    {
        $this->resourceKey = $id;
        $this->isUpdate = true;
        $this->validateRequest($request);
        if ($id === "quickSave") {
            return $this->form()->quickUpdate();
        }
        if ($id === "quickSaveItem") {
            return $this->form()->quickItemUpdate();
        }
        return $this->form()->update($id);
    }

    public function store(Request $request)
    {
        $this->isStore = true;
        $this->validateRequest($request);
        return $this->form()->store();
    }

    public function destroy(Request $request, $id)
    {
        $this->resourceKey = $id;
        $this->validateRequest($request);
        return $this->form()->destroy($id);
    }

    protected function validateRequest(Request $request): void
    {
        if ($this->formRequestClass) {
            $formRequest = app()->make($this->formRequestClass);
            $formRequest->validateResolved();
        }
    }
}
