<?php

declare(strict_types=1);

namespace Bingo\Base;

use Bingo\Core\Config\SettingsManager;
use Illuminate\Http\Request;

/**
 * BaseSettingController 基类设置控制器
 */
abstract class BaseSettingController extends BingoController
{
    protected SettingsManager $settingsManager;

    public function __construct(SettingsManager $settingsManager)
    {
        $this->settingsManager = $settingsManager;
    }


    /**
     * 获取指定模块的所有设置项
     * @param string $module
     * @return array
     */
    public function showByModule(string $module): array
    {
        return $this->settingsManager->listItems($module);
    }

    /**
     * 获取指定模块的指定设置项
     * @param string $module
     * @param string $id
     * @return array
     */
    public function show(string $module, string $id): array
    {
        return $this->settingsManager->findSettingItem($module, $id);
    }

    /**
     * 添加新的设置项
     * @param Request $request
     * @return array
     */
    public function store(Request $request): array
    {
        $module = $request->input('module');
        $data = $request->all();
        $this->settingsManager->addSettingItems($module, $data);

        return ['status' => 'success'];
    }

    /**
     * 更新指定模块的指定设置项
     * @param Request $request
     * @param string $module
     * @param string $id
     * @return array
     */
    public function update(Request $request, string $module, string $id): array
    {
        $data = $request->all();
        $this->settingsManager->addSettingItem($module, $id, $data);

        return ['status' => 'success'];
    }

    /**
     * 删除指定模块的指定设置项
     * @param string $module
     * @param string $id
     * @return array
     * @throws \Exception
     */
    public function destroy(string $module, string $id): array
    {
        $this->settingsManager->removeSettingItem($module, $id);

        return ['status' => 'success'];
    }
}
