<?php

namespace Bingo\Core\View;

use Bingo\Core\Config\BingoConfig;
use Bingo\Core\Util\AgentUtil;
use Bingo\Enums\Code;
use Bingo\Exceptions\BizException;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Str;
use Modules\Common\Provider\SiteTemplate\SiteTemplateProvider;
use Psr\Container\ContainerExceptionInterface;
use Psr\Container\NotFoundExceptionInterface;

trait ResponsiveViewTrait
{
    public function getModule()
    {
        static $module = null;
        if (null === $module) {
            $cls = get_class($this);
            if (preg_match('/^Modules\\\\([\\w]+).*?/', $cls, $mat)) {
                $module = $mat[1];
            } elseif (method_exists($this, 'getCurrentModule')) {
                $module = $this->getCurrentModule();
            } else {
                $module = '';
            }
        }
        return $module;
    }

    private function fetchViewPath($templateName, $templateRoot, $module, $device, $view)
    {
        if (Str::contains($view, '::')) {
            return $view;
        }
        $viewThemeCustom = "theme.$templateName.$device.$view";
        $viewTheme = "$templateRoot.$device.$view";
        if ($module) {
            $viewModule = "module::$module.views.$device.$view";
        }
        $viewDefault = "theme.default.$device.$view";
        if (view()->exists($viewThemeCustom)) {
            return $viewThemeCustom;
        }
        if (view()->exists($viewTheme)) {
            return $viewTheme;
        }
        if ($module) {
            if (view()->exists($viewModule)) {
                return $viewModule;
            }
        }
        if (view()->exists($viewDefault)) {
            return $viewDefault;
        }

    }

    protected function viewTemplateProvider()
    {
        $provider = null;
        try {
            $t = request()->get('bingoSiteTemplate');
        } catch (NotFoundExceptionInterface|ContainerExceptionInterface $e) {
        }
        if (! empty($t)) {
            $provider = SiteTemplateProvider::get($t);
            if (! empty($provider)) {
                Session::flash('bingoSiteTemplate', $t);
            }
        }
        if (empty($provider)) {
            $t = Session::get('bingoSiteTemplate');
            if (! empty($t)) {
                $provider = SiteTemplateProvider::get($t);
                if (empty($provider)) {
                    Session::forget('bingoSiteTemplate');
                }
            }
        }
        if (empty($provider)) {
            $t = bingostart_config()->getWithEnv('siteTemplate', BingoConfig::DEFAULT_LANG, 'default');
            $provider = SiteTemplateProvider::get($t);
            if (empty($provider)) {
                $t = null;
            }
        }
        return $provider;
    }

    /**
     * @param $view
     * @return array
     * @throws BizException
     *
     * @example
     * list($view, $viewFrame) = $this->viewPaths('member.index')
     */
    protected function viewPaths($view): array
    {
        /**
         * 存储当前模板名称，如 default, xxxxx
         */
        static $templateName = null;

        /**
         * 当前模板根目录
         */
        static $templateRoot = null;
        if (null === $templateName) {
            $provider = $this->viewTemplateProvider();
            $templateName = ($provider ? $provider->name() : 'default');
            $templateRoot = ($provider ? $provider->root() : 'theme.'.$templateName);
            Session::put('bingoSiteTemplateUsing', $templateName);
        }

        $useView = null;
        $useFrameView = null;
        $module = $this->getModule();
        if ($this->isMobile()) {
            $useView = $this->fetchViewPath($templateName, $templateRoot, $module, 'm', $view);
            $useFrameView = $this->fetchViewPath($templateName, $templateRoot, $module, 'm', 'frame');
        }
        if (empty($useView)) {
            $useView = $this->fetchViewPath($templateName, $templateRoot, $module, 'pc', $view);
        }
        if (empty($useFrameView)) {
            $useFrameView = $this->fetchViewPath($templateName, $templateRoot, $module, 'pc', 'frame');
        }
        View::share('_viewFrame', $useFrameView);
        BizException::throwsIfEmpty($useView, Code::FAILED, 'View Not Exists : '.$view);
        return [$useView, $useFrameView];
    }

    protected function shareDialogPageViewFrame(): void
    {
        try {
            list($_viewFrameDialog, $_) = $this->viewPaths('dialogPage');
        } catch (BizException $e) {
        }
        View::share('_viewFrameDialog', $_viewFrameDialog);
    }

    protected function view($view, $viewData = [])
    {
        try {
            list($view, $frameView) = $this->viewPaths($view);
        } catch (BizException $e) {
        }
        // return [$view, $frameView];
        return view($view, $viewData);
    }

    protected function viewRender($view, $viewData = []): string
    {
        try {
            list($view, $frameView) = $this->viewPaths($view);
        } catch (BizException $e) {
        }
        return view($view, $viewData)->render();
    }

    protected function viewRealpath($view): string
    {
        return View::getFinder()->find($view);
    }

    protected function isMobile(): bool
    {
        return AgentUtil::isMobile();
    }

    protected function isPC(): bool
    {
        return AgentUtil::isPC();
    }


}
