<?php

namespace Bingo\Core\View;

use Bingo\Core\Config\BingoConfig;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Str;
use Modules\Common\Provider\SiteTemplate\SiteTemplateProvider;

class ResponsiveView
{
    public static function templateRoot()
    {
        static $provider = null;
        static $templateRoot = null;
        if (null !== $templateRoot) {
            return $templateRoot;
        }
        static $templateName = 'default';
        $bingoSiteTemplate = Request::get('bingoSiteTemplate', null);
        if (! empty($bingoSiteTemplate)) {
            $provider = SiteTemplateProvider::get($bingoSiteTemplate);
            if (! empty($provider)) {
                Session::put('bingoSiteTemplate', $bingoSiteTemplate);
            }
        }
        if (empty($provider)) {
            $bingoSiteTemplate = Session::get('bingoSiteTemplate', null);
            if (! empty($bingoSiteTemplate)) {
                $provider = SiteTemplateProvider::get($bingoSiteTemplate);
                if (empty($provider)) {
                    Session::forget('bingoSiteTemplate');
                }
            }
        }
        if (empty($provider)) {
            $templateName = bingostart_config()->getWithEnv('siteTemplate', BingoConfig::DEFAULT_LANG, 'default');
            $provider = SiteTemplateProvider::get($templateName);
        }
        if ($provider && $provider->root()) {
            $templateRoot = $provider->root();
        } else {
            $templateRoot = "theme.$templateName";
        }
        return $templateRoot;
    }

    public static function templateRootRealpath($module)
    {
        $root = self::templateRoot();
        if (Str::startsWith($root, 'module::')) {
            $root = str_replace(['::', '.'], '/', $root);
            $root = base_path($root);
        } elseif (Str::startsWith($root, 'theme.')) {
            $root = 'resources/views/'.str_replace(['.'], '/', $root);
            $root = base_path($root);
            if (! file_exists($root)) {
                $root = base_path('module/'.$module.'/View');
            }
            if (! file_exists($root)) {
                $root = base_path('resources/views/theme/default');
            }
        } else {
            $root = base_path($root);
        }
        return rtrim($root, '\\/').'/';
    }
}
