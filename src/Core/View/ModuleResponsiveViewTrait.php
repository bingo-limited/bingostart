<?php

namespace Bingo\Core\View;

use Bingo\Core\Config\BingoConfig;
use Bingo\Core\Util\AgentUtil;
use Bingo\Enums\Code;
use Bingo\Exceptions\BizException;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Str;
use Modules\Common\Provider\SiteTemplate\SiteTemplateProvider;
use Psr\Container\ContainerExceptionInterface;
use Psr\Container\NotFoundExceptionInterface;

trait ModuleResponsiveViewTrait
{
    public function getModule()
    {
        static $module = null;
        if (null === $module) {
            $cls = get_class($this);
            if (preg_match('/^Modules\\\\([\\w]+).*?/', $cls, $mat)) {
                $module = $mat[1];
            } elseif (method_exists($this, 'getCurrentModule')) {
                $module = $this->getCurrentModule();
            } else {
                $module = '';
            }
        }
        return $module;
    }

    private function fetchViewPath($templateName, $templateRoot, $module, $device, $view)
    {
        if (Str::contains($view, '::')) {
            return $view;
        }

        // 动态生成视图路径，支持不同模块
        $viewThemeCustom = "$module::theme.$templateName.$device.$view";
        $viewTheme = "$templateRoot.$device.$view";
        $viewModule = "$module::$device.$view";
        $viewDefault = "$module::theme.default.$device.$view";


        if (view()->exists($viewThemeCustom)) {
            return $viewThemeCustom;
        }
        if (view()->exists($viewTheme)) {
            return $viewTheme;
        }
        if ($module && view()->exists($viewModule)) {
            return $viewModule;
        }
        if (view()->exists($viewDefault)) {
            return $viewDefault;
        }

    }

    protected function viewTemplateProvider()
    {
        $provider = null;
        try {
            $t = request()->get('bingoSiteTemplate');
        } catch (NotFoundExceptionInterface|ContainerExceptionInterface $e) {
        }
        if (! empty($t)) {
            $provider = SiteTemplateProvider::get($t);
            if (! empty($provider)) {
                Session::flash('bingoSiteTemplate', $t);
            }
        }
        if (empty($provider)) {
            $t = Session::get('bingoSiteTemplate');
            if (! empty($t)) {
                $provider = SiteTemplateProvider::get($t);
                if (empty($provider)) {
                    Session::forget('bingoSiteTemplate');
                }
            }
        }
        if (empty($provider)) {
            $t = bingostart_config()->getWithEnv('siteTemplate', BingoConfig::DEFAULT_LANG, 'default');
            $provider = SiteTemplateProvider::get($t);
            if (empty($provider)) {
                $t = null;
            }
        }
        return $provider;
    }

    /**
     * @param $view
     * @param string $frame
     * @return array
     * @throws BizException
     *
     * @example
     * list($view, $viewFrame) = $this->viewPaths('member.index', 'frame')
     */
    protected function viewPaths($view, string $frame = 'frame'): array
    {
        static $templateName = null;
        static $templateRoot = null;

        if (null === $templateName) {
            $provider = $this->viewTemplateProvider();
            $templateName = ($provider ? $provider->name() : 'default');
            $templateRoot = ($provider ? $provider->root() : 'Modules.'.$this->getModule().'.Views.theme.'.$templateName);
            Session::put('bingoSiteTemplateUsing', $templateName);
        }

        $useView = null;
        $useFrameView = null;
        $module = $this->getModule();
        $device = $this->isMobile() ? 'pc' : 'pc';

        if (empty($useView)) {
            $useView = $this->fetchViewPath($templateName, $templateRoot, $module, $device, $view);
        }
        if (empty($useFrameView)) {
            $useFrameView = $this->fetchViewPath($templateName, $templateRoot, $module, $device, $frame);
        }

        View::share('_viewFrame', $useFrameView);

        BizException::throwsIfEmpty($useView, Code::FAILED, 'View Not Exists : '.$view);

        return [$useView, $useFrameView];
    }

    protected function shareDialogPageViewFrame(): void
    {
        try {
            list($_viewFrameDialog, $_) = $this->viewPaths('dialogPage');
        } catch (BizException $e) {
        }
        View::share('_viewFrameDialog', $_viewFrameDialog);
    }

    protected function view($view, $viewData = [], $frame = 'frame')
    {
        try {
            list($view, $frameView) = $this->viewPaths($view, $frame);
        } catch (BizException $e) {
        }
        return view($view, $viewData);
    }

    protected function viewRender($view, $viewData = [], $frame = 'frame'): string
    {
        try {
            list($view, $frameView) = $this->viewPaths($view, $frame);
        } catch (BizException $e) {
        }
        return view($view, $viewData)->render();
    }

    protected function viewRealpath($view): string
    {
        return View::getFinder()->find($view);
    }

    protected function isMobile(): bool
    {
        return AgentUtil::isMobile();
    }

    protected function isPC(): bool
    {
        return AgentUtil::isPC();
    }
}
