<?php

namespace Bingo\Core\Provider;

use Bingo\Enums\Code;
use Bingo\Exceptions\BizException;
use Bingo\Core\Util\FileUtil;

/**
 * Class FontProvider
 * @package Bingo\Core\Provider
 * @method static AbstractFontProvider first()
 */
class FontProvider
{
    use ProviderTrait;
    /**
     * @var array
     */
    private static array $list = [
        DefaultFontProvider::class
    ];


    public static function firstLocalPathOrFail(): ?string
    {
        $provider = self::first();
        BizException::throwsIfEmpty($provider, Code::FAILED, 'FontProvider Empty');
        $fontLocalPath = $provider->path();
        return FileUtil::savePathToLocalTemp($fontLocalPath, 'ttf', true);
    }
}
