<?php

namespace Bingo\Core\Provider;

abstract class AbstractFontProvider
{
    abstract public function name();

    abstract public function title();

    abstract public function path();
}
