<?php

namespace Bingo\Core\Provider;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Blade;
use Bingo\Core\Services\ShortcodeService;

/**
 * BladeServiceProvider 服务提供者，用于注册和配置 Blade 扩展。
 */
class BladeServiceProvider extends ServiceProvider
{
    /**
     * 启动 Blade 扩展服务。
     *
     * @param ShortcodeService $shortcodeService
     * @return void
     */
    public function boot(ShortcodeService $shortcodeService): void
    {
        Blade::directive('shortcode', function ($expression) use ($shortcodeService) {
            return "<?php echo app('".ShortcodeService::class."')->parse($expression); ?>";
        });
    }
}
