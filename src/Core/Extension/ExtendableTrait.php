<?php

namespace Bingo\Core\Extension;

use ReflectionClass;
use ReflectionException;
use ReflectionMethod;
use ReflectionFunction;
use ReflectionFunctionAbstract;
use BadMethodCallException;
use Exception;

/**
 * ExtendableTrait 特徵用於無法訪問底層基礎類的情況，
 * 例如屬於基礎框架（Laravel）的類。它目前被控制器和
 * 模型類使用。
 *
 * @package \Bingo\Core\Extension
 * @see Extendable
 */
trait ExtendableTrait
{
    /**
     * @var array 包含類反射信息，包括行為
     */
    protected array $extensionData = [
        'extensions' => [],
        'methods' => [],
        'dynamicMethods' => [],
        'dynamicProperties' => []
    ];

    /**
     * @var array 行為使用的靜態方法集合
     */
    protected static array $extendableStaticMethods = [];

    /**
     * @var bool 指示是否可以創建動態屬性
     */
    protected static bool $extendableGuardProperties = true;

    /**
     * 應作為構造函數的一部分調用
     * @return void
     * @throws Exception
     */
    public function extendableConstruct(): void
    {
        // Apply init callbacks
        $classes = array_merge([static::class], class_parents(static::class));
        foreach ($classes as $class) {
            if (isset(Container::$classCallbacks[$class]) && is_array(Container::$classCallbacks[$class])) {
                foreach (Container::$classCallbacks[$class] as $callback) {
                    call_user_func($callback, $this);
                }
            }
        }

        // Apply extensions, soft implement behaviors with @
        foreach ($this->extensionExtractImplements() as $useClass) {
            if (str_starts_with($useClass, '@')) {
                $useClass = substr($useClass, 1);
                if (! class_exists($useClass)) {
                    continue;
                }
            }

            $this->extendClassWith($useClass);
        }
    }

    /**
     * 在序列化對象時應調用
     */
    public function extendableDestruct(): void
    {
        $this->extensionData = [
            'extensions' => [],
            'methods' => [],
            'dynamicMethods' => [],
            'dynamicProperties' => []
        ];
    }

    /**
     * `::extend()` 靜態方法的輔助方法
     * @param callable $callback
     * @return void
     */
    public static function extendableExtendCallback(callable $callback): void
    {
        $class = get_called_class();
        if (
            ! isset(Container::$classCallbacks[$class]) ||
            ! is_array(Container::$classCallbacks[$class])
        ) {
            Container::$classCallbacks[$class] = [];
        }

        Container::$classCallbacks[$class][] = $callback;
    }

    /**
     * @deprecated 使用 Bingo\Core\Extension\Container::clearExtensions()
     */
    public static function clearExtendedClasses(): void
    {
        Container::clearExtensions();
    }

    /**
     * 返回要實現的類
     */
    protected function extensionExtractImplements(): array
    {
        if (! $this->implement) {
            return [];
        }

        if (is_string($this->implement)) {
            $uses = explode(',', $this->implement);
        } elseif (is_array($this->implement)) {
            $uses = $this->implement;
        } else {
            throw new Exception(sprintf('Class %s contains an invalid $implement value', static::class));
        }

        foreach ($uses as &$use) {
            $use = str_replace('.', '\\', trim($use));
        }

        return $uses;
    }

    /**
     * 從行為中提取可用方法並將其添加到可調用方法列表中
     * @param string $extensionName
     * @param object $extensionObject
     * @return void
     * @throws Exception
     */
    protected function extensionExtractMethods(string $extensionName, object $extensionObject): void
    {
        if (! method_exists($extensionObject, 'extensionIsHiddenMethod')) {
            throw new Exception(sprintf(
                'Extension %s should inherit Bingo\Core\Extension\ExtensionBase or implement Bingo\Core\Extension\ExtensionTrait.',
                $extensionName
            ));
        }

        $extensionMethods = get_class_methods($extensionName);
        foreach ($extensionMethods as $methodName) {
            if (
                $methodName === '__construct' ||
                $extensionObject->extensionIsHiddenMethod($methodName)
            ) {
                continue;
            }

            $this->extensionData['methods'][$methodName] = $extensionName;
        }
    }

    /**
     * 以程序方式向可擴展類添加方法
     * @param string $dynamicName
     * @param callable $method
     * @param string|null $extension
     */
    public function addDynamicMethod(string $dynamicName, callable $method, string $extension = null): void
    {
        if (
            is_string($method) &&
            $extension &&
            ($extensionObj = $this->getClassExtension($extension))
        ) {
            $method = [$extensionObj, $method];
        }

        $this->extensionData['dynamicMethods'][$dynamicName] = $method;
    }

    /**
     * 以程序方式向可擴展類添加屬性
     * @param string $dynamicName
     * @param string|null $value
     */
    public function addDynamicProperty(string $dynamicName, string $value = null): void
    {
        if (property_exists($this, $dynamicName)) {
            return;
        }

        self::$extendableGuardProperties = false;

        $this->{$dynamicName} = $value;

        self::$extendableGuardProperties = true;

        $this->extensionData['dynamicProperties'][] = $dynamicName;
    }

    /**
     * 如果正在進行動態屬性操作，則返回 true
     */
    protected function extendableIsSettingDynamicProperty(): bool
    {
        return self::$extendableGuardProperties === false;
    }

    /**
     * 用指定的行為動態擴展類
     * @param string $extensionName
     * @return void
     * @throws Exception
     */
    public function extendClassWith(string $extensionName): void
    {
        if (! strlen($extensionName)) {
            return;
        }

        $extensionName = str_replace('.', '\\', trim($extensionName));

        if (isset($this->extensionData['extensions'][$extensionName])) {
            throw new Exception(sprintf(
                'Class %s has already been extended with %s',
                static::class,
                $extensionName
            ));
        }

        $this->extensionData['extensions'][$extensionName] = $extensionObject = new $extensionName($this);
        $this->extensionExtractMethods($extensionName, $extensionObject);
        $extensionObject->extensionApplyInitCallbacks();
    }

    /**
     * 檢查可擴展類是否用行為對象擴展
     * @param string $name 完全限定的行為名稱
     * @return boolean
     */
    public function isClassExtendedWith(string $name): bool
    {
        $name = str_replace('.', '\\', trim($name));
        return isset($this->extensionData['extensions'][$name]);
    }

    /**
     * 使用非干擾方式實現擴展，應與靜態 extend() 方法一起使用。
     * @param string $extensionName
     * @return void
     * @throws Exception
     */
    public function implementClassWith(string $extensionName): void
    {
        $extensionName = str_replace('.', '\\', trim($extensionName));

        if (in_array($extensionName, $this->extensionExtractImplements())) {
            return;
        }

        $this->implement[] = $extensionName;
    }

    /**
     * 檢查類是否實現了提供的接口方法。
     */
    public function isClassInstanceOf($interface): bool
    {
        $classMethods = $this->getClassMethods();

        if (is_string($interface) && ! interface_exists($interface)) {
            throw new Exception(sprintf(
                'Interface %s does not exist',
                $interface
            ));
        }

        $interfaceMethods = (array) get_class_methods($interface);
        foreach ($interfaceMethods as $methodName) {
            if (! in_array($methodName, $classMethods)) {
                return false;
            }
        }

        return true;
    }

    /**
     * 從可擴展類返回行為對象，例如：
     *
     *     $this->getClassExtension('Backend.Behaviors.FormController')
     *
     * @param string $name 完全限定的行為名稱
     * @return mixed
     */
    public function getClassExtension(string $name): mixed
    {
        $name = str_replace('.', '\\', trim($name));
        return $this->extensionData['extensions'][$name] ?? null;
    }

    /**
     * asExtension 是 `getClassExtension()` 方法的簡寫，但使用短擴展名，例如：
     *
     *     $this->asExtension('FormController')
     *
     * @param  string $shortName
     * @return mixed
     */
    public function asExtension($shortName)
    {
        foreach ($this->extensionData['extensions'] as $class => $obj) {
            if (
                preg_match('@\\\\([\w]+)$@', $class, $matches) &&
                $matches[1] === $shortName
            ) {
                return $obj;
            }
        }

        return $this->getClassExtension($shortName);
    }

    /**
     * 檢查方法是否存在，擴展等同於 method_exists()
     * @param string $name
     * @return boolean
     */
    public function methodExists(string $name): bool
    {
        return (
            method_exists($this, $name) ||
            isset($this->extensionData['methods'][$name]) ||
            isset($this->extensionData['dynamicMethods'][$name])
        );
    }

    /**
     * 獲取類方法列表，擴展等同於 get_class_methods()
     * @return array
     */
    public function getClassMethods(): array
    {
        return array_values(array_unique(array_merge(
            get_class_methods($this),
            array_keys($this->extensionData['methods']),
            array_keys($this->extensionData['dynamicMethods'])
        )));
    }

    /**
     * 獲取類方法作為反射器
     */
    public function getClassMethodAsReflector(string $name): ReflectionFunctionAbstract
    {
        $extandableMethod = $this->getExtendableMethodFromExtensions($name);
        if ($extandableMethod !== null) {
            return new ReflectionMethod($extandableMethod[0], $extandableMethod[1]);
        }

        $extandableDynamicMethod = $this->getExtendableMethodFromDynamicMethods($name);
        if ($extandableDynamicMethod !== null) {
            return new ReflectionFunction($extandableDynamicMethod);
        }

        return new ReflectionMethod($this, $name);
    }

    /**
     * 返回所有動態屬性及其值
     * @return array ['property' => 'value']
     */
    public function getDynamicProperties(): array
    {
        $result = [];

        foreach ($this->extensionData['dynamicProperties'] as $propName) {
            $result[$propName] = $this->{$propName};
        }

        return $result;
    }

    /**
     * 檢查屬性是否存在，擴展等同於 `property_exists()`
     * @param string $name
     * @return boolean
     */
    public function propertyExists(string $name): bool
    {
        if (property_exists($this, $name)) {
            return true;
        }

        foreach ($this->extensionData['extensions'] as $extensionObject) {
            if (
                property_exists($extensionObject, $name) &&
                $this->extendableIsAccessible($extensionObject, $name)
            ) {
                return true;
            }
        }

        return false;
    }

    /**
     * 檢查屬性是否可訪問，屬性等同於 `is_callable()`
     * @param mixed $class
     * @param string $propertyName
     * @return boolean
     * @throws ReflectionException
     */
    protected function extendableIsAccessible(mixed $class, string $propertyName): bool
    {
        $reflector = new ReflectionClass($class);
        $property = $reflector->getProperty($propertyName);
        return $property->isPublic();
    }

    /**
     * `__get()` 的魔術方法
     * @param string $name
     * @return string
     * @throws ReflectionException
     */
    public function extendableGet(string $name): string
    {
        foreach ($this->extensionData['extensions'] as $extensionObject) {
            if (
                property_exists($extensionObject, $name) &&
                $this->extendableIsAccessible($extensionObject, $name)
            ) {
                return $extensionObject->{$name};
            }
        }

        $parent = get_parent_class(self::class);
        if ($parent !== false && method_exists($parent, '__get')) {
            return parent::__get($name);
        }
        return "";
    }

    /**
     * `__set()` 的魔術方法
     * @param string $name
     * @param string $value
     * @return string
     */
    public function extendableSet(string $name, string $value): string
    {
        $found = false;

        // Spin over each extension to find it
        foreach ($this->extensionData['extensions'] as $extensionObject) {
            if (! property_exists($extensionObject, $name)) {
                continue;
            }

            $extensionObject->{$name} = $value;
            $found = true;
        }

        // Setting an undefined property, magic ends here since the property now exists
        if (! self::$extendableGuardProperties) {
            $this->{$name} = $value;
            return "";
        }

        // This targets trait usage in particular
        $parent = get_parent_class(self::class);
        if ($parent !== false && method_exists($parent, '__set')) {
            parent::__set($name, $value);
            $found = true;
        }

        // Undefined property, throw an exception to catch it,
        // otherwise some PHP versions will segfault
        // @deprecated Restore if year >= 2024 or v4
        // if (!$found) {
        //     throw new BadMethodCallException(sprintf(
        //         'Call to undefined property %s::%s',
        //         static::class,
        //         $name
        //     ));
        // }
        return '';
    }

    /**
     * `__call()` 的魔術方法
     * @param string $name
     * @param array|null $params
     * @return mixed
     */
    public function extendableCall(string $name, array $params = null): mixed
    {
        $callable = $this->getExtendableMethodFromExtensions($name);

        if ($callable === null) {
            $callable = $this->getExtendableMethodFromDynamicMethods($name);
        }

        if ($callable !== null) {
            return call_user_func_array($callable, $params);
        }

        $parent = get_parent_class(self::class);
        if ($parent !== false && method_exists($parent, '__call')) {
            return parent::__call($name, $params);
        }

        throw new BadMethodCallException(sprintf(
            'Call to undefined method %s::%s()',
            static::class,
            $name
        ));
    }

    /**
     * `__callStatic()` 的魔術方法
     * @param string $name
     * @param null $params
     * @return mixed
     * @throws ReflectionException
     */
    public static function extendableCallStatic(string $name, $params = null): mixed
    {
        $className = get_called_class();

        if (! array_key_exists($className, self::$extendableStaticMethods)) {
            self::$extendableStaticMethods[$className] = [];

            $class = new ReflectionClass($className);
            $defaultProperties = $class->getDefaultProperties();
            if (
                array_key_exists('implement', $defaultProperties) &&
                ($implement = $defaultProperties['implement'])
            ) {
                // Apply extensions
                if (is_string($implement)) {
                    $uses = explode(',', $implement);
                } elseif (is_array($implement)) {
                    $uses = $implement;
                } else {
                    throw new Exception(sprintf('Class %s contains an invalid $implement value', $className));
                }

                foreach ($uses as $use) {
                    $useClassName = str_replace('.', '\\', trim($use));

                    $useClass = new ReflectionClass($useClassName);
                    $staticMethods = $useClass->getMethods(ReflectionMethod::IS_STATIC);
                    foreach ($staticMethods as $method) {
                        self::$extendableStaticMethods[$className][$method->getName()] = $useClassName;
                    }
                }
            }
        }

        if (isset(self::$extendableStaticMethods[$className][$name])) {
            $extension = self::$extendableStaticMethods[$className][$name];

            if (method_exists($extension, $name) && is_callable([$extension, $name])) {
                $extension::$extendableStaticCalledClass = $className;
                $result = forward_static_call_array([$extension, $name], (array)$params);
                $extension::$extendableStaticCalledClass = null;
                return $result;
            }
        }

        // $parent = get_parent_class($className);
        // if ($parent !== false && method_exists($parent, '__callStatic')) {
        //    return parent::__callStatic($name, $params);
        // }

        throw new BadMethodCallException(sprintf(
            'Call to undefined method %s::%s()',
            $className,
            $name
        ));
    }

    /**
     * 從擴展中獲取可擴展方法
     */
    protected function getExtendableMethodFromExtensions(string $name): ?array
    {
        if (! isset($this->extensionData['methods'][$name])) {
            return null;
        }

        $extension = $this->extensionData['methods'][$name];
        $extensionObject = $this->extensionData['extensions'][$extension];

        if (! method_exists($extension, $name) || ! is_callable([$extensionObject, $name])) {
            return null;
        }

        return [$extensionObject, $name];
    }

    /**
     * 從動態方法中獲取可擴展方法
     */
    protected function getExtendableMethodFromDynamicMethods(string $name): ?callable
    {
        if (! isset($this->extensionData['dynamicMethods'][$name])) {
            return null;
        }

        $dynamicCallable = $this->extensionData['dynamicMethods'][$name];

        if (! is_callable($dynamicCallable)) {
            return null;
        }

        return $dynamicCallable;
    }
}