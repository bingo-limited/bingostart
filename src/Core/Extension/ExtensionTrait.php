<?php

namespace Bingo\Core\Extension;

/**
 * ExtensionTrait 允許使用"私有特徵"
 *
 * @package bingo\core\extension
 * @see ExtensionBase
 */
trait ExtensionTrait
{
    /**
     * @var ?string extendableStaticCalledClass 是使用靜態方法時的調用類。
     */
    public static ?string $extendableStaticCalledClass = null;

    /**
     * @var array extensionHidden 是無法訪問的屬性和方法。
     */
    protected array $extensionHidden = [
        'methods' => ['extensionIsHiddenProperty', 'extensionIsHiddenMethod'],
        'properties' => []
    ];

    /**
     * extensionApplyInitCallbacks
     */
    public function extensionApplyInitCallbacks(): void
    {
        $classes = array_merge([static::class], class_parents($this));
        foreach ($classes as $class) {
            if (isset(Container::$extensionCallbacks[$class]) && is_array(Container::$extensionCallbacks[$class])) {
                foreach (Container::$extensionCallbacks[$class] as $callback) {
                    call_user_func($callback, $this);
                }
            }
        }
    }

    /**
     * extensionExtendCallback 是 `::extend()` 靜態方法的輔助方法
     * @param callable $callback
     * @return void
     */
    public static function extensionExtendCallback(callable $callback): void
    {
        $class = get_called_class();
        if (
            ! isset(Container::$extensionCallbacks[$class]) ||
            ! is_array(Container::$extensionCallbacks[$class])
        ) {
            Container::$extensionCallbacks[$class] = [];
        }

        Container::$extensionCallbacks[$class][] = $callback;
    }

    /**
     * extensionHideMethod
     */
    protected function extensionHideMethod($name): void
    {
        $this->extensionHidden['methods'][] = $name;
    }

    /**
     * extensionHideProperty
     */
    protected function extensionHideProperty($name): void
    {
        $this->extensionHidden['properties'][] = $name;
    }

    /**
     * extensionIsHiddenMethod
     */
    public function extensionIsHiddenMethod($name): bool
    {
        return in_array($name, $this->extensionHidden['methods']);
    }

    /**
     * extensionIsHiddenProperty
     */
    public function extensionIsHiddenProperty($name): bool
    {
        return in_array($name, $this->extensionHidden['properties']);
    }

    /**
     * getCalledExtensionClass
     */
    public static function getCalledExtensionClass(): ?string
    {
        return self::$extendableStaticCalledClass;
    }
}