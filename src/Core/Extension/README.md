## Bingo 擴展

為類別添加擁有*私有特徵*的能力，也稱為行為。這些類似於原生 PHP 特徵，但有一些明顯的優勢：

1. 行為有自己的構造函數。
2. 行為可以有私有或受保護的方法。
3. 方法和屬性名稱可以安全地衝突。
4. 類別可以動態擴展行為。

您可能會這樣使用特徵：

```php
class MyClass
{
    use \Bingo\Core\UtilityFunctions;
    use \Bingo\Core\DeferredBinding;
}
```

而行為的使用方式類似：

```php
class MyClass extends \Bingo\Core\Extension\Extendable
{
    public $implement = [
        'Bingo.Core.UtilityFunctions',
        'Bingo.Core.DeferredBinding',
    ];
}
```

您可能會這樣定義特徵：

```php
trait UtilityFunctions
{
    public function sayHello()
    {
        echo "Hello from " . get_class($this);
    }
}
```

而行為的定義如下：

```php
class UtilityFunctions extends \Bingo\Core\Extension\ExtensionBase
{
    protected $parent;

    public function __construct($parent)
    {
        $this->parent = $parent;
    }

    public function sayHello()
    {
        echo "Hello from " . get_class($this->parent);
    }
}
```

被擴展的對象總是作為第一個參數傳遞給行為的構造函數。

### 使用示例

#### 行為 / 擴展類

```php
<?php namespace MyNamespace\Behaviors;

class FormController extends \Bingo\Core\Extension\ExtensionBase
{
    /**
     * @var 對擴展對象的引用。
     */
    protected $controller;

    /**
     * 構造函數
     */
    public function __construct($controller)
    {
        $this->controller = $controller;
    }

    public function someMethod()
    {
        return "我來自 FormController 行為！";
    }

    public function otherMethod()
    {
        return "你可能看不到我...";
    }
}
```

#### 擴展一個類

這個 `Controller` 類將實現 `FormController` 行為，然後這些方法將可用於該類（混入）。我們將覆蓋 `otherMethod` 方法。

```php
<?php namespace MyNamespace;

class Controller extends \Bingo\Core\Extension\Extendable
{

    /**
     * 實現 FormController 行為
     */
    public $implement = [
        'MyNamespace.Behaviors.FormController'
    ];

    public function otherMethod()
    {
        return "我來自主控制器！";
    }
}
```

#### 使用擴展

```php
$controller = new MyNamespace\Controller;

// 輸出：我來自 FormController 行為！
echo $controller->someMethod();

// 輸出：我來自主控制器！
echo $controller->otherMethod();

// 輸出：你可能看不到我...
echo $controller->asExtension('FormController')->otherMethod();
```

### 動態使用行為 / 構造函數擴展

任何使用 `Extendable` 或 `ExtendableTrait` 的類都可以通過靜態 `extend()` 方法擴展其構造函數。參數應傳遞一個閉包，該閉包將作為類構造函數的一部分被調用。例如：

```php
/**
 * 擴展 Pizza Shop 以包含 Master Splinter 行為
 */
MyNamespace\Controller::extend(function($controller){

    // 動態實現列表控制器行為
    $controller->implement[] = 'MyNamespace.Behaviors.ListController';
});
```

### 動態創建方法
可以通過使用 `addDynamicMethod` 將方法添加到 `Model` 中。

```php
Post::extend(function($model) {
    $model->addDynamicMethod('getTagsAttribute', function() use ($model) {
        return $model->tags()->lists('name');
    });
});
```

### 軟定義

如果行為類不存在，就像特徵一樣，將拋出 *Class not found* 錯誤。在某些情況下，您可能希望抑制此錯誤，以便在系統中存在模組時進行條件實現。您可以通過在類名開頭放置 `@` 符號來實現這一點。

```php
class User extends \Bingo\Core\Extension\Extendable
{
    public $implement = ['@RainLab.Translate.Behaviors.TranslatableModel'];
}
```

如果類名 `Modules\Translate\Behaviors\TranslatableModel` 不存在，將不會拋出錯誤。這相當於以下代碼：

```php
class User extends \Bingo\Core\Extension\Extendable
{
    public $implement = [];

    public function __construct()
    {
        if (class_exists('RainLab\Translate\Behaviors\TranslatableModel')) {
            $controller->implement[] = 'RainLab.Translate.Behaviors.TranslatableModel';
        }

        parent::__construct();
    }
}
```

### 使用特徵而不是基類

在某些情況下，由於其他需求，您可能不希望擴展 `ExtensionBase` 或 `Extendable` 類。因此，您可以使用特徵代替，儘管顯然行為方法將不可用於父類。

- 使用 `ExtensionTrait` 時，`ExtensionBase` 中的方法應該應用於該類。

- 使用 `ExtendableTrait` 時，`Extendable` 中的方法應該應用於該類。