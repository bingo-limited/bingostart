<?php

namespace Bingo\Core\Extension;

/**
 * Container 包含所有擴展的構造邏輯
 */
class Container
{
    /**
     * @var array classCallbacks 用於擴展可擴展類的構造函數。例如:
     *
     *     Class::extend(function($obj) { })
     *
     */
    public static array $classCallbacks = [];

    /**
     * @var array 用於擴展擴展類的構造函數。例如:
     *
     *     BehaviorClass::extend(function($obj) { })
     *
     */
    public static array $extensionCallbacks = [];

    /**
     * extendClass 在不包含類的情況下擴展一個類
     */
    public static function extendClass(string $class, callable $callback): void
    {
        self::$classCallbacks[$class][] = $callback;
    }

    /**
     * extendBehavior 在不包含類的情況下擴展一個類
     */
    public static function extendBehavior(string $class, callable $callback): void
    {
        self::$extensionCallbacks[$class][] = $callback;
    }

    /**
     * clearExtensions 清除已擴展類的列表,以便它們可以重新擴展
     */
    public static function clearExtensions(): void
    {
        self::$classCallbacks = [];
        self::$extensionCallbacks = [];
    }
}
