<?php

namespace Bingo\Core\Mail;

use App;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Message;
use Illuminate\Queue\SerializesModels;
use Illuminate\Mail\Mailable as MailableBase;

/**
 * Generic mailable class.
 *
 */
class Mailable extends MailableBase
{
    use Queueable;
    use SerializesModels;

    /**
     * @var string siteContext is the active site for this mail message.
     */
    public string $siteContext;

    /**
     * @var string forceMailer forces the mailer to use.
     */
    public string $forceMailer;

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build(): static
    {
        return $this;
    }

    /**
     * Build the view data for the message.
     *
     * @return array
     */
    public function buildViewData(): array
    {
        $data = $this->viewData;

        foreach ($data as $param => $value) {
            $data[$param] = $this->getRestoredPropertyValue($value);
        }

        return $data;
    }

    /**
     * Set serialized view data for the message.
     *
     * @param array $data
     * @return $this
     */
    public function withSerializedData($data)
    {
        $this->viewData['_current_locale'] = $this->locale ?: App::getLocale();

        //        $this->viewData['_current_site'] = $this->siteContext ?: Site::getSiteIdFromContext();

        foreach ($data as $param => $value) {
            $this->viewData[$param] = $this->getSerializedPropertyValue($value);
        }

        return $this;
    }

    /**
     * Set the subject for the message.
     *
     * @param Message $message
     * @return $this
     */
    protected function buildSubject($message): static
    {
        if ($this->subject) {
            $message->subject($this->subject);
        }

        return $this;
    }

    /**
     * siteContext sets the site context of the message.
     *
     * @param string $siteId
     * @return $this
     */
    public function siteContext(string $siteId): static
    {
        $this->siteContext = $siteId;

        return $this;
    }

    /**
     * withLocale acts as a hook to also apply the site context
     *
     * @param string $locale
     * @param \Closure $callback
     * @return mixed
     */
    public function withLocale($locale, $callback): mixed
    {
        if (! $this->siteContext) {
            return parent::withLocale($locale, $callback);
        }

        //        return Site::withContext($this->siteContext, function() use ($locale, $callback) {
        //            return parent::withLocale($locale, $callback);
        //        });
        return null;
    }

    /**
     * forceMailer forces sending using a different mail driver, useful if lazy loading
     * the mail driver configuration for multisite.
     * @param string $mailer
     * @return $this
     */
    public function forceMailer(string $mailer): static
    {
        $this->forceMailer = $mailer;

        return $this;
    }

    /**
     * mailer sets the name of the mailer that should send the message.
     * @param string $mailer
     * @return $this
     */
    public function mailer($mailer): static
    {
        $this->mailer = $this->forceMailer ?: $mailer;

        return $this;
    }
}
