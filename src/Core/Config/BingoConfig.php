<?php

namespace Bingo\Core\Config;

use Bingo\Core\Util\SerializeUtil;

/**
 * 提供统一的配置读写接口，都是操作string
 */
abstract class BingoConfig
{
    public const DEFAULT_LANG = 'zh_CN';

    /**
     * 获取配置（进出都要是string）
     * @param $key string 配置key
     * @param string $lang 语言
     * @param $defaultValue string 请勿使用 null 会导致缓存不能生效
     * @param $useCache bool 是否使用缓存
     * @return string
     */
    abstract public function get(string $key, string $lang = self::DEFAULT_LANG, string $defaultValue = '', bool $useCache = true): string;

    /**
     * 设置配置（值需要是string）
     * @param $key string
     * @param $value string
     * @param string $lang
     * @return void
     */
    abstract public function set(string $key, string $value, string $lang = self::DEFAULT_LANG): void;

    abstract public function remove($key, string $lang = self::DEFAULT_LANG);

    abstract public function has($key, string $lang = self::DEFAULT_LANG);

    abstract public function all(string $lang = null, $prefix = null);

    public function getWithEnv($key, string $lang = self::DEFAULT_LANG, $defaultValue = null)
    {
        $value = config('env.CONFIG_'.$key.$lang);
        if (null === $value) {
            $value = $this->get($key);
        }
        if (empty($value)) {
            return $defaultValue;
        }
        return $value;
    }

    public function setArray($key, $value, string $lang = self::DEFAULT_LANG): void
    {
        $this->set($key, SerializeUtil::jsonEncode($value), $lang);
    }

    public function getArray($key, string $lang = self::DEFAULT_LANG, $defaultValue = [], $useCache = true)
    {
        $value = $this->get($key, $lang, SerializeUtil::jsonEncode($defaultValue), $useCache);
        $value = @json_decode($value, true);
        if (! is_array($value) || empty($value)) {
            $value = [];
        }
        return $value;
    }

    public function getBoolean($key, string $lang = self::DEFAULT_LANG, $defaultValue = false): bool
    {
        $value = $this->get($key, $lang, null);
        return (bool) $value;
    }

    public function getInteger($key, string $lang = self::DEFAULT_LANG, $defaultValue = 0): int
    {
        $value = $this->get($key, $lang, null);
        return intval($value);
    }

    public function getString($key, string $lang = self::DEFAULT_LANG, $defaultValue = ''): string
    {
        return $this->get($key, $lang, null);
    }
}
