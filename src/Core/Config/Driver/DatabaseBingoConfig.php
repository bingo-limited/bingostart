<?php

namespace Bingo\Core\Config\Driver;

use Bingo\App\Admin\Model\Config;
use Bingo\Core\Config\BingoConfig;
use Bingo\Core\Dao\ModelUtil;
use Illuminate\Support\Facades\Cache;

class DatabaseBingoConfig extends BingoConfig
{
    public const CACHE_PREFIX = 'config:';

    public function get($key, $lang = self::DEFAULT_LANG, $defaultValue = '', $useCache = true): string
    {
        if (null === $defaultValue) {
            $defaultValue = '';
        }

        $cacheFlag = self::CACHE_PREFIX . $key . ':' . $lang;

        if ($useCache) {
            $value = Cache::get($cacheFlag);
            if ($value !== null) {
                return $value ?: $defaultValue;
            }
        }

        // 构建查询条件
        $conditions = ['key' => $key];
        if (!empty($lang)) {  // 只有当 lang 不为空时才添加条件
            $conditions['lang'] = $lang;
        }

        $config = ModelUtil::get('config', $conditions);

        if ($config && isset($config['value'])) {
            $value = $config['value'];
            if ($useCache) {
                Cache::put($cacheFlag, $value, now()->addDay());
            }
            return $value ?: $defaultValue;
        }

        // 查询不到时,设置一个较短的缓存时间,避免缓存穿透
        if ($useCache) {
            Cache::put($cacheFlag, $defaultValue, now()->addMinutes(5));
        }

        return $defaultValue;
    }

    public function set($key, $value, $lang = self::DEFAULT_LANG): void
    {
        $config = ModelUtil::get('config', ['key' => $key, 'lang' => $lang]);
        if ($config) {
            ModelUtil::update('config', ['id' => $config['id']], ['value' => $value]);
        } else {
            ModelUtil::insert('config', ['key' => $key, 'value' => $value, 'lang' => $lang]);
        }
        $cacheFlag = self::CACHE_PREFIX.$key . ':' . $lang;
        Cache::forget($cacheFlag);
    }

    public function remove($key, string $lang = self::DEFAULT_LANG): void
    {
        $config = ModelUtil::get('config', ['key' => $key, 'lang' => $lang]);
        if ($config) {
            ModelUtil::delete('config', ['id' => $config['id']]);
        }
        $cacheFlag = self::CACHE_PREFIX.$key . ':' . $lang;
        Cache::forget($cacheFlag);
    }

    public function has($key, string $lang = self::DEFAULT_LANG): bool
    {
        $config = ModelUtil::get('config', ['key' => $key, 'lang' => $lang]);
        return ! ! $config;
    }

    public function all($lang = null, $prefix = null): array
    {
        $query = Config::query();
        if (null !== $lang) {
            $query->where('lang', $lang);
        }
        if (null !== $prefix) {
            $query->where('key', 'like', $prefix.'%');
        }
        return $query->get(['key', 'value'])->toArray();
    }
}
