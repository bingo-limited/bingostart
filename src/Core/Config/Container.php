<?php

namespace Bingo\Core\Config;

/**
 * Container holds constructor logic for all extensions
 */
class Container
{
    /**
     * @var array classCallbacks is used to extend the constructor of an extendable class. Eg:
     *
     *     Class::extend(function($obj) { })
     *
     */
    public static array $classCallbacks = [];

    /**
     * @var array Used to extend the constructor of an extension class. Eg:
     *
     *     BehaviorClass::extend(function($obj) { })
     *
     */
    public static array $extensionCallbacks = [];

    /**
     * extendClass extends a class without including it
     */
    public static function extendClass(string $class, callable $callback): void
    {
        self::$classCallbacks[$class][] = $callback;
    }

    /**
     * extendBehavior extends a class without including it
     */
    public static function extendBehavior(string $class, callable $callback): void
    {
        self::$extensionCallbacks[$class][] = $callback;
    }

    /**
     * clearExtensions clears the list of extended classes, so they will be re-extended
     */
    public static function clearExtensions(): void
    {
        self::$classCallbacks = [];
        self::$extensionCallbacks = [];
    }
}
