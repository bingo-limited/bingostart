<?php

namespace Bingo\Core\Config;

use Bingo\Core\Element\ElementBase;
use Bingo\Core\Element\Navigation\ItemDefinition;
use Bingo\Facade\Html;

/**
 * SettingsMenuItem
 *
 * @method self owner(string $owner) 設置擁有者
 * @method self iconSvg(?string $iconSvg) 設置 SVG 圖標
 * @method self counter(mixed $counter) 設置計數器
 * @method self counterLabel(?string $counterLabel) 設置計數器標籤
 * @method self attributes(array $attributes) 設置屬性
 * @method self permissions(array $permissions) 設置權限
 * @method self context(string $context) 設置上下文，如 system, mysettings
 * @method self class(string $class) 設置模型或其他管理記錄的類
 * @method self size(string $size) 設置大小，如 tiny, small, medium, large, huge, giant, adaptive
 */
class SettingsMenuItem extends ItemDefinition
{
    /**
     * @var array $config
     */
    public array $config = [];

    /**
     * 初始化默認值
     */
    protected function initDefaultValues(): void
    {
        parent::initDefaultValues();

        $this->order(500)
            ->context('system')
            ->size('medium')
            ->attributes([])
            ->permissions([]);
    }

    /**
     * 返回列表項的 HTML 屬性
     */
    public function itemAttributes(): string
    {
        return $this->config['attributes'] ? Html::attributes(array_except($this->config['attributes'], ['target'])) : '';
    }

    /**
     * 返回錨鏈接的 HTML 屬性
     */
    public function linkAttributes(): string
    {
        return isset($this->config['attributes']['target']) ? Html::attributes(array_only($this->config['attributes'], ['target'])) : '';
    }

    /**
     * 設置配置值
     *
     * @param string $name
     * @param $arguments
     * @return self
     */
    public function __call($name, $arguments): ElementBase
    {
        $this->config[$name] = $arguments[0] ?? null;
        return $this;
    }

    /**
     * 獲取配置值
     *
     * @param string $name
     * @return mixed
     */
    public function __get($name): mixed
    {
        return $this->config[$name] ?? null;
    }
}