<?php

declare(strict_types=1);

namespace Bingo\Core\Permission;

class PermissionDto
{
    public function __construct(
        public string $permissionName,
        public string $permissionMark,
        public ?string $parentId,
        public string $route,
        public string $icon,
        public string $module,
        public string $component,
        public int $type,
        public int $sort
    ) {
    }

    public function toArray(): array
    {
        return [
            'permission_name' => $this->permissionName,
            'permission_mark' => $this->permissionMark,
            'parent_id' => $this->parentId,
            'route' => $this->route,
            'icon' => $this->icon,
            'module' => $this->module,
            'component' => $this->component,
            'type' => $this->type,
            'sort' => $this->sort,
        ];
    }
}
