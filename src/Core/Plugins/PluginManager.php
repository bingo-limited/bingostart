<?php

namespace Bingo\Core\Plugins;

use Db;
use App;
use Exception;
use Str;
use File;
use Log;
use Config;
use Schema;
use System;
use Bingo\Core\Composer\Manager as ComposerManager;
use RecursiveIteratorIterator;
use RecursiveDirectoryIterator;
use Throwable;

/**
 * 插件管理器
 * @package Bingo\Core\Plugins
 */
class PluginManager
{
    /**
     * 用于存储清单的常量键
     */
    public const MANIFEST_PLUGINS = 'plugins.all';
    public const MANIFEST_PLUGIN_HINTS = 'plugins.hints';

    /**
     * @var App 应用实例，因为插件是服务提供者的扩展
     */
    protected mixed $app;

    /**
     * @var array 插件容器对象，用于存储插件信息对象。
     */
    protected array $plugins;

    /**
     * @var array 插件的路径映射。
     */
    protected array $pathMap = [];

    /**
     * @var bool registered 检查是否所有插件都已调用register()方法。
     */
    protected bool $registered = false;

    /**
     * @var bool booted 检查是否所有插件都已调用boot()方法。
     */
    protected bool $booted = false;

    /**
     * @var string metaFile 禁用文件的路径。
     */
    protected string $metaFile;

    /**
     * @var array disabledPlugins 禁用的插件集合
     */
    protected array $disabledPlugins = [];

    /**
     * @var array registrationMethodCache 注册方法结果的缓存。
     */
    protected array $registrationMethodCache = [];

    /**
     * 类的构造函数
     */
    public function __construct()
    {
        $this->app = App::make('app');
        $this->metaFile = cache_path('cms/disabled.php');
        $this->loadDisabled();
        $this->loadPlugins();

        if ($this->app->runningInBackend()) {
            $this->loadDependencies();
        }
    }

    /**
     * 创建这个单例的新实例
     */
    public static function instance(): static
    {
        return App::make('system.plugins');
    }

    /**
     * loadPlugins 查找所有可用插件并将它们加载到$plugins数组中
     */
    public function loadPlugins(): array
    {
        $this->plugins = [];

        // Locate all plugins and binds them to the container
        foreach ($this->getPluginNamespaces() as $namespace => $path) {
            $this->loadPlugin($namespace, plugins_path($path));
        }

        $this->sortDependencies();

        return $this->plugins;
    }

    /**
     * unloadPlugins 卸载所有插件：loadPlugins的反操作
     */
    public function unloadPlugins(): void
    {
        $this->plugins = [];
    }

    /**
     * reloadPlugins 重新加载所有插件
     */
    public function reloadPlugins(): void
    {
        $this->unloadPlugins();
        $this->loadPlugins();
    }

    /**
     * loadPlugin 加载单个插件到管理器，其中命名空间是Acme\Blog，路径是磁盘上的某个位置
     */
    public function loadPlugin(string $namespace, string $path)
    {
        $className = $namespace.'\Plugin';
        $classPath = $path.'/Plugin.php';

        try {
            // Autoloader failed?
            if (! class_exists($className)) {
                include_once $classPath;
            }

            // Not a valid plugin!
            if (! class_exists($className)) {
                return;
            }

            $classObj = new $className($this->app);
        } catch (Throwable $e) {
            Log::error('Plugin '.$className.' could not be instantiated.', [
                'message' => $e->getMessage(),
                'file' => $e->getFile(),
                'line' => $e->getLine(),
                'trace' => $e->getTraceAsString()
            ]);
            return;
        }

        $classId = $this->getIdentifier($classObj);

        // Check for disabled plugins
        if ($this->isDisabled($classId)) {
            $classObj->disabled = true;
        }

        $this->plugins[$classId] = $classObj;
        $this->pathMap[$classId] = $path;

        return $classObj;
    }

    /**
     * registerFromProvider 确保在单元测试中不强制加载插件
     */
    public function registerFromProvider(): void
    {
        if ($this->app->runningUnitTests()) {
            return;
        }

        $this->registerAll();
    }

    /**
     * registerAll 在所有插件上运行register()方法，只能调用一次
     */
    public function registerAll($force = false): void
    {
        if ($this->registered && ! $force) {
            return;
        }

        foreach ($this->plugins as $pluginId => $plugin) {
            $this->registerPlugin($plugin, $pluginId);
        }

        $this->registered = true;
    }

    /**
     * registerPlugin 为单个对象注册插件
     * @param PluginBase $plugin
     * @param string|null $pluginId
     * @return void
     */
    public function registerPlugin(PluginBase $plugin, string $pluginId = null): void
    {
        if (! $pluginId) {
            $pluginId = $this->getIdentifier($plugin);
        }

        if (! $plugin || $plugin->disabled) {
            return;
        }

        $pluginPath = $this->getPluginPath($plugin);
        $pluginNamespace = strtolower($pluginId);

        // 注册插件类自动加载器
        $autoloadPath = $pluginPath.'/vendor/autoload.php';
        if (is_file($autoloadPath)) {
            ComposerManager::instance()->autoload($pluginPath.'/vendor');
        }

        // 注册配置路径
        $configPath = $pluginPath.'/config';
        if (! $this->app->configurationIsCached() && is_dir($configPath)) {
            Config::package($pluginNamespace, $configPath);
        }

        // 注册视图路径
        $viewsPath = $pluginPath.'/views';
        if (is_dir($viewsPath)) {
            $this->callAfterResolving('view', function ($view) use ($pluginNamespace, $viewsPath) {
                $view->addNamespace($pluginNamespace, $viewsPath);
            });
        }

        // 注册语言命名空间
        $langPath = $pluginPath.'/lang';
        if (is_dir($langPath)) {
            $this->callAfterResolving('translator', function ($translator) use ($pluginNamespace, $langPath) {
                $translator->addNamespace($pluginNamespace, $langPath);
                if (App::runningInBackend()) {
                    $translator->addJsonPath($langPath);
                }
            });
        }

        // Run the plugin's register() method
        $plugin->register();

        // Add init, if available
        $initFile = $pluginPath.'/init.php';
        if (file_exists($initFile)) {
            require $initFile;
        }

        // Add routes, if available
        $routesFile = $pluginPath.'/routes.php';
        if (! $this->app->routesAreCached() && file_exists($routesFile)) {
            require $routesFile;
        }
    }

    /**
     * bootFromProvider 确保在单元测试中不强制启动插件
     */
    public function bootFromProvider(): void
    {
        if ($this->app->runningUnitTests()) {
            return;
        }

        $this->bootAll();
    }

    /**
     * bootAll 在所有插件上运行boot()方法。只能调用一次。
     */
    public function bootAll($force = false): void
    {
        if ($this->booted && ! $force) {
            return;
        }

        foreach ($this->plugins as $pluginId => $plugin) {
            $this->bootPlugin($plugin, $pluginId);
        }

        $this->booted = true;
    }

    /**
     * bootPlugin 注册单个插件对象。
     * @param PluginBase $plugin
     * @param null $pluginId
     * @return void
     */
    public function bootPlugin(PluginBase $plugin, $pluginId = null): void
    {
        if (! $pluginId) {
            $pluginId = $this->getIdentifier($plugin);
        }

        if (! $plugin || $plugin->disabled) {
            return;
        }

        $plugin->boot();
    }

    /**
     * callAfterResolving 设置一个after resolving监听器，或者如果已经解析，则立即触发。
     */
    protected function callAfterResolving($name, $callback): void
    {
        $this->app->afterResolving($name, $callback);

        if ($this->app->resolved($name)) {
            $callback($this->app->make($name), $this->app);
        }
    }

    /**
     * getPluginPaths 返回插件及其路径的数组。
     */
    public function getPluginPaths(): array
    {
        return array_diff_key($this->pathMap, $this->disabledPlugins);
    }

    /**
     * getPluginPath 返回插件的目录路径
     */
    public function getPluginPath($id)
    {
        $classId = $this->getIdentifier($id);

        if (! isset($this->pathMap[$classId])) {
            return;
        }

        return File::normalizePath($this->pathMap[$classId]);
    }

    /**
     * getComposerCode 查找插件的composer代码
     */
    public function getComposerCode($id)
    {
        $path = $this->getPluginPath($id);
        $file = $path.'/composer.json';

        if (! $path || ! file_exists($file)) {
            return;
        }

        $info = json_decode(File::get($file), true);

        return $info['name'] ?? null;
    }

    /**
     * exists 检查插件是否存在并启用
     * @param string $id 插件标识符，例如：Namespace.PluginName
     * @return boolean
     */
    public function exists(string $id): bool
    {
        return ! (! $this->findByIdentifier($id) || $this->isDisabled($id));
    }

    /**
     * getPlugins 返回所有已注册插件的数组
     *  索引是插件命名空间，值是插件信息对象。
     */
    public function getPlugins(): array
    {
        return array_diff_key($this->plugins, $this->disabledPlugins);
    }

    /**
     * getAllPlugins 不考虑启用状态返回所有插件
     */
    public function getAllPlugins(): ?array
    {
        return $this->plugins;
    }

    /**
     * findByNamespace 根据其命名空间(Author\Plugin)返回一个插件注册类
     */
    public function findByNamespace($namespace)
    {
        if (! $this->hasPlugin($namespace)) {
            return;
        }

        $classId = $this->getIdentifier($namespace);

        return $this->plugins[$classId];
    }

    /**
     * findByIdentifier 根据其标识符(Author.Plugin)返回一个插件注册类
     */
    public function findByIdentifier($identifier)
    {
        if (! isset($this->plugins[$identifier])) {
            $identifier = $this->normalizeIdentifier($identifier);
        }

        if (! isset($this->plugins[$identifier])) {
            return;
        }

        return $this->plugins[$identifier];
    }

    /**
     * hasPlugin 检查是否已注册一个插件
     */
    public function hasPlugin($namespace): bool
    {
        $classId = $this->getIdentifier($namespace);

        $normalized = $this->normalizeIdentifier($classId);

        return isset($this->plugins[$normalized]);
    }

    /**
     * getPluginNamespace 返回一个插件的命名空间
     */
    public function getPluginNamespace($id): ?string
    {
        if ($classObj = $this->findByIdentifier($id)) {
            return dirname(get_class($classObj));
        }

        return null;
    }

    /**
     * getPluginNamespaces 返回一个供应商插件命名空间及其路径的扁平数组
     */
    public function getPluginNamespaces(): array
    {
        if (Manifest::has(self::MANIFEST_PLUGINS)) {
            return (array) Manifest::get(self::MANIFEST_PLUGINS);
        }

        $classNames = [];

        foreach ($this->getVendorAndPluginNames() as $vendorName => $vendorList) {
            foreach ($vendorList as $pluginName => $pluginPath) {
                $namespace = strtolower($vendorName).'\\'.strtolower($pluginName);
                $classNames[$namespace] = $pluginPath;
            }
        }

        Manifest::put(self::MANIFEST_PLUGINS, $classNames);

        return $classNames;
    }

    /**
     * getPluginHints 返回所有已注册提示的数组，其中键是提示名称，值是插件标识符。
     */
    public function getPluginHints(): array
    {
        if (Manifest::has(self::MANIFEST_PLUGIN_HINTS)) {
            return (array) Manifest::get(self::MANIFEST_PLUGIN_HINTS);
        }

        $hintNames = [];

        foreach ($this->getPlugins() as $pluginId => $plugin) {
            if ($hintName = $plugin->pluginDetails()['hint'] ?? null) {
                $hintNames[$hintName] = $pluginId;
            }
        }

        Manifest::put(self::MANIFEST_PLUGIN_HINTS, $hintNames);

        return $hintNames;
    }

    /**
     * getVendorAndPluginNames 返回供应商及其插件的二维数组。
     */
    public function getVendorAndPluginNames(): array
    {
        $plugins = [];

        $dirPath = plugins_path();
        if (! is_dir($dirPath)) {
            return $plugins;
        }

        $it = new RecursiveIteratorIterator(
            new RecursiveDirectoryIterator($dirPath, RecursiveDirectoryIterator::FOLLOW_SYMLINKS)
        );
        $it->setMaxDepth(2);
        $it->rewind();

        while ($it->valid()) {
            if (($it->getDepth() > 1) && $it->isFile() && (strtolower($it->getFilename()) === "plugin.php")) {
                $filePath = dirname($it->getPathname());
                $pluginName = basename($filePath);
                $vendorName = basename(dirname($filePath));
                $plugins[$vendorName][$pluginName] = "$vendorName/$pluginName";
            }

            $it->next();
        }

        return $plugins;
    }

    /**
     * getIdentifier 从插件类名或对象解析插件标识符。传入一个插件类名或对象，返回Vendor.Plugin格式的标识符
     * @param mixed $namespace
     * @return string
     */
    public function getIdentifier(mixed $namespace): string
    {
        $namespace = Str::normalizeClassName($namespace);

        // Value provided is a namespace, convert to a code
        if (strpos($namespace, '\\') !== null) {
            $parts = explode('\\', $namespace);
            $slice = array_slice($parts, 0, 2);
            $namespace = implode('.', $slice);
        }

        return $namespace;
    }

    /**
     * normalizeIdentifier 将人类插件代码(acme.blog)转化为真实的(Acme.Blog)
     * @param string $identifier
     * @return string
     */
    public function normalizeIdentifier(string $identifier): string
    {
        foreach ($this->plugins as $id => $object) {
            if (strtolower($id) === strtolower($identifier)) {
                return $id;
            }
        }

        return $identifier;
    }

    /**
     * getRegistrationMethodValues 遍历每个插件对象并收集方法调用的结果。
     * @param string $methodName
     * @return array
     */
    public function getRegistrationMethodValues(string $methodName): array
    {
        if (isset($this->registrationMethodCache[$methodName])) {
            return $this->registrationMethodCache[$methodName];
        }

        $results = [];

        // Load module items
        foreach (System::listModules() as $module) {
            if ($provider = App::getProvider($module.'\\ServiceProvider')) {
                if (method_exists($provider, $methodName)) {
                    $results['Bingo.'.$module] = $provider->{$methodName}();
                }
            }
        }

        // Load plugin items
        foreach ($this->getPlugins() as $id => $plugin) {
            if (method_exists($plugin, $methodName)) {
                $results[$id] = $plugin->{$methodName}();
            }
        }

        // Load app items
        if ($app = App::getProvider(\App\Provider::class)) {
            if (method_exists($app, $methodName)) {
                $results['Bingo.App'] = $app->{$methodName}();
            }
        }

        return $this->registrationMethodCache[$methodName] = $results;
    }

    //
    // Disability 插件禁用相关方法
    //

    /**
     * listDisabledByConfig
     */
    public function listDisabledByConfig(): array
    {
        $disablePlugins = Config::get('system.disable_plugins');

        if (! $disablePlugins) {
            return [];
        } elseif (is_array($disablePlugins)) {
            return $disablePlugins;
        } else {
            return array_map('trim', explode(',', (string) $disablePlugins));
        }
    }

    /**
     * reloadDisabledCache
     */
    public function reloadDisabledCache(): void
    {
        $this->clearDisabledCache();
        $this->disabledPlugins = [];

        $this->loadDisabled();
        $this->loadPlugins();
        $this->loadDependencies();
    }

    /**
     * clearDisabledCache
     */
    public function clearDisabledCache(): void
    {
        File::delete($this->metaFile);
    }

    /**
     * loadDisabled 从元文件加载所有禁用的插件。
     */
    protected function loadDisabled(): void
    {
        $path = $this->metaFile;

        foreach ($this->listDisabledByConfig() as $disabled) {
            $this->disabledPlugins[$disabled] = true;
        }

        if (file_exists($path)) {
            $disabled = (array) (File::getRequire($path) ?: []);
            $this->disabledPlugins = array_merge($this->disabledPlugins, $disabled);
        } else {
            $this->populateDisabledPluginsFromDb();
            $this->writeDisabled();
        }
    }

    /**
     * isDisabled 确定插件是否被禁用，通过查看元信息或应用配置。
     * @param $id
     * @return boolean
     */
    public function isDisabled($id): bool
    {
        $code = $this->getIdentifier($id);

        return (bool) (array_key_exists($code, $this->disabledPlugins))



        ;
    }

    /**
     * writeDisabled 将禁用的插件写入元文件。
     */
    protected function writeDisabled(): void
    {
        File::put(
            $this->metaFile,
            '<?php return '.var_export($this->disabledPlugins, true).';'
        );
    }

    /**
     * populateDisabledPluginsFromDb 从数据库中填充禁用插件的信息
     * @return void
     */
    protected function populateDisabledPluginsFromDb(): void
    {
        if (! $this->app->hasDatabase()) {
            return;
        }

        if (! Schema::hasTable('system_plugin_versions')) {
            return;
        }

        $disabled = Db::table('system_plugin_versions')->where('is_disabled', 1)->pluck('code')->all();

        foreach ($disabled as $code) {
            $this->disabledPlugins[$code] = true;
        }
    }

    /**
     * disablePlugin 禁用系统中的单个插件。
     * @param string $id 插件代码/命名空间
     * @param bool $isUser 如果由用户禁用，则设为true
     * @return bool
     */
    public function disablePlugin(string $id, bool $isUser = false): bool
    {
        $code = $this->getIdentifier($id);
        if (array_key_exists($code, $this->disabledPlugins)) {
            return false;
        }

        $this->disabledPlugins[$code] = $isUser;
        $this->writeDisabled();

        if ($pluginObj = $this->findByIdentifier($code)) {
            $pluginObj->disabled = true;
        }

        return true;
    }

    /**
     * enablePlugin 启用系统中的单个插件。
     * @param string $id 插件代码/命名空间
     * @param bool $isUser 如果由用户启用，则设为true
     * @return bool
     */
    public function enablePlugin(string $id, bool $isUser = false): bool
    {
        $code = $this->getIdentifier($id);
        if (! array_key_exists($code, $this->disabledPlugins)) {
            return false;
        }

        // Prevent system from enabling plugins disabled by the user
        if (! $isUser && $this->disabledPlugins[$code] === true) {
            return false;
        }

        unset($this->disabledPlugins[$code]);
        $this->writeDisabled();

        if ($pluginObj = $this->findByIdentifier($code)) {
            $pluginObj->disabled = false;
        }

        return true;
    }

    //
    // Dependencies 依赖相关方法
    //

    /**
     * findMissingDependencies 扫描系统插件以定位当前未安装的依赖项。返回所需插件代码的数组。
     *
     *     PluginManager::instance()->findMissingDependencies();
     *
     * @return array
     */
    public function findMissingDependencies(): array
    {
        $missing = [];

        foreach ($this->plugins as $id => $plugin) {
            $required = $this->getDependencies($plugin);
            if (! $required) {
                continue;
            }

            foreach ($required as $require) {
                if (! $require || $this->hasPlugin($require)) {
                    continue;
                }

                if (! in_array($require, $missing)) {
                    $missing[] = $require;
                }
            }
        }

        return $missing;
    }

    /**
     * loadDependencies 检查所有插件及其依赖项，如果不满足则禁用插件，反之亦然。
     * @return void
     */
    protected function loadDependencies(): void
    {
        foreach ($this->plugins as $id => $plugin) {
            $required = $this->getDependencies($plugin);
            if (! $required) {
                continue;
            }

            $disable = false;

            foreach ($required as $require) {
                if (! $pluginObj = $this->findByIdentifier($require)) {
                    $disable = true;
                } elseif ($pluginObj->disabled) {
                    $disable = true;
                }
            }

            if ($disable) {
                $this->disablePlugin($id);
            } else {
                $this->enablePlugin($id);
            }
        }
    }

    /**
     * sortDependencies 对插件集合进行排序，根据给定的依赖关系，依赖最少的排在前面。
     * @return array 已排序的插件标识符集
     * @throws Exception
     */
    protected function sortDependencies(): array
    {
        ksort($this->plugins);

        // Canvas the dependency tree
        $checklist = $this->plugins;
        $result = [];

        $loopCount = 0;
        while (count($checklist)) {
            if (++$loopCount > 2048) {
                throw new Exception('Too much recursion! Check for circular dependencies in your plugins.');
            }

            foreach ($checklist as $code => $plugin) {
                // Get dependencies and remove any aliens
                $depends = $this->getDependencies($plugin) ?: [];
                $depends = array_filter($depends, function ($pluginCode) {
                    return isset($this->plugins[$pluginCode]);
                });

                // No dependencies
                if (! $depends) {
                    $result[] = $code;
                    unset($checklist[$code]);
                    continue;
                }

                // Find dependencies that have not been checked
                $depends = array_diff($depends, $result);
                if (count($depends) > 0) {
                    continue;
                }

                // All dependencies are checked
                $result[] = $code;
                unset($checklist[$code]);
            }
        }

        // Reassemble plugin map
        $sortedPlugins = [];

        foreach ($result as $code) {
            $sortedPlugins[$code] = $this->plugins[$code];
        }

        return $this->plugins = $sortedPlugins;
    }

    /**
     * getDependencies 返回所提供插件所需的插件标识符。
     * @param string $plugin 插件标识符，对象或类
     * @return false|array|null
     */
    public function getDependencies(string $plugin): false|array|null
    {
        if ((! $plugin = $this->findByIdentifier($plugin))) {
            return false;
        }

        if (! isset($plugin->require) || ! $plugin->require) {
            return null;
        }

        return is_array($plugin->require) ? $plugin->require : [$plugin->require];
    }

    //
    // 管理
    //

    /**
     * deletePlugin 完全回滚并从系统中删除一个插件。
     * @param string $id 插件代码/命名空间
     * @return void
     */
    public function deletePlugin(string $id): void
    {
        // 回滚插件
        UpdateManager::instance()->rollbackPlugin($id);

        // 从文件系统中删除
        if ($pluginPath = self::instance()->getPluginPath($id)) {
            File::deleteDirectory($pluginPath);
        }
    }

    /**
     * refreshPlugin 拆除一个插件的数据库表并重新建立它们。
     * @param string $id 插件代码/命名空间
     * @return void
     */
    public function refreshPlugin($id): void
    {
        $manager = UpdateManager::instance();
        $manager->rollbackPlugin($id);
        $manager->migratePlugin($id);
    }

    /**
     * @deprecated 使用 unloadPlugins 代替
     */
    public function unregisterAll(): void
    {
        $this->registered = false;
        $this->plugins = [];
    }

}
