<?php

namespace Bingo\Core\Monitor;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Bingo\Core\Util\ArrayUtil;

class DatabaseMonitor
{
    private static int $queryCountPerRequest = 0;
    private static array $queryCountPerRequestSql = [];

    public static function init(): void
    {
        if (! config('bingo.trackPerformance', false)) {
            return;
        }
        self::$queryCountPerRequest = 0;
        self::$queryCountPerRequestSql = [];
        try {
            DB::listen(function ($query, $bindings = null, $time = null) {
                self::$queryCountPerRequest++;
                $sql = $query;
                self::$queryCountPerRequestSql[] = [
                    'sql' => $sql,
                    'bindings' => $bindings,
                ];
                // Log::info("SQL $sql, " . json_encode($bindings));
                if ($time > config('bingo.trackLongSqlThreshold', 5000)) {
                    Log::warning("LONG_SQL {$time}ms, $sql, ".ArrayUtil::serializeForLog($bindings));
                }
            });
        } catch (\Exception) {
        }
    }

    public static function getQueryCountPerRequest(): int
    {
        return self::$queryCountPerRequest;
    }

    public static function getQueryCountPerRequestSql(): array
    {
        foreach (self::$queryCountPerRequestSql as $i => $v) {
            if (is_array($v)) {
                $bindings = ArrayUtil::serializeForLog($v['bindings']);
                self::$queryCountPerRequestSql[$i] = $v['sql'].', '.$bindings;
            }
        }
        return self::$queryCountPerRequestSql;
    }

}
