<?php

namespace Bingo\Core\Widgets;

use Illuminate\Contracts\Support\Arrayable;
use Illuminate\Support\Arr;
use Illuminate\Support\Str;

abstract class WidgetBase implements Arrayable
{
    /**
     * @var array<string, mixed> Widget 配置
     */
    protected array $config = [];

    /**
     * @var string Widget 的別名
     */
    protected string $alias;

    /**
     * @var array<string> 可用的 Widget 權限
     */
    protected array $permissions = [];

    /**
     * @var bool Widget 是否可見
     */
    protected bool $visible = true;

    /**
     * WidgetBase 構造函數
     *
     * @param array<string, mixed> $config Widget 的配置
     */
    public function __construct(array $config = [])
    {
        $this->config = array_merge($this->defaultConfig(), $config);
    }


    /**
     * 初始化 Widget
     */
    protected function init(): void
    {
        // 子類可以根據需要重寫此方法進行初始化操作
    }

    /**
     * 獲取 Widget 的配置
     *
     * @param string|null $key 配置鍵
     * @param mixed|null $default 默認值
     * @return mixed
     */
    public function getConfig(string $key = null, mixed $default = null): mixed
    {
        if ($key === null) {
            return $this->config;
        }

        return $this->config[$key] ?? $default;
    }

    public function getTitle(): string
    {
        return $this->getConfig('title', class_basename($this));
    }


    /**
     * 設置 Widget 的配置
     *
     * @param string $key 配置鍵
     * @param mixed $value 配置值
     * @return $this
     */
    public function setConfig(string $key, mixed $value): self
    {
        Arr::set($this->config, $key, $value);
        return $this;
    }

    /**
     * 獲取 Widget 的唯一 ID
     *
     * @return string
     */
    public function getId(): string
    {
        return Str::slug($this->alias);
    }

    /**
     * 獲取 Widget 的數據
     *
     * @return array<string, mixed>
     */
    abstract public function getData(): array;

    /**
     * 將 Widget 的數據轉換為數組
     *
     * @return array<string, mixed>
     */
    public function toArray(): array
    {
        return [
            'alias' => $this->alias,
            'id' => $this->getId(),
            'type' => $this->getWidgetType(),
            'config' => $this->getConfig(),
            'data' => $this->getData(),
            'permissions' => $this->getPermissions(),
            'visible' => $this->isVisible(),
        ];
    }

    /**
     * 獲取 Widget 類型
     *
     * @return string
     */
    protected function getWidgetType(): string
    {
        return Str::snake(class_basename($this));
    }

    /**
     * 生成默認的別名
     *
     * @return string
     */
    protected function defaultAlias(): string
    {
        return Str::snake(class_basename($this));
    }

    /**
     * 合併配置
     *
     * @param array<string, mixed> $config
     * @return array<string, mixed>
     */
    protected function mergeConfig(array $config): array
    {
        return array_merge($this->defaultConfig(), $config);
    }

    /**
     * 提供 Widget 的默認配置
     *
     * @return array<string, mixed>
     */
    protected function defaultConfig(): array
    {
        return [];
    }

    /**
     * 獲取 Widget 的別名
     *
     * @return string
     */
    public function getAlias(): string
    {
        return $this->alias;
    }

    /**
     * 設置 Widget 的權限
     *
     * @param array<string> $permissions
     * @return $this
     */
    public function setPermissions(array $permissions): self
    {
        $this->permissions = $permissions;
        return $this;
    }

    /**
     * 獲取 Widget 的權限
     *
     * @return array<string>
     */
    public function getPermissions(): array
    {
        return $this->permissions;
    }

    /**
     * 檢查 Widget 是否可見
     *
     * @return bool
     */
    public function isVisible(): bool
    {
        return $this->getConfig('visible', true);
    }

    /**
     * 設置 Widget 的可見性
     *
     * @param bool $visible
     * @return $this
     */
    public function setVisible(bool $visible): self
    {
        $this->visible = $visible;
        return $this;
    }
}