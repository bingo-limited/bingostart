<?php

namespace Bingo\Core\Widgets\WidgetManager;

trait HasFilterWidgets
{

    /**
     * @var array filterWidgets 儲存過濾器小部件
     */
    protected array $filterWidgets = [];

    /**
     * 註冊單個過濾器小部件
     *
     * @param string $className 小部件類名
     * @param array|string $widgetInfo 註冊信息，可以包含 'code' 鍵
     * @return void
     */
    public function registerFilterWidget(string $className, array|string $widgetInfo): void
    {
        if (!is_array($widgetInfo)) {
            $widgetInfo = ['code' => $widgetInfo];
        }

        $widgetCode = $widgetInfo['code'] ?? $this->generateWidgetCode($className);

        $this->filterWidgets[$className] = $widgetInfo;
        $this->addWidget($className, array_merge($widgetInfo, [
            'type' => 'filter',
            'code' => $widgetCode
        ]));
    }

    /**
     * 獲取所有註冊的過濾器小部件
     *
     * @return array
     */
    public function getFilterWidgets(): array
    {
        return $this->filterWidgets;
    }

    /**
     * 解析過濾器小部件的類名
     *
     * @param string $name 類名或過濾器小部件代碼
     * @return string 解析後的類名
     */
    public function resolveFilterWidget(string $name): string
    {
        foreach ($this->filterWidgets as $className => $widgetInfo) {
            if ($widgetInfo['code'] === $name || $className === $name) {
                return $className;
            }
        }

        return $name;
    }

}