<?php

namespace Bingo\Core\Widgets\WidgetManager;


trait HasReportWidgets
{
    /**
     * @var array reportWidgets 儲存報告小部件
     */
    protected array $reportWidgets = [];

    /**
     * 註冊單個報告小部件
     *
     * @param string $className 小部件類名
     * @param array $widgetInfo 註冊信息
     * @return void
     */
    public function registerReportWidget(string $className, array $widgetInfo): void
    {
        $widgetCode = $widgetInfo['code'] ?? $this->generateWidgetCode($className);

        $this->reportWidgets[$className] = $widgetInfo;
        $this->addWidget($className, array_merge($widgetInfo, [
            'type' => 'report',
            'code' => $widgetCode
        ]));
    }

    /**
     * 獲取所有註冊的報告小部件
     *
     * @return array
     */
    public function getReportWidgets(): array
    {
        return $this->reportWidgets;
    }

    /**
     * 移除一個已註冊的報告小部件
     *
     * @param string $className 小部件類名
     * @return void
     */
    public function removeReportWidget(string $className): void
    {
        unset($this->reportWidgets[$className]);
        $this->removeWidget($className);
    }

}