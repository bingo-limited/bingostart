<?php

namespace Bingo\Core\Events;

/**
 * 模块已启用
 * Class ModuleEnabledEvent
 * @package Bingo\Core\Events
 */
class ModuleEnabledEvent
{
    public $name;
}
