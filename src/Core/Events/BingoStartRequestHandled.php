<?php

namespace Bingo\Core\Events;

use Illuminate\Http\Response;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Str;

class BingoStartRequestHandled
{
    public $url;
    public $method;
    public $time;
    public $statusCode;
    /** @var Response */
    public $response;

    public function isHtml(): bool
    {
        try {
            if ($this->response->headers) {
                return Str::contains($this->response->headers->get('content-type'), 'text/html');
            }
        } catch (\Exception) {
            Log::info('Bingo.BingoRequestHandled.Unknown - '.get_class($this->response));
        }
        return false;
    }

    public function isGet(): bool
    {
        return 'GET' == $this->method;
    }

}
