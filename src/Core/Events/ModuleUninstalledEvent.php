<?php

namespace Bingo\Core\Events;

/**
 * 模块已卸载
 * Class ModuleUninstalledEvent
 * @package Bingo\Core\Events
 */
class ModuleUninstalledEvent
{
    public $name;
}
