<?php

namespace Bingo\Core\Events;

/**
 * 模块已安装
 * Class ModuleInstalledEvent
 * @package Bingo\Core\Events
 */
class ModuleInstalledEvent
{
    public $name;
}
