<?php

namespace Bingo\Core\Util;

class PlatformUtil
{
    public const WINDOWS = 'windows';
    public const LINUX = 'linux';
    public const OSX = 'osx';
    public const UNKNOWN = 'unknown';

    private static function name(): string
    {
        return strtoupper(PHP_OS);
    }

    public static function isWindows(): bool
    {
        return str_starts_with(self::name(), "WIN");
    }

    public static function isOsx(): bool
    {
        return self::name() == 'DARWIN';
    }

    public static function isLinux(): bool
    {
        return self::name() == 'LINUX';
    }

    public static function isType($types): bool
    {
        return in_array(self::type(), $types);
    }

    public static function type(): string
    {
        if (self::isOsx()) {
            return self::OSX;
        }
        if (self::isWindows()) {
            return self::WINDOWS;
        }
        if (self::isLinux()) {
            return self::LINUX;
        }
        return self::UNKNOWN;
    }
}
