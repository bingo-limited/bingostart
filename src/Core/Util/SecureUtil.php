<?php

namespace Bingo\Core\Util;

use Bingo\Enums\Code;
use Bingo\Exceptions\BizException;

class SecureUtil
{
    public const DEFAULT_CIPHER_ALGO = 'aes-256-cbc';

    /**
     * 默认模式
     */
    public const MODE_DEFAULT = 'default';
    /**
     * 适配encrypt-js模式
     */
    public const MODE_SALTED = 'salted';

    /**
     * @param $key string base64后的key
     * @param $data string 需要加密的数据
     * @param $keyIsBase64 bool key是否是base64后的
     * @param $mode string 加密模式，default默认模式
     * @return string base64后的加密数据
     */
    public static function aesEncode(string $key, string $data, bool $keyIsBase64 = false, string $mode = 'default'): string
    {
        $encryptionKey = $key;
        if ($keyIsBase64) {
            $encryptionKey = base64_decode($encryptionKey);
        }
        switch ($mode) {
            case self::MODE_SALTED:
                $method = "AES-256-CBC";
                $salt = openssl_random_pseudo_bytes(8);
                $keyIv = self::evpBytesToKey($salt, $key);
                $key = substr($keyIv, 0, 32);
                $iv = substr($keyIv, 32, 16);
                $ciphertext = openssl_encrypt(urlencode($data), $method, $key, OPENSSL_RAW_DATA, $iv);
                return base64_encode("Salted__".$salt.$ciphertext);
            default:
                $ivLength = openssl_cipher_iv_length(self::DEFAULT_CIPHER_ALGO);
                $iv = openssl_random_pseudo_bytes($ivLength);
                $encrypted = openssl_encrypt($data, self::DEFAULT_CIPHER_ALGO, $encryptionKey, 0, $iv);
                return base64_encode($encrypted.'::'.$iv);
        }
    }

    /**
     * @param $key string base64后的key
     * @param $data string base64后的加密数据
     * @param $keyIsBase64 bool key是否是base64后的
     * @return bool|string|null 解密后的数据
     * @throws BizException
     */
    public static function aesDecode(string $key, string $data, bool $keyIsBase64 = false): bool|string|null
    {
        BizException::throwsIfEmpty($key, Code::FAILED, 'SecureUtil.Key Empty');
        $encryptionKey = $key;
        if ($keyIsBase64) {
            $encryptionKey = base64_decode($encryptionKey);
        }
        $data = base64_decode($data);
        if (str_starts_with($data, 'Salted__')) {
            $salt = substr($data, 8, 8);
            $ciphertext = substr($data, 16);
            $keyIv = self::evpBytesToKey($salt, $key);
            $key = substr($keyIv, 0, 32);
            $iv = substr($keyIv, 32, 16);
            return urldecode(openssl_decrypt($ciphertext, self::DEFAULT_CIPHER_ALGO, $key, OPENSSL_RAW_DATA, $iv));
        }
        $pcs = explode('::', $data, 2);
        if (count($pcs) != 2) {
            return null;
        }
        list($encryptedData, $iv) = $pcs;
        return openssl_decrypt($encryptedData, self::DEFAULT_CIPHER_ALGO, $encryptionKey, 0, $iv);
    }

    private static function evpBytesToKey($salt, $password): string
    {
        $bytes = '';
        $last = '';
        while (strlen($bytes) < 48) {
            $last = hash('md5', $last.$password.$salt, true);
            $bytes .= $last;
        }
        return $bytes;
    }

    public static function encryptKey()
    {
        return config('env.ENCRYPT_KEY');
    }
}
