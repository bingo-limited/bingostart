<?php

namespace Bingo\Core\Util;

/**
 * Class RandomUtil
 * @package Bingo\Core\Util
 *
 * @Util 随机字符串
 */
class RandomUtil
{
    /**
     * @Util 随机数字
     * @param $length int 长度
     * @return string 字符串
     */
    public static function number(int $length): string
    {
        $pool = '0123456789';
        return substr(str_shuffle(str_repeat($pool, $length)), 0, $length);
    }


    /**
     * @Util 随机字符串
     * @param $length int 长度
     * @return string 字符串
     */
    public static function string(int $length): string
    {
        $pool = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        return substr(str_shuffle(str_repeat($pool, $length)), 0, $length);
    }

    /**
     * @Util 随机可读字符串
     * @param $length int 长度
     * @return string 字符串
     * @desc 去掉0、O等相似字符
     */
    public static function readableString(int $length): string
    {
        $pool = '2345678abcdefghijkmnoprstuvwxyzABCDEFGHIJKMNPQRSTUVWXYZ';
        return substr(str_shuffle(str_repeat($pool, $length)), 0, $length);
    }

    /**
     * @Util 随机可读字符串（小写）
     * @param $length int 长度
     * @return string 字符串
     */
    public static function lowerReadableString(int $length): string
    {
        return strtolower(self::readableString($length));
    }

    /**
     * @Util 随机可读字符串（大写）
     * @param $length int 长度
     * @return string 字符串
     */
    public static function upperReadableString(int $length): string
    {
        return strtoupper(self::readableString($length));
    }

    /**
     * @Util 随机Hex字符串
     * @param $length int 长度
     * @return string 字符串
     */
    public static function hexString(int $length): string
    {
        $pool = '0123456789abcdef';
        return substr(str_shuffle(str_repeat($pool, $length)), 0, $length);
    }

    /**
     * @Util 随机小写字符串
     * @param $length int 长度
     * @return string 字符串
     */
    public static function lowerString(int $length): string
    {
        $pool = '0123456789abcdefghijklmnopqrstuvwxyz';
        return substr(str_shuffle(str_repeat($pool, $length)), 0, $length);
    }

    /**
     * @Util 随机小写字符串
     * @param $length int 长度
     * @return string 字符串
     * @desc 只包含字母
     */
    public static function lowerChar(int $length): string
    {
        $pool = 'abcdefghijklmnopqrstuvwxyz';
        return substr(str_shuffle(str_repeat($pool, $length)), 0, $length);
    }

    /**
     * @Util 随机大写字符串
     * @param $length int 长度
     * @return string 字符串
     * @desc 只包含字母
     */
    public static function upperChar(int $length): string
    {
        $pool = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
        return substr(str_shuffle(str_repeat($pool, $length)), 0, $length);
    }

    /**
     * @Util 随机大写字符串
     * @param $length int 长度
     * @return string 字符串
     */
    public static function upperString(int $length): string
    {
        $pool = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ';
        return substr(str_shuffle(str_repeat($pool, $length)), 0, $length);
    }

    /**
     * @Util 随机UUID
     * @return string UUID
     * @desc 使用年月日构造
     */
    public static function uuid(): string
    {
        return date('Ymd')
            .'-'
            .date('Hi')
            .'-'
            .date('s')
            .self::hexString(2)
            .'-'
            .self::hexString(4)
            .'-'
            .self::hexString(12);
    }

    /**
     * @Util 随机概率
     * @param $value int 概率值
     * @return bool 是否成功
     */
    public static function percent(int $value): bool
    {
        return rand(0, 99) < $value;
    }

    public static function dateCollection($length = 10): array
    {
        $list = [];
        $t = time();
        for ($i = $t - $length * TimeUtil::MINUTE_PERIOD_DAY; $i < $t; $i += TimeUtil::MINUTE_PERIOD_DAY) {
            $list[] = date('Y-m-d', $i);
        }
        return $list;
    }

    public static function numberCollection($length = 10, $min = 10, $max = 100): array
    {
        $list = [];
        for ($i = 0; $i < $length; $i++) {
            $list[] = rand($min, $max);
        }
        return $list;
    }

}
