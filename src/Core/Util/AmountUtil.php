<?php

namespace Bingo\Core\Util;

class AmountUtil
{
    /**
     * 将字符串"元"转换成"分"
     * @param string $str
     * @return string
     */
    public static function convertDollarToCent(string $str): string
    {
        $amount = floatval($str);
        return strval(intval(round($amount * 100)));
    }

    /**
     * 将字符串"分"转换成"元"（长格式），如：100分被转换为1.00元。
     * @param string $s
     * @return string
     */
    public static function convertCentToDollar(string $s): string
    {
        if ($s === "") {
            return "";
        }
        $l = intval($s);
        $negative = $l < 0;
        $l = abs($l);
        $dollars = number_format($l / 100, 2, '.', '');

        return $negative ? '-'.$dollars : $dollars;
    }

    /**
     * 将Long "分"转换成"元"（长格式），如：100分被转换为1.00元。
     * @param int|null $s
     * @return string
     */
    public static function convertCentToDollarFromLong(?int $s): string
    {
        if ($s === null) {
            return "";
        }
        return number_format($s / 100, 2, '.', '');
    }

    /**
     * 将字符串"分"转换成"元"（短格式），如：100分被转换为1元。
     * @param string $s
     * @return string
     */
    public static function convertCentToDollarShort(string $s): string
    {
        $fullFormat = self::convertCentToDollar($s);
        return rtrim(rtrim($fullFormat, '0'), '.');
    }

    /**
     * 计算百分比类型的各种费用值  （订单金额 * 真实费率  结果四舍五入并保留0位小数 ）
     * @param int $amount 订单金额  （保持与数据库的格式一致 ，单位：分）
     * @param float $rate 费率   （保持与数据库的格式一致 ，真实费率值，如费率为0.55%，则传入 0.0055）
     * @return int
     */
    public static function calculatePercentageFee(int $amount, float $rate): int
    {
        return intval(round($amount * $rate));
    }
}
