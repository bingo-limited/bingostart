<?php

namespace Bingo\Core\Util;

class LangUtil
{
    public static function extractFileLangScripts($file): string
    {
        $file = base_path($file);
        if (! file_exists($file)) {
            return '';
        }
        $content = file_get_contents($file);
        preg_match_all('/L\\((([^()]*|\\([^()]*\\))*)\\)/', $content, $mat);
        if (! empty($mat[1])) {
            $langArr = [];
            foreach ($mat[0] as $item) {
                if (preg_match('/^L\\([\'|"](.*?)[\'|"].*?\\)/', $item, $mat1)) {
                    $langArr[$mat1[1]] = T($mat1[1]);
                }

            }
            ksort($langArr);
            return "\n{!! \Bingo\Developer\LangUtil::langScriptPrepare(".SerializeUtil::jsonEncodePretty(array_keys($langArr)).") !!}";
        }
        return '';
    }

    public static function langScriptPrepare($langs): string
    {
        $script = [];
        $script[] = "\n(function(){";
        $script[] = "  window.lang = window.lang||{};";
        foreach ($langs as $l) {
            $script[] = "  window.lang[".SerializeUtil::jsonEncode($l)."]=".SerializeUtil::jsonEncode(T($l)).";";
        }
        $script[] = "})();";
        return join("\n", $script);
    }
}
