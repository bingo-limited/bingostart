<?php

namespace Bingo\Core\Util;

/**
 * @Util 分页渲染工具
 */
class PageHtmlUtil
{
    private static function itemRender($start, $end, $currentPage, $url, $template): string
    {
        $html = [];
        for ($i = $start; $i <= $end; $i++) {
            if ($i == $currentPage) {
                $html[] = self::replace($template['current'], [
                    '%p%' => $i,
                    '%s%' => self::buildPage($url, $i)
                ]);
            } else {
                $html[] = self::replace($template['item'], [
                    '%p%' => $i,
                    '%s%' => self::buildPage($url, $i)
                ]);
            }
        }
        return join('', $html);
    }

    /**
     * @Util 渲染下一页分页链接
     * @param $total int 总记录数
     * @param $pageSize int 每页记录数
     * @param $currentPage int 当前页
     * @param $url string 分页链接，页码使用 {page} 占位
     */
    public static function nextPageUrl(int $total, int $pageSize, int $currentPage, string $url = '/url/for/path?page={page}'): array|string|null
    {
        $totalPage = ceil($total / $pageSize);
        if ($currentPage < $totalPage) {
            return self::buildPage($url, $currentPage + 1);
        }
        return null;
    }

    /**
     * @Util 渲染上一页分页链接
     * @param $total int 总记录数
     * @param $pageSize int 每页记录数
     * @param $currentPage int 当前页
     * @param $url string 分页链接，页码使用 {page} 占位
     */
    public static function prevPageUrl(int $total, int $pageSize, int $currentPage, string $url = '/url/for/path?page={page}'): array|string|null
    {
        if ($currentPage > 1) {
            return self::buildPage($url, $currentPage - 1);
        }
        return null;
    }

    private static function replace($tpl, $param = []): array|string
    {
        return str_replace(array_keys($param), array_values($param), $tpl);
    }

    private static function buildPage($url, $page): array|string
    {
        return str_replace('{page}', $page, $url);
    }

    /**
     * @Util 渲染分页工具
     * @param $total int 总记录数
     * @param $pageSize int 每页记录数
     * @param $currentPage int 当前页
     * @param $url string 分页链接，页码使用 {page} 占位
     * @param $template string|null 模板
     */
    public static function render(int $total, int $pageSize, int $currentPage, string $url = '/url/for/path?page={page}', string $template = null): array|string
    {
        if (is_null($template)) {
            $template = [
                'warp' => '<div class="ul">%s%</div>',
                'more' => '<span class="more">...</span>',
                'prev' => '<div class="li"><a href="%s%">'.T('PrevPage').'</a></div>',
                'prevDisabled' => null,
                'next' => '<div class="li"><a href="%s%">'.T('NextPage').'</a></div>',
                'nextDisabled' => null,
                'current' => '<div class="li active"><a href="%s%">%p%</a></div>',
                'item' => '<div class="li"><a href="%s%">%p%</a></div>',
            ];
        }

        $totalPage = ceil($total / $pageSize);

        if ($currentPage < 1) {
            $currentPage = 1;
        } elseif ($currentPage > $totalPage) {
            $currentPage = $totalPage;
        }

        $html = [];

        if (! empty($template['first'])) {
            $html[] = self::replace($template['first'], [
                '%s%' => self::buildPage($url, 1),
            ]);
        }

        if ($currentPage > 1) {
            $html[] = self::replace($template['prev'], [
                '%s%' => self::buildPage($url, $currentPage - 1),
                '%p%' => $currentPage - 1,
            ]);
        } else {
            if (! empty($template['prevDisabled'])) {
                $html[] = $template['prevDisabled'];
            }
        }

        if ($totalPage < 6) {
            if ($totalPage > 0) {
                $html[] = self::itemRender(1, $totalPage, $currentPage, $url, $template);
            }
        } else {

            $html[] = self::itemRender(1, 3, $currentPage, $url, $template);

            $midStart = $currentPage - 3;
            $midEnd = $currentPage + 3;
            if ($midStart < 4) {
                $midStart = 4;
            }
            if ($midEnd > $totalPage - 3) {
                $midEnd = $totalPage - 3;
            }
            if ($midStart > 3 + 1) {
                $html[] = $template['more'];
            }

            $html[] = self::itemRender($midStart, $midEnd, $currentPage, $url, $template);

            if ($midEnd < $totalPage - 3) {
                $html[] = $template['more'];
            }

            $html[] = self::itemRender($totalPage - 2, $totalPage, $currentPage, $url, $template);
        }

        if ($currentPage < $totalPage) {
            $html[] = self::replace($template['next'], [
                '%s%' => self::buildPage($url, $currentPage + 1),
            ]);
        } else {
            if (! empty($template['nextDisabled'])) {
                $html[] = $template['nextDisabled'];
            }
        }

        if (! empty($template['last'])) {
            $html[] = self::replace($template['last'], [
                '%s%' => self::buildPage($url, $totalPage),
            ]);
        }

        return self::replace($template['warp'], [
            '%s%' => join('', $html),
        ]);
    }
}
