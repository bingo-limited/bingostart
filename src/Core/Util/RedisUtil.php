<?php

namespace Bingo\Core\Util;

use Illuminate\Support\Facades\Redis;
use Predis\Client;

class RedisUtil
{
    public static function isEnable(): bool
    {
        return ! ! config('env.REDIS_HOST');
    }

    /**
     * @return Client|null
     */
    public static function client()
    {
        static $client = null;
        if (null === $client) {
            $client = Redis::connection('default');
        }
        return $client;
    }

    public static function get($key): ?string
    {
        return self::client()->get($key);
    }

    public static function getObject($key)
    {
        $value = self::client()->get($key);
        return @json_decode($value, true);
    }

    public static function set($key, $value): void
    {
        self::client()->set($key, $value);
    }

    public static function setnx($key, $value): int
    {
        return self::client()->setnx($key, $value);
    }

    public static function setex($key, $value, $expireSecond): void
    {
        self::client()->setex($key, $expireSecond, $value);
    }

    public static function setexObject($key, $value, $expireSecond): void
    {
        self::client()->setex($key, $expireSecond, SerializeUtil::jsonEncode($value));
    }

    public static function delete($key): void
    {
        self::client()->del([$key]);
    }

    public static function incr($key): void
    {
        self::client()->incr($key);
    }

    public static function decr($key): int
    {
        return self::client()->decr($key);
    }

    public static function expire($key, $seconds): void
    {
        self::client()->expire($key, $seconds);
    }
}
