<?php

namespace Bingo\Core\Util;

class StubUtil
{
    public static function render($file, $variables = [], $base = null): array|bool|string
    {
        if (null === $base) {
            $base = base_path('vendor/bingo/resources/stub');
        }
        $content = file_get_contents("$base/$file.stub");
        $variables = array_build($variables, function ($k, $v) {
            return ['${'.$k.'}', $v];
        });
        return str_replace(array_keys($variables), array_values($variables), $content);
    }
}
