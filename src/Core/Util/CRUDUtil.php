<?php

namespace Bingo\Core\Util;

use Bingo\Core\Input\InputPackage;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\View;

class CRUDUtil
{
    public static function copyId($defaultValue = null)
    {
        $input = InputPackage::buildFromInput();
        return $input->getInteger('_copyId', $defaultValue);
    }

    public static function scope($defaultValue = null)
    {
        $input = InputPackage::buildFromInput();
        return $input->getTrimString('_scope', $defaultValue);
    }

    public static function id()
    {
        $input = InputPackage::buildFromInput();
        $id = $input->getInteger('_id');
        if (! $id) {
            $id = $input->getInteger('id');
        }
        return $id;
    }

    public static function ids(): array
    {
        $input = InputPackage::buildFromInput();
        $id = $input->getTrimString('_id');
        if (! $id) {
            $id = $input->getTrimString('id');
            if (empty($id)) {
                $id = $input->getTrimString('ids');
            }
        }
        $ids = [];
        foreach (explode(',', $id) as $i) {
            $ids[] = intval($i);
        }
        return $ids;
    }

    public static function registerRouteResource($prefix, $class): void
    {
        Route::match(['get', 'post'], "$prefix", "$class@index");
        Route::match(['get', 'post'], "$prefix/add", "$class@add");
        Route::match(['get', 'post'], "$prefix/edit", "$class@edit");
        Route::match(['post'], "$prefix/delete", "$class@delete");
        Route::match(['get'], "$prefix/show", "$class@show");
        Route::match(['post'], "$prefix/sort", "$class@sort");
    }

    public static function jsGridRefresh($index = 0): string
    {
        return "[js]window.__grids.get($index).lister.refresh();";
    }

    public static function jsDialogCloseAndParentGridRefresh($index = 0): string
    {
        return "[js]parent.__grids.get($index).lister.refresh();__dialogClose();";
    }

    public static function jsDialogClose(): string
    {
        return "[js]__dialogClose();";
    }

    public static function jsDialogCloseAndParentRefresh(): string
    {
        return "[js]parent.location.reload();";
    }

    public static function adminRedirectOrTabClose($url): string
    {
        $redirect = bingostart_admin_url($url);
        if (View::shared('_isTab')) {
            $redirect = '[tab-close]';
        }
        return $redirect;
    }

    public static function adminUrlWithTab($url, $query = []): string
    {
        if (View::shared('_isTab')) {
            $query['_is_tab'] = 1;
        }
        return bingostart_admin_url($url, $query);
    }

}
