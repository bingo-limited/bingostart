<?php

namespace Bingo\Core\Assets\Depends;

use Bingo\Core\Assets\AssetsBundle;

class JqueryAsset extends AssetsBundle
{
    public ?string $sourcePath = '@bower/jquery/dist';
    public array $js = [
        'asset/form/js/libs/jquery.js',
    ];
}
