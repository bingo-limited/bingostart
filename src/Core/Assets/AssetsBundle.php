<?php

namespace Bingo\Core\Assets;

use Illuminate\Contracts\Container\BindingResolutionException;
use Illuminate\Support\Facades\URL;

class AssetsBundle
{
    /**
     * @var string|null 包含此资源包的源资产文件的目录。
     */
    public ?string $sourcePath = "";

    /**
     * @var string 包含此资源包中的资产文件的Web可访问目录。
     */
    public string $basePath = "";

    /**
     * @var string 相对于 $js 和 $css 中列出的资产文件的基础URL。
     */
    public string $baseUrl = "";

    /**
     * @var array 此资源包依赖的资源包类名列表。
     */
    public array $depends = [];

    /**
     * @var array 此资源包包含的JavaScript文件列表。
     */
    public array $js = [];

    /**
     * @var array 此资源包包含的CSS文件列表。
     */
    public array $css = [];

    /**
     * @var array 注册此资源包中的JS文件时传递的选项。
     */
    public array $jsOptions = [];

    /**
     * @var array 注册此资源包中的CSS文件时传递的选项。
     */
    public array $cssOptions = [];

    /**
     * @var array 当发布资产包时传递的选项。仅当设置了 $sourcePath 时使用。
     */
    public array $publishOptions = [];

    public static function className(): string
    {
        return get_called_class();
    }

    /**
     * 将此资源包注册到视图中。
     *
     * @param View $view 要注册的视图
     * @return AssetsBundle
     */
    public static function register(View $view): AssetsBundle
    {
        // 在视图中注册资源包
        return $view->registerAssetBundle(get_called_class());
    }

    /**
     * 初始化资源包。
     * 如果重写此方法，请确保在最后调用父类实现。
     */
    public function init(): void
    {
        if ($this->sourcePath !== null) {
            $this->sourcePath = rtrim(base_path($this->sourcePath), '/\\');
        }
        if ($this->basePath !== null) {
            $this->basePath = rtrim(public_path($this->basePath), '/\\');
        }
        if ($this->baseUrl !== null) {
            $this->baseUrl = rtrim(URL::to($this->baseUrl), '/');
        }
    }

    /**
     * 将CSS和JS文件注册到给定的视图中。
     *
     * @param View $view 要注册资产文件的视图
     * @throws BindingResolutionException
     */
    public function registerAssetFiles(View $view): void
    {
        $manager = app('assetManager');
        foreach ($this->js as $js) {
            if (is_array($js)) {
                $file = array_shift($js);
                $options = array_merge($this->jsOptions, $js);
                $view->registerJsFile($manager->getAssetUrl($this, $file), $options);
            } elseif ($js !== null) {
                $view->registerJsFile($manager->getAssetUrl($this, $js), $this->jsOptions);
            }
        }
        foreach ($this->css as $css) {
            if (is_array($css)) {
                $file = array_shift($css);
                $options = array_merge($this->cssOptions, $css);
                $view->registerCssFile($manager->getAssetUrl($this, $file), $options);
            } elseif ($css !== null) {
                $view->registerCssFile($manager->getAssetUrl($this, $css), $this->cssOptions);
            }
        }
    }

    /**
     * 如果源代码不在Web可访问目录下，则发布资产包。
     * 它还会尝试将非CSS或JS文件（例如LESS、Sass）转换为相应的CSS或JS文件。
     *
     * @param AssetsManager $am 执行资产发布的资产管理器
     */
    public function publish(AssetsManager $am): void
    {
        if ($this->sourcePath !== null && ! isset($this->basePath, $this->baseUrl)) {
            list($this->basePath, $this->baseUrl) = $am->publish($this->sourcePath, $this->publishOptions);
        }

        //        if (isset($this->basePath, $this->baseUrl) && ($converter = $am->getConverter()) !== null) {
        //            foreach ($this->js as $i => $js) {
        //                if (is_array($js)) {
        //                    $file = array_shift($js);
        //                    if (Str::startsWith($file, ['http://', 'https://', '//'])) {
        //                        $js = array_merge($this->jsOptions, $js);
        //                        array_unshift($js, $converter->convert($file, $this->basePath));
        //                        $this->js[$i] = $js;
        //                    }
        //                } elseif (Str::startsWith($js, ['http://', 'https://', '//'])) {
        //                    $this->js[$i] = $converter->convert($js, $this->basePath);
        //                }
        //            }
        //            foreach ($this->css as $i => $css) {
        //                if (is_array($css)) {
        //                    $file = array_shift($css);
        //                    if (Str::startsWith($file, ['http://', 'https://', '//'])) {
        //                        $css = array_merge($this->cssOptions, $css);
        //                        array_unshift($css, $converter->convert($file, $this->basePath));
        //                        $this->css[$i] = $css;
        //                    }
        //                } elseif (Str::startsWith($css, ['http://', 'https://', '//'])) {
        //                    $this->css[$i] = $converter->convert($css, $this->basePath);
        //                }
        //            }
        //        }
    }
}
