<?php

namespace Bingo\Core\Assets;

use Illuminate\Container\Container;
use Illuminate\Contracts\Container\BindingResolutionException;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\HtmlString;
use InvalidArgumentException;

/**
 * View 类代表 MVC 模式中的视图对象。
 *
 * View 提供了一组方法（例如 [[render()]]）用于渲染目的。
 *
 * 更多详细信息和用法，请参见视图的[指南文章](guide:structure-views)。
 */
class View extends BaseView
{
    /**
     * @event Event 由 [[beginBody()]] 触发的事件。
     */
    public const EVENT_BEGIN_BODY = 'beginBody';
    /**
     * @event Event 由 [[endBody()]] 触发的事件。
     */
    public const EVENT_END_BODY = 'endBody';
    /**
     * 注册的 JavaScript 代码块或文件的位置。
     * 这意味着位置在 head 部分。
     */
    public const POS_HEAD = 1;
    /**
     * 注册的 JavaScript 代码块或文件的位置。
     * 这意味着位置在 body 部分的开始。
     */
    public const POS_BEGIN = 2;
    /**
     * 注册的 JavaScript 代码块或文件的位置。
     * 这意味着位置在 body 部分的结束。
     */
    public const POS_END = 3;
    /**
     * 注册的 JavaScript 代码块的位置。
     * 这意味着 JavaScript 代码块将包含在 `jQuery(document).ready()` 中。
     */
    public const POS_READY = 4;
    /**
     * 注册的 JavaScript 代码块的位置。
     * 这意味着 JavaScript 代码块将包含在 `jQuery(window).load()` 中。
     */
    public const POS_LOAD = 5;
    /**
     * 这在内部用作占位符，用于接收注册在 head 部分的内容。
     */
    public const PH_HEAD = '<![CDATA[YII-BLOCK-HEAD]]>';
    /**
     * 这在内部用作占位符，用于接收注册在 body 部分开始的内容。
     */
    public const PH_BODY_BEGIN = '<![CDATA[YII-BLOCK-BODY-BEGIN]]>';
    /**
     * 这在内部用作占位符，用于接收注册在 body 部分结束的内容。
     */
    public const PH_BODY_END = '<![CDATA[YII-BLOCK-BODY-END]]>';

    /**
     * @var array 注册的资产包列表。键是包的名称，值是注册的 [[AssetBundle]] 对象。
     * @see registerAssetBundle()
     */
    public array $assetBundles = [];
    /**
     * @var string 页面标题
     */
    public string $title;
    /**
     * @var array 注册的 meta 标签。
     * @see registerMetaTag()
     */
    public array $metaTags = [];
    /**
     * @var array 注册的链接标签。
     * @see registerLinkTag()
     */
    public array $linkTags = [];
    /**
     * @var array 注册的 CSS 代码块。
     * @see registerCss()
     */
    public array $css = [];
    /**
     * @var array 注册的 CSS 文件。
     * @see registerCssFile()
     */
    public array $cssFiles = [];
    /**
     * @var array 注册的 JS 代码块。
     * @see registerJs()
     */
    public array $js = [];
    /**
     * @var array 注册的 JS 文件。
     * @see registerJsFile()
     */
    public array $jsFiles = [];

    private ?AssetsManager $_AssetsManager = null; // 确保初始值为 null

    /**
     * 是否已调用 [[endPage()]] 并且所有文件都已注册。
     * @var bool
     */
    protected bool $isPageEnded = false;

    protected array $integrityCache = [];

    /**
     * 标记 HTML head 部分的位置。
     */
    public function head(): void
    {
        echo self::PH_HEAD;
    }


    /**
     * 标记 HTML body 部分的开始。
     */
    public function beginBody(): void
    {
        echo self::PH_BODY_BEGIN;
        // 触发开始 body 部分的事件
        // $this->trigger(self::EVENT_BEGIN_BODY);
    }

    /**
     * 标记 HTML body 部分的结束。
     */
    public function endBody(): void
    {
        // 触发结束 body 部分的事件
        // $this->trigger(self::EVENT_END_BODY);
        echo self::PH_BODY_END;

        foreach (array_keys($this->assetBundles) as $bundle) {
            $this->registerAssetFiles($bundle);
        }
    }

    /**
     * 标记 HTML 页面的结束。
     * @param bool $ajaxMode 是否在 AJAX 模式下渲染视图。
     */
    public function endPage(bool $ajaxMode = false): void
    {
        $this->isPageEnded = true;

        $content = ob_get_clean();

        echo strtr($content, [
            self::PH_HEAD => $this->renderHeadHtml(),
            self::PH_BODY_BEGIN => $this->renderBodyBeginHtml(),
            self::PH_BODY_END => $this->renderBodyEndHtml($ajaxMode),
        ]);

        $this->clear();
    }

    /**
     * 渲染 AJAX 请求响应中的视图。
     *
     * 此方法类似于 [[render()]]，不同之处在于它会围绕渲染的视图调用
     * [[beginPage()]]、[[head()]]、[[beginBody()]]、[[endBody()]] 和 [[endPage()]]。
     * 通过这样做，该方法能够将注册到视图中的 JS/CSS 脚本和文件注入到渲染结果中。
     *
     * @param string $view 视图名称。
     * @param array $params 在视图文件中可用的参数（名称-值对）。
     * @param object|null $context 渲染视图时使用的上下文。如果为 null，则使用现有的 [[context]]。
     * @return string 渲染结果
     * @see render()
     */
    public function renderAjax(string $view, array $params = [], object $context = null): string
    {
        $viewFile = $this->findViewFile($view, $context);

        ob_start();
        ob_implicit_flush(false);

        $this->beginPage();
        $this->head();
        $this->beginBody();
        echo $this->renderFile($viewFile, $params, $context);
        $this->endBody();
        $this->endPage(true);

        return ob_get_clean();
    }

    /**
     * 获取正在使用的资产管理器。
     * @return AssetsManager 资产管理器。
     */
    public function getAssetsManager(): AssetsManager
    {
        if (! isset($this->_AssetsManager)) {
            $this->_AssetsManager = app('assetManager');
        }
        return $this->_AssetsManager;
    }

    /**
     * 设置资产管理器。
     * @param AssetsManager $value 资产管理器
     */
    public function setAssetsManager(AssetsManager $value): void
    {
        $this->_AssetsManager = $value;
    }

    /**
     * 清理注册的 meta 标签、链接标签、CSS/JS 脚本和文件。
     */
    public function clear(): void
    {
        $this->metaTags = [];
        $this->linkTags = [];
        $this->css = [];
        $this->cssFiles = [];
        $this->js = [];
        $this->jsFiles = [];
        $this->assetBundles = [];
    }

    /**
     * 注册资产包提供的所有文件，包括依赖包文件。
     * 注册文件后从 [[assetBundles]] 中移除包。
     * @param string $name 要注册的包的名称
     */
    protected function registerAssetFiles(string $name): void
    {
        if (! isset($this->assetBundles[$name])) {
            return;
        }
        $bundle = $this->assetBundles[$name];
        if ($bundle) {
            foreach ($bundle->depends as $dep) {
                $this->registerAssetFiles($dep);
            }
            $bundle->registerAssetFiles($this);
        }
        unset($this->assetBundles[$name]);
    }

    /**
     * 注册命名的资产包。
     * 所有依赖的资产包都会被注册。
     * @param string $name 资产包的类名（不带前导反斜杠）
     * @param int|null $position 如果设置，则强制执行 JavaScript 文件的最小位置。
     * @return AssetsBundle 注册的资产包实例
     * @throws InvalidArgumentException 如果资产包不存在或检测到循环依赖
     */
    public function registerAssetBundle(string $name, int|null $position = null): AssetsBundle
    {
        if (! isset($this->assetBundles[$name])) {
            $am = $this->getAssetsManager();
            $bundle = $am->getBundle($name);
            $this->assetBundles[$name] = false;
            // 注册依赖项
            $pos = $bundle->jsOptions['position'] ?? null;
            foreach ($bundle->depends as $dep) {
                $this->registerAssetBundle($dep, $pos);
            }
            $this->assetBundles[$name] = $bundle;
        } elseif ($this->assetBundles[$name] === false) {
            throw new InvalidArgumentException("检测到包 '$name' 的循环依赖。");
        } else {
            $bundle = $this->assetBundles[$name];
        }

        if ($position !== null) {
            $pos = $bundle->jsOptions['position'] ?? null;
            if ($pos === null) {
                $bundle->jsOptions['position'] = $pos = $position;
            } elseif ($pos > $position) {
                throw new InvalidArgumentException("依赖于 '$name' 的资产包配置了比 '$name' 更高的 JavaScript 文件位置。");
            }
            // 更新所有依赖项的位置
            foreach ($bundle->depends as $dep) {
                $this->registerAssetBundle($dep, $pos);
            }
        }

        return $bundle;
    }

    /**
     * 注册 meta 标签。
     * 例如，可以像下面这样添加描述 meta 标签：
     *
     * ```php
     * $view->registerMetaTag([
     *     'name' => 'description',
     *     'content' => 'This website is about funny raccoons.'
     * ]);
     * ```
     *
     * 结果是 meta 标签 `<meta name="description" content="This website is about funny raccoons.">`。
     *
     * @param array $options meta 标签的 HTML 属性。
     * @param string|null $key 标识 meta 标签的键。如果注册了两个具有相同键的 meta 标签，
     * 后者将覆盖前者。如果为 null，新 meta 标签将附加到现有标签之后。
     */
    public function registerMetaTag(array $options, string $key = null): void
    {
        if ($key === null) {
            $this->metaTags[] = new HtmlString('<meta '.$this->attributes($options).'>');
        } else {
            $this->metaTags[$key] = new HtmlString('<meta '.$this->attributes($options).'>');
        }
    }

    /**
     * 注册 CSRF meta 标签。
     * 它们会动态渲染以在每个请求时检索新的 CSRF 令牌。
     *
     * ```php
     * $view->registerCsrfMetaTags();
     * ```
     *
     * 上面的代码将在页面中添加 `<meta name="csrf-param" content="[yii\web\Request::$csrfParam]">`
     * 和 `<meta name="csrf-token" content="tTNpWKpdy-bx8ZmIq9R72...K1y8IP3XGkzZA==">`。
     */
    public function registerCsrfMetaTags(): void
    {
        $this->metaTags['csrf_meta_tags'] = csrf_token();
    }

    /**
     * 注册链接标签。
     * 例如，可以像下面这样添加自定义 [favicon](https://www.w3.org/2005/10/howto-favicon) 的链接标签：
     *
     * ```php
     * $view->registerLinkTag(['rel' => 'icon', 'type' => 'image/png', 'href' => '/myicon.png']);
     * ```
     *
     * 结果是 `<link rel="icon" type="image/png" href="/myicon.png">` 的 HTML。
     *
     * @param array $options 链接标签的 HTML 属性。
     * @param string|null $key 标识链接标签的键。如果注册了两个具有相同键的链接标签，
     * 后者将覆盖前者。如果为 null，新链接标签将附加到现有标签之后。
     */
    public function registerLinkTag(array $options, string $key = null): void
    {
        if ($key === null) {
            $this->linkTags[] = new HtmlString('<link '.$this->attributes($options).'>');
        } else {
            $this->linkTags[$key] = new HtmlString('<link '.$this->attributes($options).'>');
        }
    }

    /**
     * 注册 CSS 代码块。
     * @param string $css 要注册的 CSS 代码块的内容
     * @param array $options `<style>` 标签的 HTML 属性。
     * @param string|null $key 标识 CSS 代码块的键。如果为 null，它将使用 $css 作为键。
     * 如果注册了两个具有相同键的 CSS 代码块，后者将覆盖前者。
     */
    public function registerCss(string $css, array $options = [], string $key = null): void
    {
        $key = $key ?: md5($css);
        $this->css[$key] = new HtmlString('<style '.$this->attributes($options).'>'.$css.'</style>');
    }

    /**
     * 注册 CSS 文件。
     *
     * 此方法应用于简单注册 CSS 文件。如果要使用 [[AssetsManager]] 的功能（例如向 URL 添加时间戳和文件发布选项），
     * 请改用 [[AssetBundle]] 和 [[registerAssetBundle()]]。
     *
     * @param string $url 要注册的 CSS 文件。
     * @param array $options 链接标签的 HTML 属性。以下选项会特别处理，不会作为 HTML 属性：
     *
     * - `depends`: array，指定此 CSS 文件依赖的资产包名称。
     * - `appendTimestamp`: bool 是否向 URL 添加时间戳。
     *
     * @param string|null $key 标识 CSS 文件的键。如果为 null，它将使用 $url 作为键。
     * 如果注册了两个具有相同键的 CSS 文件，后者将覆盖前者。
     * @throws BindingResolutionException
     */
    public function registerCssFile(string $url, array $options = [], string $key = null): void
    {
        $this->registerFile('css', $url, $options, $key);
    }

    /**
     * 注册 JS 代码块。
     * @param string $js 要注册的 JS 代码块
     * @param int $position JS 脚本标签应插入页面中的位置。可能的值有：
     *
     * - [[POS_HEAD]]: 在 head 部分
     * - [[POS_BEGIN]]: 在 body 部分的开始
     * - [[POS_END]]: 在 body 部分的结束
     * - [[POS_LOAD]]: 包含在 jQuery(window).load() 中。
     * - [[POS_READY]]: 包含在 jQuery(document).ready() 中。这是默认值。
     *
     * @param string|null $key 标识 JS 代码块的键。如果为 null，它将使用 $js 作为键。
     * 如果注册了两个具有相同键的 JS 代码块，后者将覆盖前者。
     */
    public function registerJs(string $js, int $position = self::POS_READY, string $key = null): void
    {
        $key = $key ?: md5($js);
        $this->js[$position][$key] = $js;
        if ($position === self::POS_READY || $position === self::POS_LOAD) {
            // 自动注册 jQuery
            // JqueryAsset::register($this);
        }
    }

    /**
     * 注册 JS 或 CSS 文件。
     *
     * @param string $url 要注册的文件。
     * @param string $type 文件的类型（js 或 css）。
     * @param array $options 脚本标签的 HTML 属性。以下选项会特别处理，不会作为 HTML 属性：
     *
     * - `depends`: array，指定此文件依赖的资产包名称。
     * - `appendTimestamp`: bool 是否向 URL 添加时间戳。
     *
     * @param string|null $key 标识 JS 脚本文件的键。如果为 null，它将使用 $url 作为键。
     * 如果在相同位置注册了两个具有相同键的 JS 文件，后者将覆盖前者。
     * @throws InvalidArgumentException|BindingResolutionException
     */
    private function registerFile(string $type, string $url, array $options = [], string $key = null): void
    {
        $originalUrl = $url;
        $url = $this->normalizeUrl($url);
        $key = $key ?: $url;
        $depends = Arr::pull($options, 'depends', []);
        $originalOptions = $options;
        $position = Arr::pull($options, 'position', self::POS_END);
        $appendTimestamp = Arr::pull($options, 'appendTimestamp', false);

        if ($this->isPageEnded) {
            trigger_error('You\'re trying to register a file after View::endPage() has been called.', E_USER_WARNING);
        }

        // 处理 integrity 属性
        $integrity = Arr::pull($options, 'integrity', null);
        if ($this->isLocalAsset($originalUrl)) {
            $integrity = $this->generateIntegrityForLocalAsset($url);
        }
        if ($integrity) {
            $options['integrity'] = $integrity;
            $options['crossorigin'] = $options['crossorigin'] ?? 'anonymous';
        }

        if (empty($depends)) {
            // 直接注册不使用 AssetsManager
            if ($appendTimestamp && $this->isLocalAsset($originalUrl)) {
                $timestamp = @filemtime(public_path($url));
                if ($timestamp) {
                    $url .= (strpos($url, '?') === false ? '?' : '&') . 'v=' . $timestamp;
                }
            }
            if ($type === 'js') {
                $this->jsFiles[$position][$key] = $this->createJsTag($url, $options);
            } else {
                $this->cssFiles[$key] = $this->createCssTag($url, $options);
            }
        } else {
            $assetManager = app('assetManager');
            $bundleConfig = [
                'class' => AssetsBundle::className(),
                'baseUrl' => '',
                'basePath' => public_path(),
                $type => [array_merge([$url], $originalOptions)],
                "{$type}Options" => $options,
                'depends' => (array) $depends,
            ];
            $bundle = self::createObject($bundleConfig);
            $assetManager->bundles[$key] = $bundle;
            $this->registerAssetBundle($key);
        }
    }

    /**
     * 规范化 URL，保留完整 URL 或移除域名部分。
     *
     * @param string $url
     * @return string
     */
    protected function normalizeUrl(string $url): string
    {
        // 如果 URL 是绝对路径（以 http:// 或 https:// 开头）
        if (preg_match('/^(http|https):\/\//', $url)) {
            $parsedUrl = parse_url($url);
            // 检查是否是本地资源
            if ($this->isLocalAsset($url)) {
                $url = $parsedUrl['path'] ?? '';
                if (isset($parsedUrl['query'])) {
                    $url .= '?' . $parsedUrl['query'];
                }
                // 确保 URL 以斜杠开头
                return '/' . ltrim($url, '/');
            }
            // 对于非本地资源，保留完整的 URL
            return $url;
        }

        // 对于相对 URL，确保以斜杠开头
        return '/' . ltrim($url, '/');
    }

    /**
     * 检查给定的 URL 是否指向本地资源。
     *
     * @param string $url
     * @return bool
     */
    protected function isLocalAsset(string $url): bool
    {
        $parsedUrl = parse_url($url);
        $host = $parsedUrl['host'] ?? '';

        // 获取当前应用程序的所有可能的域名（包括本地、测试和生产环境）
        $allowedHosts = config('app.asset_hosts', ['localhost', '127.0.0.1']);

        return in_array($host, $allowedHosts);
    }

    /**
     * 为本地资源生成 integrity 值
     *
     * @param string $url
     * @return string|null
     */
    protected function generateIntegrityForLocalAsset(string $url): ?string
    {
        if (isset($this->integrityCache[$url])) {
            return $this->integrityCache[$url];
        }

        $filePath = public_path(ltrim($url, '/'));
        if (!file_exists($filePath)) {
            return null;
        }

        $content = file_get_contents($filePath);
        $integrity = IntegrityGenerator::generate($content);

        $this->integrityCache[$url] = $integrity;
        return $integrity;
    }

    /**
     * 创建 JavaScript 标签。
     *
     * @param string $url
     * @param array $options
     * @return HtmlString
     */
    protected function createJsTag(string $url, array $options): HtmlString
    {
        $attrs = $this->buildTagAttributes($options);
        return new HtmlString("<script src=\"{$url}\"{$attrs}></script>");
    }

    /**
     * 创建 CSS 标签。
     *
     * @param string $url
     * @param array $options
     * @return HtmlString
     */
    protected function createCssTag(string $url, array $options): HtmlString
    {
        $attrs = $this->buildTagAttributes($options);
        return new HtmlString("<link href=\"{$url}\" rel=\"stylesheet\"{$attrs}>");
    }

    /**
     * 构建 HTML 标签属性。
     *
     * @param array $options
     * @return string
     */
    protected function buildTagAttributes(array $options): string
    {
        $attrs = '';
        foreach ($options as $name => $value) {
            if (is_bool($value)) {
                if ($value) {
                    $attrs .= " $name";
                }
            } elseif ($value !== null) {
                $attrs .= " $name=\"" . htmlspecialchars($value, ENT_QUOTES, 'UTF-8') . '"';
            }
        }
        return $attrs;
    }
    

    /**
     * 注册 JS 文件。
     *
     * 此方法应用于简单注册 JS 文件。如果要使用 [[AssetsManager]] 的功能（例如向 URL 添加时间戳和文件发布选项），
     * 请改用 [[AssetBundle]] 和 [[registerAssetBundle()]]。
     *
     * @param string $url 要注册的 JS 文件。
     * @param array $options 脚本标签的 HTML 属性。以下选项会特别处理，不会作为 HTML 属性：
     *
     * - `depends`: array，指定此 JS 文件依赖的资产包名称。
     * - `position`: 指定 JS 脚本标签应插入页面中的位置。可能的值有：
     *     * [[POS_HEAD]]: 在 head 部分
     *     * [[POS_BEGIN]]: 在 body 部分的开始
     *     * [[POS_END]]: 在 body 部分的结束。这是默认值。
     * - `appendTimestamp`: bool 是否向 URL 添加时间戳。
     *
     * @param string|null $key 标识 JS 脚本文件的键。如果为 null，它将使用 $url 作为键。
     * 如果在相同位置注册了两个具有相同键的 JS 文件，后者将覆盖前者。
     * @throws InvalidArgumentException|BindingResolutionException
     */
    public function registerJsFile(string $url, array $options = [], string $key = null): void
    {
        $this->registerFile('js', $url, $options, $key);
    }

    /**
     * 注册定义变量的 JS 代码块。变量名将用作键，防止变量名重复。
     *
     * @param string $name 变量名
     * @param mixed $value 变量值
     * @param int $position JavaScript 变量应插入页面中的位置。可能的值有：
     *
     * - [[POS_HEAD]]: 在 head 部分。这是默认值。
     * - [[POS_BEGIN]]: 在 body 部分的开始。
     * - [[POS_END]]: 在 body 部分的结束。
     * - [[POS_LOAD]]: 包含在 jQuery(window).load() 中。
     * - [[POS_READY]]: 包含在 jQuery(document).ready() 中。
     */
    public function registerJsVar(string $name, mixed $value, int $position = self::POS_HEAD): void
    {
        $js = sprintf('var %s = %s;', $name, json_encode($value));
        $this->registerJs($js, $position, $name);
    }

    /**
     * 渲染要插入 head 部分的内容。
     * 内容使用已注册的 meta 标签、链接标签、CSS/JS 代码块和文件进行渲染。
     * @return string 渲染的内容
     */
    protected function renderHeadHtml(): string
    {
        $lines = [];
        if (! empty($this->metaTags)) {
            $lines[] = implode("\n", $this->metaTags);
        }

        if (! empty($this->linkTags)) {
            $lines[] = implode("\n", $this->linkTags);
        }
        if (! empty($this->cssFiles)) {
            $lines[] = implode("\n", $this->cssFiles);
        }
        if (! empty($this->css)) {
            $lines[] = implode("\n", $this->css);
        }
        if (! empty($this->jsFiles[self::POS_HEAD])) {
            $lines[] = implode("\n", $this->jsFiles[self::POS_HEAD]);
        }
        if (! empty($this->js[self::POS_HEAD])) {
            $lines[] = new HtmlString('<script>'.implode("\n", $this->js[self::POS_HEAD]).'</script>');
        }

        return empty($lines) ? '' : implode("\n", $lines);
    }

    /**
     * 渲染要插入 body 部分开始处的内容。
     * 内容使用已注册的 JS 代码块和文件进行渲染。
     * @return string 渲染的内容
     */
    protected function renderBodyBeginHtml(): string
    {
        $lines = [];
        if (! empty($this->jsFiles[self::POS_BEGIN])) {
            $lines[] = implode("\n", $this->jsFiles[self::POS_BEGIN]);
        }
        if (! empty($this->js[self::POS_BEGIN])) {
            $lines[] = new HtmlString('<script>'.implode("\n", $this->js[self::POS_BEGIN]).'</script>');
        }

        return empty($lines) ? '' : implode("\n", $lines);
    }

    /**
     * 渲染要插入 body 部分结束处的内容。
     * 内容使用已注册的 JS 代码块和文件进行渲染。
     * @param bool $ajaxMode 是否在 AJAX 模式下渲染视图。
     * @return string 渲染的内容
     */
    protected function renderBodyEndHtml(bool $ajaxMode): string
    {
        $lines = [];

        if (! empty($this->jsFiles[self::POS_END])) {
            $lines[] = implode("\n", $this->jsFiles[self::POS_END]);
        }

        if ($ajaxMode) {
            $scripts = [];
            if (! empty($this->js[self::POS_END])) {
                $scripts[] = implode("\n", $this->js[self::POS_END]);
            }
            if (! empty($this->js[self::POS_READY])) {
                $scripts[] = implode("\n", $this->js[self::POS_READY]);
            }
            if (! empty($this->js[self::POS_LOAD])) {
                $scripts[] = implode("\n", $this->js[self::POS_LOAD]);
            }
            if (! empty($scripts)) {
                $lines[] = new HtmlString('<script>'.implode("\n", $scripts).'</script>');
            }
        } else {
            if (! empty($this->js[self::POS_END])) {
                $lines[] = new HtmlString('<script>'.implode("\n", $this->js[self::POS_END]).'</script>');
            }
            if (! empty($this->js[self::POS_READY])) {
                $js = "jQuery(function ($) {\n".implode("\n", $this->js[self::POS_READY])."\n});";
                $lines[] = new HtmlString('<script>'.$js.'</script>');
            }
            if (! empty($this->js[self::POS_LOAD])) {
                $js = "jQuery(window).on('load', function () {\n".implode("\n", $this->js[self::POS_LOAD])."\n});";
                $lines[] = new HtmlString('<script>'.$js.'</script>');
            }
        }

        return empty($lines) ? '' : implode("\n", $lines);
    }

    /**
     * Helper function to convert attributes array to string.
     *
     * @param array $attributes
     * @return string
     */
    protected function attributes(array $attributes): string
    {
        $result = [];
        foreach ($attributes as $key => $value) {
            $result[] = sprintf('%s="%s"', $key, htmlspecialchars((string) $value, ENT_QUOTES, 'UTF-8'));
        }
        return implode(' ', $result);
    }



    /**
     * Checks if a given URL is relative.
     *
     * @param string $url
     * @return bool
     */
    protected function isRelativeUrl(string $url): bool
    {
        return ! preg_match('/^(http|https):\/\//', $url);
    }


    public static function createObject($type, array $params = [])
    {
        $container = Container::getInstance();

        if (is_string($type)) {
            return self::buildObject($type, $params);
        }

        if (is_callable($type)) {
            return $container->call($type, $params);
        }

        if (! is_array($type)) {
            throw new InvalidArgumentException('Unsupported configuration type: '.gettype($type));
        }

        if (isset($type['class'])) {
            $class = $type['class'];
            unset($type['class']);
            return self::buildObject($class, array_merge($params, $type));
        }

        throw new InvalidArgumentException('Object configuration must be an array containing a "class" element.');
    }

    protected static function buildObject($class, array $params)
    {
        $container = Container::getInstance();

        // Create the object using the container
        $object = $container->makeWith($class, $params);

        // Apply the configuration
        foreach ($params as $key => $value) {
            if (property_exists($object, $key)) {
                $object->$key = $value;
            }
        }

        return $object;
    }
}
