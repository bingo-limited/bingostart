<?php

namespace Bingo\Core\Assets\Driver;

use Bingo\Core\Assets\AssetsPath;
use Illuminate\Support\Facades\Cache;

class CdnAssetsPath implements AssetsPath
{
    public const CACHE_PREFIX = 'bingostart:asset-file:';

    public function getPathWithHash(string $file): string
    {
        $hash = Cache::get($flag = self::CACHE_PREFIX.$file, null);
        if (null !== $hash) {
            return $file.'?'.$hash;
        }
        if (file_exists($file)) {
            $hash = ''.crc32(md5_file($file));
            Cache::put($flag, $hash, 0);
            return $file.'?'.$hash;
        }
        Cache::put($flag, '', 0);
        return $file;
    }

    public function getCDN(string $file): string
    {
        $cdnArray = config('bingo.asset.cdn_array', ['/']);
        $cdnIndex = abs(crc32($file) % count($cdnArray));
        return $cdnArray[$cdnIndex];
    }
}
