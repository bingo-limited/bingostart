<?php

namespace Bingo\Core\Type;

interface BaseType
{
    public static function getList();
}
