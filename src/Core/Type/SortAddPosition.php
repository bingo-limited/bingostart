<?php

namespace Bingo\Core\Type;

class SortAddPosition implements BaseType
{
    public const HEAD = 'head';
    public const TAIL = 'tail';

    public static function getList(): array
    {
        return [
            self::HEAD => 'Head',
            self::TAIL => 'Tail',
        ];
    }


}
