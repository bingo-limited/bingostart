<?php

namespace Bingo\Core\Services;

use Illuminate\Support\Facades\Cache;
use Exception;
use Psr\Container\ContainerExceptionInterface;
use Psr\Container\NotFoundExceptionInterface;

class NavigationService
{
    protected array $navigation = [];
    protected array $navItems = [];
    public const CACHE_KEY_PREFIX = 'navigation_cache:';
    public const CACHE_KEY_LIFE_TIME = 600;

    /**
     * 注册导航项
     *
     * @param array $items
     * @throws Exception
     */
    public function registerNavigation(array $items): void
    {
        foreach ($items as $item) {
            if (! $this->validateNavItem($item)) {
                throw new Exception("Invalid navigation item configuration: ".json_encode($item));
            }
            $this->navItems[$item['key']] = $item;
        }
    }

    /**
     * 获取导航项
     *
     * @param string $lang
     * @return array
     */
    public function getNavigation(string $lang): array
    {
        $cacheKey = self::CACHE_KEY_PREFIX.$lang;

        // 尝试从缓存中获取导航数据
        return Cache::remember($cacheKey, self::CACHE_KEY_LIFE_TIME, function () use ($lang) {
            // 初始化导航结构
            foreach ($this->navItems as $key => $item) {
                if (! isset($item['parent'])) {
                    $item['nav_name'] = T($item['nav_name'], $lang);
                    $this->navigation[] = $item;
                }
            }

            // 构建层次结构
            foreach ($this->navigation as &$navItem) {
                $this->buildChildren($navItem, $lang);
            }

            // 排序顶层导航项
            usort($this->navigation, fn ($a, $b) => $a['order'] <=> $b['order']);

            return $this->navigation;
        });
    }

    /**
     * 构建子导航项
     *
     * @param array $parentItem
     * @param string $lang
     * @throws ContainerExceptionInterface
     * @throws NotFoundExceptionInterface
     */
    private function buildChildren(array &$parentItem, string $lang): void
    {
        $children = [];
        foreach ($this->navItems as $key => $item) {
            if (isset($item['parent']) && $item['parent'] === $parentItem['key']) {
                $item['nav_name'] = T($item['nav_name'], $lang);
                $this->buildChildren($item, $lang); // 递归构建子节点
                $children[] = $item;
            }
        }
        if (! empty($children)) {
            usort($children, fn ($a, $b) => $a['order'] <=> $b['order']);
            $parentItem['children'] = $children;
        }
    }

    /**
     * 清除导航缓存
     */
    public function clearCache(): void
    {
        foreach (Cache::getKeys(self::CACHE_KEY_PREFIX.'*') as $key) {
            Cache::forget($key);
        }
    }

    /**
     * 验证导航项配置
     *
     * @param array $item
     * @return bool
     */
    private function validateNavItem(array $item): bool
    {
        $requiredFields = ['key', 'nav_name', 'path', 'icon', 'order'];
        foreach ($requiredFields as $field) {
            if (! isset($item[$field])) {
                return false;
            }
        }
        return true;
    }
}
