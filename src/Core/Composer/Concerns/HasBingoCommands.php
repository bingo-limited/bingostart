<?php

namespace Bingo\Core\Composer\Concerns;

/**
 * HasBingoCommands for composer
 */
trait HasBingoCommands
{
    /**
     * addBingoRepository
     */
    public function addBingoRepository(string $url): void
    {
        $this->addRepository(
            'bingocms',
            'composer',
            $url,
            [
                'only' => ['bingo/*', '*-plugin', '*-theme']
            ]
        );
    }
}
