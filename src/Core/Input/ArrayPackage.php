<?php

namespace Bingo\Core\Input;

use Bingo\Core\Util\StrUtil;
use Bingo\Core\Util\TimeUtil;

class ArrayPackage
{
    private $data;
    private int $cursor = 0;

    public function __construct($data)
    {
        $this->data = $data;
    }

    public static function build($data): ArrayPackage
    {
        $package = new ArrayPackage($data);
        $package->cursor = 0;
        return $package;
    }

    public function resetCursor(): void
    {
        $this->cursor = 0;
    }

    public function next($defaultValue = null)
    {
        $index = $this->cursor;
        $this->cursor++;
        if (isset($this->data[$index])) {
            return $this->data[$index];
        }
        return $defaultValue;
    }

    public function nextTrimString($defaultValue = null)
    {
        $value = $this->next($defaultValue);
        if (null === $value) {
            return $defaultValue;
        }
        $value = @trim((string) $value);
        return StrUtil::filterSpecialChars($value);
    }

    public function nextTrimStringMapInteger($stringMap = [], $defaultValue = null)
    {
        $value = $this->nextTrimString();
        if (isset($stringMap[$value])) {
            return $stringMap[$value];
        }
        return $defaultValue;
    }

    public function nextType($typeCls, $defaultValue = null)
    {
        $value = $this->nextTrimString();
        $list = $typeCls::getList();
        foreach ($list as $k => $v) {
            if ($value == $k) {
                return $k;
            }
        }
        return $defaultValue;
    }

    public function nextTypeValue($typeCls, $defaultValue = null)
    {
        $value = $this->nextTrimString();
        $list = $typeCls::getList();
        foreach ($list as $k => $v) {
            if ($value === $v) {
                return $k;
            }
        }
        return $defaultValue;
    }

    public function nextInteger($defaultValue = null): ?int
    {
        $value = $this->next($defaultValue);
        if (null === $value) {
            return null;
        }
        return intval($value);
    }

    public function nextDatetime($defaultValue = null)
    {
        $value = $this->next($defaultValue);
        if (TimeUtil::isDatetimeEmpty($value)) {
            return $defaultValue;
        }
        return $value;
    }
}
