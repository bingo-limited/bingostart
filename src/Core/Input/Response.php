<?php

namespace Bingo\Core\Input;

use BackedEnum;
use Bingo\Enums\Code;
use Illuminate\Http\JsonResponse;
use Bingo\Exceptions\BizException;
use Bingo\Core\Util\FileUtil;
use Bingo\Core\Util\SerializeUtil;
use Illuminate\Support\Facades\Request as Requests;
use Illuminate\Support\HigherOrderTapProxy;
use Psr\Container\ContainerExceptionInterface;
use Psr\Container\NotFoundExceptionInterface;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;
use Bingo\Facade\Format;

class Response
{
    /**
     *  Respond with an accepted response and associate a location and/or content if provided.
     *
     * @param array $data
     * @param string $message
     * @param string $location
     * @return HigherOrderTapProxy
     */
    public function accepted(array $data = [], string $message = '', string $location = ''): HigherOrderTapProxy
    {
        return tap($this->success($data, $message, 202), function ($response) use ($location) {
            if ($location) {
                $response->header('Location', $location);
            }
        });
    }

    /**
     * Respond with a created response and associate a location if provided.
     *
     * @param array|null $data
     * @param string $message
     * @param string $location
     * @return HigherOrderTapProxy
     */
    public function created(?array $data = [], string $message = '', string $location = ''): HigherOrderTapProxy
    {
        return tap($this->success($data, $message, 201), function ($response) use ($location) {
            if ($location) {
                $response->header('Location', $location);
            }
        });
    }

    /**
     * Respond with a no content response.
     */
    public function noContent(string $message = ''): JsonResponse
    {
        return $this->success(message: $message, code: 204);
    }

    /**
     * Alias of success method, no need to specify data parameter.
     */
    public function ok(string $message = '', int|BackedEnum $code = 200): JsonResponse
    {
        return $this->success(message: $message, code: $code);
    }

    /**
     * Alias of the successful method, no need to specify the message and data parameters.
     * You can use ResponseCodeEnum to localize the message.
     */
    public function localize(int|BackedEnum $code = 200): JsonResponse
    {
        return $this->ok(code: $code);
    }

    /**
     * Return a 400 bad request error.
     */
    public function errorBadRequest(string $message = ''): JsonResponse
    {
        return $this->fail($message, 400);
    }

    /**
     * Return a 401 unauthorized error.
     */
    public function errorUnauthorized(string $message = ''): JsonResponse
    {
        return $this->fail($message, 401);
    }

    /**
     * Return a 403 forbidden error.
     */
    public function errorForbidden(string $message = ''): JsonResponse
    {
        return $this->fail($message, 403);
    }

    /**
     * Return a 404 not found error.
     */
    public function errorNotFound(string $message = ''): JsonResponse
    {
        return $this->fail($message, 404);
    }

    /**
     * Return a 405 method not allowed error.
     */
    public function errorMethodNotAllowed(string $message = ''): JsonResponse
    {
        return $this->fail($message, 405);
    }

    /**
     * Return an fail response.
     *
     * @param null $errors
     */
    public function fail(string $message = '', int|BackedEnum $code = 500, $errors = null): JsonResponse
    {
        return Format::data(message: $message, code: $code, error: $errors)->response();
    }

    /**
     * Return a success response.
     *
     * @param array $data
     * @param string $message
     * @param int|BackedEnum $code
     * @return JsonResponse
     */
    public function success(array $data = [], string $message = '', int|BackedEnum $code = 200): JsonResponse
    {
        return Format::data(data: $data, message: $message, code: $code)->response();
    }

    /**
     * -----------------------------------  以下是旧的response 封装的方法  -----------------------------------
     */


    /**
     * 从另外一个接口返回data数据
     * 如果ret是一个标准错误会抛出BizException异常，该异常会被同一捕获
     * 如果ret是一个JsonResponse，会尝试获取实际值
     *
     * @param $ret
     * @param $key
     * @return mixed
     * @throws BizException
     */
    public static function tryGetData($ret, $key = null): mixed
    {
        if ($ret instanceof JsonResponse) {
            $ret = $ret->getData(true);
        }
        if (! isset($ret['code'])) {
            BizException::throws(Code::FAILED, 'ERROR_RESPONSE');
        }
        if ($ret['code'] !== Code::SUCCESS->value) {
            BizException::throws(Code::FAILED, $ret['message']);
        }
        if (null !== $key) {
            if (! array_key_exists($key, $ret['data'])) {
                BizException::throws(Code::FAILED, 'data not exists '.$key);
            }
            return $ret['data'][$key];
        }
        return $ret['data'] ?? null;
    }

    public static function isSuccess($result): bool
    {
        if ($result instanceof JsonResponse) {
            $result = $result->getData(true);
        }
        return (isset($result['code']) && 200 === $result['code']);
    }

    public static function isError($result): bool
    {
        return ! self::isSuccess($result);
    }

    /**
     * 需要独立处理的返回结果，原始返回结果
     * @param $result
     * @return bool
     */
    public static function isRaw($result): bool
    {
        return $result instanceof \Illuminate\Http\Response;
    }

    public static function generateRedirect($redirect): array
    {
        return self::generate(-1, null, null, $redirect);
    }

    public static function generate($code, $message, $data = null, $redirect = null): array
    {
        if (null === $redirect) {
            if ($_redirect = trim(Requests::get('_redirect'))) {
                $redirect = $_redirect;
            }
        }
        $response = [
            'code' => $code,
            'message' => $message,
            'data' => $data,
            'redirect' => $redirect,
        ];
        if (null === $data) {
            unset($response['data']);
        }
        if (null === $redirect) {
            unset($response['redirect']);
        }
        return $response;
    }

    public static function generateSuccessPaginate($page, $pageSize, $paginateData): array
    {
        return self::generateSuccessPaginateData($page, $pageSize, $paginateData['records'], $paginateData['total']);
    }

    public static function generateSuccessPaginateData($page, $pageSize, $records, $total, $maxRecords = -1): array
    {
        return self::generateSuccessData([
            'page' => $page,
            'pageSize' => $pageSize,
            'records' => $records,
            'total' => $total,
            'maxRecords' => $maxRecords,
        ]);
    }

    public static function generateSuccessData($data): array
    {
        return self::generate(Code::SUCCESS->value, 'ok', $data);
    }

    public static function generateSuccess($message = 'ok'): array
    {
        return self::generate(Code::SUCCESS->value, $message);
    }

    public static function generateError($message = 'error', $data = null, $redirect = null): array
    {
        return self::generate(-1, $message, $data, $redirect);
    }

    public static function jsonSuccessData($data): JsonResponse
    {
        return self::json(Code::SUCCESS->value, 'ok', $data);
    }

    public static function jsonSuccess($message = 'ok'): JsonResponse
    {
        return self::json(Code::SUCCESS->value, $message);
    }

    public static function jsonError($message = 'error'): JsonResponse
    {
        return self::json(-1, $message);
    }

    public static function jsonException(\Exception $e): JsonResponse
    {
        return self::jsonError($e->getMessage());
    }

    public static function json($code, $message, $data = null, $redirect = null): JsonResponse
    {
        $response = [
            'code' => $code,
            'message' => $message,
            'data' => $data,
            'redirect' => $redirect,
        ];
        if (null === $redirect) {
            unset($response['redirect']);
        }
        return \Illuminate\Support\Facades\Response::json($response);
    }

    public static function jsonFromGenerate($ret): JsonResponse
    {
        if ($ret instanceof JsonResponse) {
            return $ret;
        }
        if ($ret['code']) {
            return self::json($ret['code'], $ret['message'], $ret['data'] ?? null, $ret['redirect'] ?? null);
        }
        if (! isset($ret['message'])) {
            $ret['message'] = null;
        }
        return self::json($ret['code'], $ret['message'], $ret['data'] ?? null, $ret['redirect'] ?? null);
    }

    public static function jsonIfGenerateSuccess($ret, $message = 'ok', $data = null, $redirect = null): JsonResponse
    {
        if ($ret['code']) {
            return self::json($ret['code'], $ret['message'], empty($ret['data']) ? null : $ret['data'], empty($ret['redirect']) ? null : $ret['redirect']);
        }
        return self::json(Code::SUCCESS->value, $message, $data, $redirect);
    }

    public static function jsonRaw($data): JsonResponse
    {
        return \Illuminate\Support\Facades\Response::json($data);
    }

    public static function jsonp($data, $callback = null): JsonResponse
    {
        if (empty($callback)) {
            $callback = \Illuminate\Support\Facades\Request::get('callback', null);
        }
        if (empty($callback)) {
            return \Illuminate\Support\Facades\Response::json($data);
        }
        if (! preg_match('/^[a-zA-Z_0-9]+$/', $callback)) {
            return \Illuminate\Support\Facades\Response::json([
                'code' => -1,
                'message' => 'callback error',
            ]);
        }
        return \Illuminate\Support\Facades\Response::jsonp($callback, $data);
    }

    public static function sendIfGenerateSuccess($ret, $message = 'ok', $data = null, $redirect = null): JsonResponse
    {
        if ($ret['code']) {
            return self::send($ret['code'], $ret['message'], empty($ret['data']) ? null : $ret['data'], empty($ret['redirect']) ? null : $ret['redirect']);
        }
        return self::send(Code::SUCCESS->value, $message, $data, $redirect);
    }

    public static function sendFromGenerate($ret): JsonResponse
    {
        return self::send($ret['code'], empty($ret['message']) ? null : $ret['message'], empty($ret['data']) ? null : $ret['data'], empty($ret['redirect']) ? null : $ret['redirect']);
    }

    public static function sendError($message, $data = null, $redirect = null): JsonResponse
    {
        return self::send(-1, $message, $data, $redirect);
    }

    public static function sendException(\Exception $e): JsonResponse
    {
        return self::sendError($e->getMessage());
    }

    /**
     * 中断操作，会停止程序执行并输出提示信息，无返回
     * @param $message
     * @return void
     */
    public static function abortmessage($message): void
    {
        abort(200, $message);
    }

    /**
     * 返回一个404页面
     * @return void|JsonResponse
     * @throws ContainerExceptionInterface
     * @throws NotFoundExceptionInterface
     */
    public static function page404()
    {
        if (Request::isAjax()) {
            return self::json(-1, T('Api Not Found : %s', Request::basePath()));
        } else {
            abort(404, T('Page Not Found'));
        }
    }

    /**
     * 页面无权限
     * @param null $message
     * @return void|JsonResponse
     * @throws ContainerExceptionInterface
     * @throws NotFoundExceptionInterface
     */
    public static function pagePermissionDenied($message = null)
    {
        if (Request::isAjax()) {
            return self::json(-1, $message ? $message : T('No Permission'));
        } else {
            abort(403, $message ? $message : T('No Permission'));
        }
    }

    public static function quit($code, $message, $data = null, $redirect = null): void
    {
        $response = [
            'code' => $code,
            'message' => $message,
            'redirect' => $redirect,
            'data' => $data
        ];
        if (null === $redirect) {
            unset($response['redirect']);
        }
        header('Content-Type: application/json; charset=UTF-8');
        echo SerializeUtil::jsonEncode($response);
        exit();
    }

    public static function redirect($redirect): JsonResponse
    {
        return self::send(Code::SUCCESS->value, null, null, $redirect);
    }

    public static function redirectPermanently($redirect)
    {
        return app('redirect')->away($redirect, 301);
    }

    public static function send($code, $message, $data = null, $redirect = null): JsonResponse
    {
        return self::json($code, $message, $data, $redirect);
    }

    public static function download($filename, $content, $headers = [], $filenameFallback = null): \Illuminate\Http\Response
    {
        if (empty($filenameFallback)) {
            $filenameFallback = 'file.'.FileUtil::extension($filename);
        }
        $response = new \Illuminate\Http\Response($content);
        $disposition = $response->headers->makeDisposition(
            ResponseHeaderBag::DISPOSITION_ATTACHMENT,
            $filename,
            $filenameFallback
        );
        $response->headers->set('Content-Disposition', $disposition);
        // 已知部分浏览器（QQ手机浏览器）不设置Content-Type，会导致下载文件失败
        if (! isset($headers['Content-Type'])) {
            $response->headers->set('Content-Type', 'application/octet-stream');
        }
        foreach ($headers as $k => $v) {
            $response->headers->set($k, $v);
        }
        return $response;
    }

    public static function raw($content, $headers = []): \Illuminate\Http\Response
    {
        $response = new \Illuminate\Http\Response($content);
        foreach ($headers as $k => $v) {
            $response->headers->set($k, $v);
        }
        return $response;
    }
}
