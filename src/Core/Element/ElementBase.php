<?php

namespace Bingo\Core\Element;

use Bingo\Core\Extension\Extendable;
use Illuminate\Contracts\Support\Arrayable;
use Illuminate\Contracts\Support\Jsonable;
use JsonSerializable;
use ArrayAccess;

/**
 * 所有元素的 ElementBase 類
 *
 * @package bingo\core\element
 */
abstract class ElementBase extends Extendable implements Arrayable, ArrayAccess, Jsonable, JsonSerializable
{
    /**
     * @var array 此實例的配置值
     */
    public array $config = [];

    /**
     * __construct
     */
    public function __construct(array $config = [])
    {
        parent::__construct();
        $this->initDefaultValues();

        $this->useConfig($config);
    }

    /**
     * initDefaultValues 重寫方法
     */
    protected function initDefaultValues(): void
    {
    }

    /**
     * evalConfig 重寫方法
     */
    public function evalConfig(array $config): void
    {
    }

    /**
     * useConfig 在內部使用
     */
    public function useConfig(array $config): ElementBase
    {
        $this->config = array_merge($this->config, $config);

        $this->evalConfig($config);

        return $this;
    }

    /**
     * getConfig 返回整個配置數組
     */
    public function getConfig(?string $key = null, mixed $default = null): mixed
    {
        if ($key !== null) {
            return $this->get($key, $default);
        }

        return $this->config;
    }

    /**
     * 從元素實例獲取屬性。
     * @param  string  $key
     */
    public function get(string $key, mixed $default = null): mixed
    {
        if (array_key_exists($key, $this->config)) {
            return $this->config[$key];
        }

        return value($default);
    }

    /**
     * toArray 將元素實例轉換為數組。
     * @return array
     */
    public function toArray(): array
    {
        return $this->config;
    }

    /**
     * jsonSerialize 將對象轉換為可 JSON 序列化的內容。
     */
    public function jsonSerialize(): array
    {
        return $this->toArray();
    }

    /**
     * toJson 將元素實例轉換為 JSON。
     * @param  int  $options
     * @return string
     */
    public function toJson($options = 0): string
    {
        return json_encode($this->jsonSerialize(), $options);
    }

    /**
     * offsetExists 確定給定的偏移量是否存在。
     * @param  string  $offset
     * @return bool
     */
    public function offsetExists($offset): bool
    {
        return isset($this->config[$offset]);
    }

    /**
     * offsetGet 獲取給定偏移量的值。
     * @param  string  $offset
     * @return mixed
     */
    public function offsetGet($offset): mixed
    {
        return $this->get($offset);
    }

    /**
     * offsetSet 設置給定偏移量的值。
     * @param  string  $offset
     * @param  mixed  $value
     */
    public function offsetSet($offset, $value): void
    {
        $this->config[$offset] = $value;
    }

    /**
     * offsetUnset 取消設置給定偏移量的值。
     * @param  string  $offset
     * @return void
     */
    public function offsetUnset($offset): void
    {
        unset($this->config[$offset]);
    }

    /**
     * __call 處理對元素實例的動態調用以設置配置。
     * @param  string  $method
     * @param  array  $parameters
     * @return $this
     */
    public function __call($method, $parameters): self
    {
        $this->config[$method] = count($parameters) > 0 ? $parameters[0] : true;

        return $this;
    }

    /**
     * __get 動態檢索屬性的值。
     * @param  string  $key
     * @return mixed
     */
    public function __get($key): mixed
    {
        return $this->get($key);
    }

    /**
     * __set 動態設置屬性的值。
     *
     * @param  string  $key
     * @param  mixed  $value
     * @return void
     */
    public function __set($key, mixed $value): void
    {
        $this->offsetSet($key, $value);
    }

    /**
     * __isset 動態檢查屬性是否設置。
     *
     * @param  string  $key
     * @return bool
     */
    public function __isset(string $key): bool
    {
        return $this->offsetExists($key);
    }

    /**
     * __unset 動態取消設置屬性。
     *
     * @param  string  $key
     * @return void
     */
    public function __unset(string $key): void
    {
        $this->offsetUnset($key);
    }
}