<?php

namespace Bingo\Core\Element;

use Illuminate\Support\Collection;
use IteratorAggregate;

/**
 * ElementHolder 是一個通用集合，用於通過引用傳遞一組元素，
 * 允許通過數組和對象表示法進行訪問。
 *
 * @package bingo\core\element
 */
class ElementHolder extends ElementBase implements IteratorAggregate
{
    /**
     * @var array touchedElements 由 getTouchedElements 使用
     */
    protected array $touchedElements = [];

    /**
     * getTouchedElements 返回已被訪問的字段名
     */
    public function getTouchedElements(): array
    {
        return $this->touchedElements;
    }

    /**
     * 從持有者實例中獲取一個元素。
     * @param  string  $key
     * @param  mixed  $default
     * @return mixed
     */
    public function get($key, $default = null): mixed
    {
        if (isset($this->touchedElements[$key])) {
            return $this->touchedElements[$key];
        }

        if (isset($this->config[$key])) {
            return $this->touchedElements[$key] = $this->config[$key];
        }

        return parent::get($key, $default);
    }

    /**
     * 獲取元素的迭代器。
     */
    public function getIterator(): Collection
    {
        return new Collection($this->config);
    }
}