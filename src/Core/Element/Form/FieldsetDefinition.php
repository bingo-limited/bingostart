<?php

namespace Bingo\Core\Element\Form;

use Bingo\Core\Element\ElementBase;
use IteratorAggregate;
use ArrayIterator;

/**
 * FieldsetDefinition
 *
 * @method FieldsetDefinition defaultTab(string $defaultTab) defaultTab 是在未指定時使用的默認標籤標籤
 * @method FieldsetDefinition suppressTabs(bool $suppressTabs) suppressTabs 如果設置為 TRUE，字段將不會顯示在標籤中
 *
 * @package bingo\core\element
 */
class FieldsetDefinition extends ElementBase implements IteratorAggregate
{
    /**
     * @var array fields 是這些標籤的窗格字段的集合
     */
    protected array $fields = [];

    /**
     * 初始化此範圍的默認值
     */
    protected function initDefaultValues(): void
    {
        $this
            ->defaultTab('Misc')
            ->suppressTabs(false)
        ;
    }

    /**
     * 將字段添加到標籤集合中
     */
    public function addField($name, FieldDefinition $field): void
    {
        $this->fields[$name] = $field;
    }

    /**
     * 通過名稱從所有標籤中移除字段
     * @param string $name
     * @return boolean
     */
    public function removeField($name): bool
    {
        if (isset($this->fields[$name])) {
            unset($this->fields[$name]);
            return true;
        }

        return false;
    }

    /**
     * hasFields 如果這些標籤已註冊任何字段，則返回 true
     * @return bool
     */
    public function hasFields()
    {
        return count($this->fields) > 0;
    }

    /**
     * getFields 返回註冊字段的數組，包括格式為 array[tab][field] 的標籤
     * @return array
     */
    public function getFields()
    {
        $fieldsTabbed = [];

        foreach ($this->fields as $name => $field) {
            $tabName = $field->tab ?: $this->defaultTab;
            $fieldsTabbed[$tabName][$name] = $field;
        }

        return $fieldsTabbed;
    }

    /**
     * 獲取指定的字段對象
     */
    public function getField(string $field)
    {
        if (isset($this->fields[$field])) {
            return $this->fields[$field];
        }


    }

    /**
     * getAllFields 返回註冊字段的數組，不包括標籤
     * @return array
     */
    public function getAllFields()
    {
        return $this->fields;
    }

    /**
     * sortAllFields 將根據其 order 屬性對定義的字段進行排序
     */
    public function sortAllFields()
    {
        uasort($this->fields, static function ($a, $b) {
            return $a->order - $b->order;
        });
    }

    /**
     * getIterator 獲取項目的迭代器
     * @return ArrayIterator
     */
    public function getIterator(): ArrayIterator
    {
        return new ArrayIterator(
            $this->suppressTabs
                ? $this->getAllFields()
                : $this->getFields()
        );
    }
}