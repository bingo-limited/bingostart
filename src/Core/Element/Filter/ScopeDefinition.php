<?php

namespace Bingo\Core\Element\Filter;

use Bingo\Core\Element\ElementBase;

/**
 * ScopeDefinition
 *
 * @method ScopeDefinition scopeName(string $name) 此範圍的 scopeName
 * @method ScopeDefinition label(string $label) 此範圍的 label
 * @method ScopeDefinition shortLabel(string $shortLabel) 在列表標頭中使用的 shortLabel
 * @method ScopeDefinition value(mixed $value) 此範圍的當前值
 * @method ScopeDefinition nameFrom(string $column) 用於顯示名稱的模型屬性 nameFrom
 * @method ScopeDefinition valueFrom(mixed $value) 用於源值的模型屬性 valueFrom
 * @method ScopeDefinition descriptionFrom(string $column) 用於描述的列 descriptionFrom
 * @method ScopeDefinition dependsOn(array $scopes) 依賴於其他範圍，當其他範圍被修改時，此範圍將更新
 * @method ScopeDefinition context(string $context) 此範圍的上下文可見性
 * @method ScopeDefinition defaults(mixed $value) 範圍的默認值
 * @method ScopeDefinition conditions(string $sql) 以原始 SQL 格式應用的條件
 * @method ScopeDefinition scope(string $method) 模型的範圍方法
 * @method ScopeDefinition cssClass(string $class) 附加到範圍容器的 cssClass
 * @method ScopeDefinition emptyOption(string $label) 有意選擇空值的 emptyOption 標籤（可選）
 * @method ScopeDefinition disabled(bool $value) 範圍的禁用設置
 * @method ScopeDefinition order(int $order) 顯示時的排序號
 * @method ScopeDefinition after(string $after) 使用顯示順序（+1）將此範圍放在另一個現有範圍名稱之後
 * @method ScopeDefinition before(string $before) 使用顯示順序（-1）將此範圍放在另一個現有範圍名稱之前
 *
 * @package bingo\core\element
 */
class ScopeDefinition extends ElementBase
{
    /**
     * @var array $config
     */
    public array $config = [];

    /**
     * 初始化此範圍的默認值
     */
    protected function initDefaultValues(): void
    {
        $this
            ->displayAs('group')
            ->nameFrom('name')
            ->disabled(false)
            ->order(-1);
    }

    /**
     * 使用配置
     *
     * @param array<string, mixed> $config
     */
    public function useConfig(array $config): self
    {
        $this->config = array_merge($this->config, $config);

        // 配置默認代理到 defaults
        if (array_key_exists('default', $this->config)) {
            $this->defaults($this->config['default']);
        }

        return $this;
    }

    /**
     * 此範圍的顯示類型。支持的模式有：
     * - group - 按 ID 組過濾。默認。
     * - checkbox - 通過簡單的切換開關進行過濾。
     */
    public function displayAs(string $type): self
    {
        $this->config['type'] = $type;

        return $this;
    }

    /**
     * 如果已指定選項，則返回 true
     */
    public function hasOptions(): bool
    {
        return isset($this->config['options']) &&
            (is_array($this->config['options']) || is_callable($this->config['options']));
    }

    /**
     * 獲取/設置下拉列表、單選列表和複選框列表的選項
     *
     * @param callable|array|null $value
     * @return array|self
     */
    public function options(array|callable $value = null): array|ScopeDefinition|static
    {
        if ($value === null) {
            if (isset($this->config['options'])) {
                if (is_array($this->config['options'])) {
                    return $this->config['options'];
                }

                if (is_callable($this->config['options'])) {
                    return ($this->config['options'])();
                }
            }

            return [];
        }

        $this->config['options'] = $value;

        return $this;
    }

    /**
     * 設置範圍值並將值合併為配置
     *
     * @param mixed $value
     */
    public function setScopeValue(mixed $value): void
    {
        if (is_array($value)) {
            $this->config = array_merge($this->config, $value);
        }

        $this->scopeValue($value);
    }

    /**
     * 設置範圍值
     *
     * @param mixed $value
     */
    protected function scopeValue(mixed $value): void
    {
        $this->config['scopeValue'] = $value;
    }
}