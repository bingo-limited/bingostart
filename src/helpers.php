<?php

declare(strict_types=1);

use Bingo\Core\Services\ShortcodeService;
use Illuminate\Console\Application as Artisan;
use Illuminate\Console\Command;
use Illuminate\Contracts\Support\Htmlable;
use Illuminate\Support\Arr;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Request;
use Illuminate\Support\Str;
use Symfony\Component\Finder\Finder;
use Bingo\BingoStart;
use Bingo\App\Core\CurrentApp;
use Bingo\Core\Assets\AssetsUtil;
use Bingo\Core\Config\BingoConfig;
use Bingo\Core\Util\SerializeUtil;
use Bingo\Exceptions\BizException;
use Illuminate\Config\Repository;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Support\Facades\View;
use Bingo\Module\ModuleManager;
use Illuminate\Support\HtmlString;
use Psr\Container\ContainerExceptionInterface;
use Psr\Container\NotFoundExceptionInterface;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Storage;

/**
 * @Util BinGoCore版本
 * @desc 获取BinGoCore版本
 * @return string 版本号
 */
function bingostart_version(): string
{
    return BingoStart::VERSION;
}


/**
 * load commands
 */
if (! function_exists('loadCommands')) {
    /**
     * @throws ReflectionException
     */
    function loadCommands($paths, $namespace, $searchPath = null): void
    {
        try {
            if (! $searchPath) {
                $searchPath = dirname($paths).DIRECTORY_SEPARATOR;
            }

            $paths = Collection::make(Arr::wrap($paths))->unique()->filter(function ($path) {
                return is_dir($path);
            });

            if ($paths->isEmpty()) {
                return;
            }

            foreach ((new Finder())->in($paths->toArray())->files() as $command) {
                $command = $namespace.str_replace(['/', '.php'], ['\\', ''], Str::after($command->getRealPath(), $searchPath));

                if (is_subclass_of($command, Command::class) &&
                    ! (new ReflectionClass($command))->isAbstract()) {
                    Artisan::starting(function ($artisan) use ($command) {
                        $artisan->resolve($command);
                    });
                }
            }
        } catch (\Exception $e) {
            // 处理加载命令过程中的异常
            Log::error("Error loading commands: ".$e->getMessage());
        }
    }

}

/**
 * table prefix
 */
if (! function_exists('withTablePrefix')) {
    function withTablePrefix(string $table): string
    {
        return DB::connection()->getTablePrefix().$table;
    }
}

/**
 * get guard name
 */
if (! function_exists('getGuardName')) {
    function getGuardName(): string
    {
        $guardKeys = array_keys(config('bingo.auth.guards', []));

        if (count($guardKeys)) {
            return $guardKeys[0];
        }

        return 'sanctum';
    }
}

/**
 * get table columns
 */
if (! function_exists('getTableColumns')) {
    function getTableColumns(string $table): array
    {
        $SQL = 'desc '.withTablePrefix($table);

        $columns = [];

        foreach (DB::select($SQL) as $column) {
            $columns[] = $column->Field;
        }

        return $columns;
    }
}

if (! function_exists('dd_')) {
    /**
     * @param mixed ...$vars
     * @return never
     */
    function dd_(...$vars): never
    {
        header('Access-Control-Allow-Origin: *');
        header('Access-Control-Allow-Methods: *');
        header('Access-Control-Allow-Headers: *');

        dd(...$vars);
    }
}

if (! function_exists('getAuthUserModel')) {
    /**
     * get user model
     *
     * @return mixed
     */
    function getAuthUserModel(): mixed
    {
        return config('bingo.auth_model');
    }
}

if (! function_exists('importTreeData')) {
    /**
     * import tree data
     *
     * @param array $data
     * @param string $table
     * @param string $pid
     * @param string $primaryKey
     * @param array $parentIdMap
     */
    function importTreeData(array $data, string $table, string $pid = 'parent_id', string $primaryKey = 'id', array &$parentIdMap = []): void
    {
        foreach ($data as $value) {
            $oldId = $value[$primaryKey] ?? null;
            if ($oldId !== null) {
                unset($value[$primaryKey]);
            }

            $children = $value['children'] ?? null;
            if ($children !== null) {
                unset($value['children']);
            }

            // 如果存在parentIdMap映射，则更新当前节点的parent_id
            if (isset($parentIdMap[$value[$pid]])) {
                $value[$pid] = $parentIdMap[$value[$pid]];
            }

            // 首先查询是否存在
            $menu = DB::table($table)->where([
                ['permission_name', '=', $value['permission_name']],
                ['module', '=', $value['module']],
                ['permission_mark', '=', $value['permission_mark']],
            ])->first();

            if ($menu) {
                $newId = $menu->id;
            } else {
                $newId = DB::table($table)->insertGetId($value);
            }

            // 如果这是一个顶级节点，或者我们需要维护旧的ID到新的ID的映射
            if ($oldId !== null && $value[$pid] == 0) {
                $parentIdMap[$oldId] = $newId;
            }

            if ($children !== null) {
                foreach ($children as &$child) {
                    $child[$pid] = $newId; // 确保子节点的parent_id是新插入的ID
                }
                importTreeData($children, $table, $pid, $primaryKey, $parentIdMap);
            }
        }
    }


}

if (! function_exists('isRequestFromDashboard')) {
    /**
     * @return bool
     */
    function isRequestFromDashboard(): bool
    {
        return Request::hasHeader('Request-from')
            && Str::of(Request::header('Request-from'))->lower()->exactly('dashboard');
    }
}

if (! function_exists('loadCachedAdminRoutes')) {
    function loadCachedAdminRoutes(): void
    {
        if (routesAreCached()) {
            if (app()->runningInConsole()) {
                require BingoStart::getRouteCachePath();
            } elseif (isRequestFromDashboard()) {
                require BingoStart::getRouteCachePath();
            }
        }
    }
}

if (! function_exists('routesAreCached')) {
    function routesAreCached(): bool
    {
        return file_exists(BingoStart::getRouteCachePath());
    }
}

if (! function_exists('getModuleNameFromClassName')) {
    function getModuleNameFromClassName($className): ?string
    {
        // 定义一个分隔符，用于分割命名空间
        $delimiter = '\\';
        $parts = explode($delimiter, $className);

        $modulesIndex = array_search('Modules', $parts);
        if ($modulesIndex !== false && isset($parts[$modulesIndex + 1])) {
            return $parts[$modulesIndex + 1];
        }
        // 如果没有找到 'Modules' 或者 'Modules' 后面没有元素，返回 null 或者自定义错误消息
        return null;
    }
}


/**
 * 管理绝对路径
 * @desc 生成Admin的文件绝对路径
 * @param string $path
 * @return string
 */
function bingostart_admin_path(string $path = ''): string
{
    return ucfirst(config('bingo.admin.directory')).($path ? DIRECTORY_SEPARATOR.$path : $path);
}

/**
 * @Util Admin路径
 * @desc 生成Admin的路径，自动加前缀
 * @param string $url 路径
 * @param array $param 参数
 * @return string
 * @example
 * // 返回 /admin/aaa/bbb
 * bingostart_admin_url('aaa/bbb')
 * // 返回 /admin/aaa/bbb?x=y
 * bingostart_admin_url('aaa/bbb',['x'=>'y'])
 */
function bingostart_admin_url(string $url = '', array $param = []): string
{
    if (! empty($param)) {
        $url = $url.'?'.http_build_query($param);
    }
    $prefix = config('bingo.admin.prefix');
    $prefix = config('bingo.subdir').$prefix;
    if ('/' != $prefix) {
        $prefix .= '/';
    }
    return $prefix.$url;
}

/**
 * 判断是否为Tab
 * @return boolean
 */
function bingostart_admin_is_tab(): bool
{
    return boolval(View::shared('_isTab'));
}

/**
 * 生成Web的文件绝对路径
 * @param string $path
 * @return string 路径
 */
function bingostart_web_path(string $path = ''): string
{
    return ucfirst(config('bingo.web.directory')).($path ? DIRECTORY_SEPARATOR.$path : $path);
}

/**
 * @Util 生成完整的Web路径
 * @param string $url 路径
 * @param array $param 参数
 * @return string 地址
 * @throws ContainerExceptionInterface
 * @throws NotFoundExceptionInterface
 * @example
 * // 返回 http://www.example.com/aaa/bbb
 * bingostart_web_full_url('aaa/bbb')
 * // ���回 http://www.example.com/aaa/bbb?x=y
 * bingostart_web_full_url('aaa/bbb',['x'=>'y'])
 */
function bingostart_web_full_url(string $url = '', array $param = []): string
{
    $domainUrl = \Bingo\Core\Input\Request::domainUrl();
    if ('http://localhost' == $domainUrl) {
        $domainUrl = rtrim(bingostart_config('siteUrl', BingoConfig::DEFAULT_LANG, 'http://localhost'), '/');
    }
    return $domainUrl.bingostart_web_url($url, $param);
}

/**
 * @Util Web路径
 * @desc 生成Web的路径，自动加前缀
 * @param string $url 路径
 * @param array $param 参数
 * @return string 地址
 * @example
 * // 返回 /aaa/bbb
 * bingostart_web_url('aaa/bbb')
 * // 返回 /aaa/bbb?x=y
 * bingostart_web_url('aaa/bbb',['x'=>'y'])
 */
function bingostart_web_url(string $url = '', array $param = []): string
{
    if (! empty($param)) {
        $url = $url.'?'.http_build_query($param);
    }
    $prefix = config('bingo.web.prefix');
    $prefix = config('bingo.subdir').$prefix;
    return $prefix.$url;
}

/**
 * 生成Api文件绝对路径
 * @param string $path
 * @return string
 */
function bingostart_api_path(string $path = ''): string
{
    return ucfirst(config('bingo.api.directory')).($path ? DIRECTORY_SEPARATOR.$path : $path);
}


/**
 * @Util Api路径
 * @desc 生成Api的路径，自动加前缀
 * @param string $url 路径
 * @param array $param 参数
 * @return string
 * @example
 * // 返回 /api/aaa/bbb
 * bingostart_api_url('aaa/bbb')
 * // 返回 /api/aaa/bbb?x=y
 * bingostart_api_url('aaa/bbb',['x'=>'y'])
 */
function bingostart_api_url(string $url = '', array $param = []): string
{
    if (! empty($param)) {
        $url = $url.'?'.http_build_query($param);
    }
    $prefix = config('bingo.api.prefix');
    $prefix = config('bingo.subdir').$prefix;
    return $prefix.'/'.$url;
}

/**
 * 生成OpenApi的文件绝对路径
 * @param string $path
 * @return string
 */
function bingostart_open_api_path(string $path = ''): string
{
    return ucfirst(config('bingo.openApi.directory')).($path ? DIRECTORY_SEPARATOR.$path : $path);
}

/**
 * OpenApi路径
 * @desc 生成Api的路径，自动加前缀
 * @param string $url 路径
 * @return string
 * @example
 * // 返回 /open_api/aaa/bbb
 * bingostart_open_api_url('aaa/bbb')
 * // 返回 /open_api/aaa/bbb?x=y
 * bingostart_open_api_url('aaa/bbb',['x'=>'y'])
 */
function bingostart_open_api_url(string $url = ''): string
{
    $prefix = config('bingo.openApi.prefix');
    $prefix = config('bingo.subdir').$prefix;
    return $prefix.$url;
}

/**
 * 获取模块系统配置
 * @param $module string 模块名称
 * @param $key string 配置名称
 * @param $default integer|boolean|array|string|null 默认值
 * @return mixed|null
 * @throws ContainerExceptionInterface
 * @throws NotFoundExceptionInterface
 */
function bingostart_module_config(string $module, string $key, int|bool|array|string $default = null): mixed
{
    return ModuleManager::getModuleConfig($module, $key, $default);
}

/**
 * @Util 获取多个配置中第一个非空值
 * @param $keys array 多个配置名
 * @param $default string 默认值
 * @return array|bool|int|mixed|BingoConfig|string
 */
function bingostart_configs(array $keys, string $lang = BingoConfig::DEFAULT_LANG, string $default = ''): mixed
{
    foreach ($keys as $key) {
        $v = "";
        try {
            $v = bingostart_config($key, $lang);
        } catch (NotFoundExceptionInterface|ContainerExceptionInterface) {
        }
        if ($v) {
            return $v;
        }
    }
    return $default;
}

/**
 * @Util 获取配置
 * @desc 用于获取表 config 中的配置选项
 * @param $key string|null 配置名称
 * @param string $default string|array|boolean|integer 默认值，不能为 null
 * @param $useCache bool 启用缓存，默认为true
 * @return array|bool|int|BingoConfig|string|null 返回配置值或配置对象
 * @throws ContainerExceptionInterface
 * @throws NotFoundExceptionInterface
 * @example
 * // 网站名称
 * bingostart_config('siteName','zh_CN','[默认名称]');
 * // 获取一个配置数组，数组需存储成 json 格式
 * bingostart_config()->getArray('xxx','zh_CN')
 * // 设置配置项
 * bingostart_config()->set('xxx','aaa','zh_CN')
 */
function bingostart_config(string $key = null, string $lang = BingoConfig::DEFAULT_LANG, string $default = '', bool $useCache = true): array|bool|int|BingoConfig|string|null
{
    static $lastKey = null;
    static $lastValue = null;
    try {
        if ($key && $key === $lastKey) {
            return $lastValue;
        }
        if (is_null($key)) {
            return app('bingostartConfig');
        }
        $lastKey = $key;
        $configDefault = $default;
        if (is_array($default)) {
            $configDefault = SerializeUtil::jsonEncode($default);
        }
        $v = app('bingostartConfig')->get($key, $lang, $configDefault, $useCache);
        if (true === $default || false === $default) {
            $lastValue = boolval($v);
            return $lastValue;
        }
        if (is_int($default)) {
            $lastValue = intval($v);
            return $lastValue;
        }
        if (is_array($default)) {
            $v = @json_decode($v, true);
            if (null === $v) {
                $lastValue = $default;
                return $default;
            }
            $lastValue = $v;
            return $v;
        }
        $lastValue = $v;
        return $v;
    } catch (Exception) {
        $lastValue = $default;
        return $default;
    }
}

/**
 * @Util 获取配置资源路径
 * @param $key string 配置名称
 * @param $default string 默认值
 * @return string
 * @throws ContainerExceptionInterface
 * @throws NotFoundExceptionInterface
 */
function bingostart_config_asset_url(string $key, string $default = ''): string
{
    $value = bingostart_config($key, BingoConfig::DEFAULT_LANG, $default);
    return AssetsUtil::fixFull($value);
}

/**
 * @Util 模块判断
 * @desc 判断模块是否已安装并启用
 * @param $module string 模块名称，如 Member
 * @param $version string|null 模块版本要求，如 1.0.0， >=1.0.0
 * @return bool 模块是否安装并启用
 * @throws ContainerExceptionInterface
 * @throws NotFoundExceptionInterface
 * @throws BizException
 * @example
 * // 模块Member是否安装并启用
 * bingostart_module_enabled('Member')
 * // 模块Member是否安装了 >=1.2.0 的版本
 * bingostart_module_enabled('Member','>=1.2.0')
 */
function bingostart_module_enabled(string $module, string $version = null): bool
{
    if (null === $version) {
        return ModuleManager::isModuleEnabled($module);
    } else {
        return ModuleManager::isModuleEnableMatch($module, $version);
    }
}

/**
 * @Util 获取语言标题
 * @param $locale
 * @return Repository|Application|\Illuminate\Foundation\Application|\Illuminate\Routing\Route|mixed|object|string|null
 */

function T_locale_title($locale = null): mixed
{
    if (null === $locale) {
        try {
            $locale = T_locale();
        } catch (NotFoundExceptionInterface|ContainerExceptionInterface $e) {
        }
    }
    $lang = config('bingo.i18n.lang', []);
    return isset($lang[$locale]) ? $lang[$locale] : $locale;
}

/**
 * @Util 获取当前语言
 * @param null $locale
 * @return Repository|Application|\Illuminate\Foundation\Application|\Illuminate\Routing\Route|mixed|object|string|null
 * @throws ContainerExceptionInterface
 * @throws NotFoundExceptionInterface
 */

function T_locale($locale = null): mixed
{
    $sessionLocaleKey = '_locale';

    // 如果当前应用是管理员应用，使用 '_adminLocale' 作为会话键
    if (CurrentApp::is(CurrentApp::ADMIN)) {
        $sessionLocaleKey = '_adminLocale';
    }

    // 如果传入了新的 locale 值，则重置会话中的值并返回
    if (null !== $locale) {
        Session::put($sessionLocaleKey, $locale);
        return $locale;
    }

    // routeLocale > sessionLocale > i18nLocale > locale > fallbackLocale
    // 检查路由中的 locale 参数
    $routeLocale = Request::get('locale');
    $currentLocale = $routeLocale;

    // 如果路由中没有 locale 参数，则检查会话中的 locale
    if (empty($currentLocale)) {
        $sessionLocale = Session::get($sessionLocaleKey, null);
        $currentLocale = $sessionLocale;
    }

    // 如果会话中没有 locale，并且启用了多语言模块，则检查多语言模块中的默认语言
    if (empty($currentLocale)) {
        if (! CurrentApp::is(CurrentApp::ADMIN) && ModuleManager::isModuleInstalled('Multilingual')) {
            $i18nLocale = \Modules\Multilingual\Domain\LangUtil::getDefault('mark');
            $currentLocale = $i18nLocale;
        }
    }

    // 如果多语言模块中没有设置 locale，则使用配置文件中的默认语言
    if (empty($currentLocale)) {
        $currentLocale = config('app.locale');
    }

    // 如果配置文件中没有设置默认语言，则使用后备语言
    if (empty($currentLocale)) {
        $fallbackLocale = config('app.fallback_locale');
        $currentLocale = $fallbackLocale;
    }

    // 更新会话中的 locale
    Session::put($sessionLocaleKey, $currentLocale);
    return $currentLocale;
}

/**
 * 获取支持语言列表
 * 该函数用于后台多语言
 * $type = 1 后台多语言列表
 * $type = 2 前台多语言列表
 * $type = 3 所有多语言列表
 * @param int $type
 * @return array
 * @throws ContainerExceptionInterface
 * @throws NotFoundExceptionInterface
 */
function T_list(int $type = 1): array
{
    if (! CurrentApp::is(CurrentApp::ADMIN)
        &&
        ModuleManager::isModuleInstalled('Multilingual')) {
        $listAll = \Modules\Multilingual\Domain\LangUtil::listAll($type);
        $langArr = [];
        foreach ($listAll as $list) {
            $langArr[] = [
                'label' => $list['title'],
                'value' => $list['mark']
            ];
        }
        return $langArr;
    }
    return [];
}


/**
 * @Util 多语言
 * @desc 获取多语言翻译，支持多模块多语言
 * @param $name string 多语言
 * @param ...$params array|string|int 多语言参数
 * @return string 多语言翻译
 * @throws ContainerExceptionInterface
 * @throws NotFoundExceptionInterface
 * @example
 * // 返回 消息
 * 支持如下几种格式
 * T('Message');
 * T('Message %s','Hello');
 * T('Message.hello');
 * T('Cms::Message');
 * T('Cms::Message %s','Hello');
 * T('Cms::Message.hello');
 * T('Cms::Message.hello', ['startTag' => '<code>', 'endTag' => '</code>']);
 * // 返回 文件最大为10M
 */
function T(string $name, ...$params): string
{
    static $trackMissing = null;
    static $trackMissingData = null;
    $useLocale = T_locale();
    if (null === $trackMissing) {
        $trackMissing = config('bingo.trackMissingLang', false);
    }
    if (empty($useLocale)) {
        return $name;
    }
    if ($trackMissing && null === $trackMissingData) {
        $trackMissingData = [];
        if (file_exists($file = storage_path('cache/lang_missing.php'))) {
            $trackMissingData = (require $file);
        }
        register_shutdown_function(function () use (&$trackMissingData, $file) {
            ksort($trackMissingData);
            file_put_contents($file, '<?ph'.'p return '.var_export($trackMissingData, true).';');
        });
    }

    if (! CurrentApp::is(CurrentApp::ADMIN)
        &&
        ModuleManager::isModuleInstalled('Multilingual')) {
        $langTrans = \Modules\Multilingual\Domain\LangTransUtil::map();
        if ($useLocale && isset($langTrans[$useLocale][$name])) { // 通过后台数据库变量获取
            if ($trackMissing && isset($trackMissingData[$name])) {
                unset($trackMissingData[$name]);
            }
            return replaceParams($langTrans[$useLocale][$name], $params);
        }
    }

    // 拆分命名空间和实际的键名
    $namespace = null;
    $key = $name;
    if (str_contains($name, '::')) {
        [$namespace, $key] = explode('::', $name, 2);
    }

    // 生成查找的键名列表, 默认支持 base. 和 bingo::base.
    $ids = $namespace ? [$namespace.'::base.'.$key, $namespace.'::'.$key] : ['base.'.$key, 'bingo::base.'.$key];

    $nameRaw = $key;
    if (preg_match('/^[a-z0-9]+\.(.+)$/i', $key, $mat)) {
        array_unshift($ids, $key);
        $nameRaw = $mat[1];
    }

    foreach ($ids as $id) {
        $trans = trans($id, [], $useLocale);
        if ($trans !== $id) {
            if ($trackMissing && isset($trackMissingData[$nameRaw])) {
                unset($trackMissingData[$nameRaw]);
            }
            return replaceParams($trans, $params);
        }
    }

    if ($trackMissing) {
        $trackMissingData[$nameRaw] = $nameRaw;
    }
    return replaceParams($nameRaw, $params);
}

/**
 * @desc 替换翻译字符串中的参数
 * @param $string string 翻译字符串
 * @param $params array|string|int 翻译参数
 * @return string 替换后的字符串
 */
function replaceParams(string $string, $params): string
{
    if (! empty($params)) {
        if (is_array($params[0])) {
            foreach ($params[0] as $key => $value) {
                $string = str_replace(':'.$key, $value, $string);
            }
        } else {
            $string = call_user_func_array('sprintf', array_merge([$string], $params));
        }
    }
    return $string;
}


/**
 * 选中状态
 * @param $match
 * @param string $output
 * @return string
 */
function bingostart_baseurl_active($match, string $output = 'active'): string
{
    $pass = false;
    $url = \Bingo\Core\Input\Request::basePathWithQueries();
    if (is_string($match)) {
        if (! starts_with($match, '/')) {
            $match = bingostart_web_url($match);
        }
        if (\Bingo\Core\Util\ReUtil::isWildMatch($match, $url)) {
            $pass = true;
        }
    } elseif (is_array($match)) {
        foreach ($match as $item) {
            if (! starts_with($item, '/')) {
                $item = bingostart_web_url($item);
            }
            if (\Bingo\Core\Util\ReUtil::isWildMatch($item, $url)) {
                $pass = true;
                break;
            }
        }
    }
    if ($pass) {
        return $output;
    }
    return '';
}


if (! function_exists('array_build')) {
    function array_build($array, callable $callback): array
    {
        $results = [];

        foreach ($array as $key => $value) {
            list($innerKey, $innerValue) = call_user_func($callback, $key, $value);

            $results[$innerKey] = $innerValue;
        }

        return $results;
    }
}

if (! function_exists('starts_with')) {
    function starts_with($haystack, $needles): bool
    {
        foreach ((array) $needles as $needle) {
            if ($needle != '' && mb_strpos($haystack, $needle) === 0) {
                return true;
            }
        }
        return false;
    }
}

if (! function_exists('array_get')) {
    function array_get($array, $key, $default = null)
    {
        if (is_null($key)) {
            return $array;
        }

        if (isset($array[$key])) {
            return $array[$key];
        }

        foreach (explode('.', $key) as $segment) {
            if (! is_array($array) || ! array_key_exists($segment, $array)) {
                return value($default);
            }

            $array = $array[$segment];
        }

        return $array;
    }
}


if (! function_exists('array_has')) {
    function array_has($array, $key): bool
    {
        if (empty($array) || is_null($key)) {
            return false;
        }

        if (array_key_exists($key, $array)) {
            return true;
        }

        foreach (explode('.', $key) as $segment) {
            if (! is_array($array) || ! array_key_exists($segment, $array)) {
                return false;
            }

            $array = $array[$segment];
        }

        return true;
    }
}

if (! function_exists('array_except')) {
    function array_except($array, $keys)
    {
        array_forget($array, $keys);

        return $array;
    }
}

if (! function_exists('array_forget')) {
    function array_forget(&$array, $keys): void
    {
        $original = &$array;

        $keys = (array) $keys;

        if (count($keys) === 0) {
            return;
        }

        foreach ($keys as $key) {
            $parts = explode('.', $key);

            while (count($parts) > 1) {
                $part = array_shift($parts);

                if (isset($array[$part]) && is_array($array[$part])) {
                    $array = &$array[$part];
                } else {
                    $parts = [];
                }
            }

            unset($array[array_shift($parts)]);

            // clean up after each pass
            $array = &$original;
        }
    }
}
if (! function_exists('ends_with')) {
    function ends_with($haystack, $needles): bool
    {
        foreach ((array) $needles as $needle) {
            if ((string) $needle === mb_substr($haystack, -mb_strlen($needle))) {
                return true;
            }
        }
        return false;
    }
}

if (! function_exists('route_get')) {
    function route_get($name, $parameters = [], $absolute = true, $isReplace = true): array|string
    {
        $url = app('url')->route($name, $parameters, $absolute);

        if ($isReplace) {
            $url = str_replace('/admin/', '', $url);
        }

        if (config('bingo.https')) {
            return str_replace('http://', 'https://', $url);
        }
        return $url;
    }
}

function admin_route($path = ''): string
{

    $prefix = trim(config('bingo.admin.prefix'));
    $path = str_replace($prefix.'/', '/', $path);

    return (string) Str::of($path)->finish('/')->start('/')->rtrim("/");
}

if (! function_exists('admin_file_url')) {
    function admin_file_url($path)
    {
        if (! $path) {
            return $path;
        }

        if (Str::startsWith($path, ["http://", "https://"])) {
            return $path;
        }

        $disk = Storage::disk(config('bingo.upload.disk'));
        return $disk->path($path);
    }
}

if (! function_exists('admin_file_restore_path')) {
    function admin_file_restore_path($url)
    {
        if (! $url) {
            return $url;
        }
        if (Str::startsWith($url, ["http://", "https://"])) {
            $disk = Storage::disk(config('bingo.upload.disk'));
            $base_path = $disk->path('');
            return str_replace($base_path, '', $url);
        }
        return $url;
    }
}

function admin_path($path = ''): string
{
    return ucfirst(config('bingo.directory')).($path ? DIRECTORY_SEPARATOR.$path : $path);
}

if (! function_exists('admin_asset')) {
    function admin_asset($path): string
    {
        return (config('bingo.https') || config('bingo.secure')) ? secure_asset($path) : asset($path);
    }
}

function vite_assets(): HtmlString
{

    if (app()->environment('local')) {
        $devServerIsRunning = false;
        $viteUrl = env("VITE_URL");
        if ($viteUrl) {
            try {
                Http::get($viteUrl);
                $devServerIsRunning = true;
            } catch (Exception) {
            }
            if ($devServerIsRunning) {
                return new HtmlString(
                    <<<HTML
            <script type="module" src="$viteUrl/@vite/client"></script>
            <script type="module" src="$viteUrl/resources/admin/app.ts"></script>
        HTML
                );
            }
        }
    }

    return new HtmlString(
        <<<HTML
        <script type="module" src="../resources/admin/app.ts"></script>
        <script src="./static/config.js"></script>
    HTML
    );
}

function arr2tree($list, $id = 'id', $pid = 'parent_id', $son = 'children'): array
{
    if (! is_array($list)) {
        $list = collect($list)->toArray();
    }

    [$tree, $map] = [[], []];
    foreach ($list as $item) {
        $map[$item[$id]] = $item;
    }

    foreach ($list as $item) {
        if (isset($item[$pid], $map[$item[$pid]])) {
            $map[$item[$pid]][$son][] = &$map[$item[$id]];
        } else {
            $tree[] = &$map[$item[$id]];
        }
    }
    unset($map);
    return $tree;
}


/**
 * 转换  枚举到前端 select 组件所需的格式
 *
 * @param array $options 枚举的数组表示，举例 MenuPosition::toArray() 方法
 * @return array 转换后的数组，适合用于构建前端下拉选择框
 */
function getSelectOption(array $options): array
{
    return array_map(function ($option) {
        return [
            'label' => $option['description'],
            'value' => $option['value'],
        ];
    }, $options);
}

/**
 * @Util 获取当前的URL
 * @param $paramName
 * @param $paramValue
 * @return string
 */
function build_filter_url($paramName, $paramValue): string
{
    $url = request()->url();
    $queryParams = request()->query();
    $queryParams[$paramName] = $paramValue;
    $queryString = http_build_query($queryParams);
    return $url.'?'.$queryString;
}

/**
 * 移除URL中的某个参数
 * @param $paramName
 * @return string
 */
function build_remove_filter_url($paramName): string
{
    $url = request()->url();
    $queryParams = request()->query();
    unset($queryParams[$paramName]);
    $queryString = http_build_query($queryParams);
    return $queryString ? $url.'?'.$queryString : $url;
}

if (! function_exists('admin_user')) {
    function admin_user()
    {
        return BingoStart::user();
    }
}

if (! function_exists('blank')) {
    /**
     * Determine if the given value is "blank".
     *
     * @param mixed $value
     * @return bool
     */
    function blank($value): bool
    {
        if (is_null($value)) {
            return true;
        }

        if (is_string($value)) {
            return trim($value) === '';
        }

        if (is_numeric($value) || is_bool($value)) {
            return false;
        }

        if ($value instanceof Countable) {
            return count($value) === 0;
        }

        return empty($value);
    }
}

/**
 * 添加短代码
 */

if (! function_exists('add_shortcode')) {
    function add_shortcode(string $tag, callable $callback): void
    {
        app(ShortcodeService::class)->register($tag, $callback);
    }
}

/**
 * 执行解析短代码
 */
if (! function_exists('do_shortcode')) {
    function do_shortcode(string $content): string
    {
        return app(ShortcodeService::class)->parse($content);
    }
}


if (! function_exists('trace_log')) {
    /**
     * trace_log 将跟踪消息写入日志文件
     */
    function trace_log()
    {
        $messages = func_get_args();

        foreach ($messages as $message) {
            $level = 'info';

            if ($message instanceof Exception) {
                $level = 'error';
            } elseif (is_array($message) || is_object($message)) {
                $message = print_r($message, true);
            }

            Log::$level($message);
        }
    }
}

if (! function_exists('traceLog')) {
    /**
     * traceLog 是 trace_log() 的别名
     */
    function traceLog()
    {
        call_user_func_array('trace_log', func_get_args());
    }
}

if (! function_exists('trace_sql')) {
    /**
     * trace_sql 开始监控所有 SQL 输出
     * @return void
     */
    function trace_sql()
    {
        if (! defined('BINGO_NO_EVENT_LOGGING')) {
            define('BINGO_NO_EVENT_LOGGING', 1);
        }

        if (! defined('BINGO_TRACING_SQL')) {
            define('BINGO_TRACING_SQL', 1);
        } else {
            return;
        }

        Event::listen('illuminate.query', function ($query, $bindings, $time, $name) {
            $data = compact('bindings', 'time', 'name');

            foreach ($bindings as $i => $binding) {
                if ($binding instanceof \DateTime) {
                    $bindings[$i] = $binding->format('\'Y-m-d H:i:s\'');
                } elseif (is_string($binding)) {
                    $bindings[$i] = "'$binding'";
                }
            }

            $query = str_replace(['%', '?'], ['%%', '%s'], $query);
            $query = vsprintf($query, $bindings);

            Log::info($query);
        });
    }
}

if (! function_exists('traceSql')) {
    /**
     * traceSql 是 trace_sql() 的别名
     * @return void
     */
    function traceSql()
    {
        trace_sql();
    }
}

if (! function_exists('traceBack')) {
    /**
     * traceBack 是 trace_back() 的别名
     */
    function traceBack(int $distance = 25)
    {
        trace_back($distance);
    }
}

if (! function_exists('trace_back')) {
    /**
     * trace_back 从调用点记录一个简单的回溯
     * @return void
     */
    function trace_back(int $distance = 25)
    {
        trace_log(debug_backtrace(2, $distance));
    }
}

if (! function_exists('plugins_path')) {
    /**
     * plugins_path 获取插件文件夹的路径
     * @param string $path
     * @return string
     */
    function plugins_path(string $path = ''): string
    {
        return app('path.plugins').($path ? '/'.$path : $path);
    }
}

if (! function_exists('cache_path')) {
    /**
     * cache_path 获取缓存文件夹的路径
     * @param string $path
     * @return string
     */
    function cache_path(string $path = ''): string
    {
        return app('path.cache').($path ? '/'.$path : $path);
    }
}

if (! function_exists('themes_path')) {
    /**
     * themes_path 获取主题文件夹的路径
     * @param string $path
     * @return string
     */
    function themes_path(string $path = ''): string
    {
        return app('path.themes').($path ? '/'.$path : $path);
    }
}

if (! function_exists('temp_path')) {
    /**
     * temp_path 获取临时存储文件夹的路径
     * @param string $path
     * @return string
     */
    function temp_path(string $path = ''): string
    {
        return app('path.temp').($path ? '/'.$path : $path);
    }
}

if (! function_exists('e')) {
    /**
     * e 将字符串中的 HTML 特殊字符编码
     * @param string|Htmlable $value
     * @param bool $doubleEncode
     * @return string
     */
    function e(Htmlable|string $value, bool $doubleEncode = false): string
    {
        if ($value instanceof Htmlable) {
            return $value->toHtml();
        }

        return htmlspecialchars($value, ENT_QUOTES, 'UTF-8', $doubleEncode);
    }
}

if (! function_exists('trans')) {
    /**
     * trans 翻译给定的信息
     * @param string|null $id
     * @param array $parameters
     * @param string|null $locale
     * @return string
     */
    function trans(string $id = null, array $parameters = [], string $locale = null): string
    {
        return app('translator')->get($id, $parameters, $locale);
    }
}

if (! function_exists('array_build')) {
    /**
     * array_build 使用回调函数构建一个新数组
     * @param array $array
     * @param callable $callback
     * @return array
     */
    function array_build(array $array, callable $callback): array
    {
        return Arr::build($array, $callback);
    }
}

if (! function_exists('collect')) {
    /**
     * collect 从给定值创建一个集合
     * @param mixed|null $value
     * @return Collection
     */
    function collect(mixed $value = null): Collection
    {
        return new Collection($value);
    }
}

if (! function_exists('array_add')) {
    /**
     * array_add 使用 "dot" 符号将一个元素添加到数组中，如果不存在
     * @param array $array
     * @param string $key
     * @param mixed $value
     * @return array
     */
    function array_add(array $array, string $key, $value): array
    {
        return Arr::add($array, $key, $value);
    }
}

if (! function_exists('array_collapse')) {
    /**
     * array_collapse 将数组的数组折叠为单个数组
     * @param array $array
     * @return array
     */
    function array_collapse(array $array): array
    {
        return Arr::collapse($array);
    }
}

if (! function_exists('array_divide')) {
    /**
     * array_divide 将数组分成两个数组，一个是键，另一个是值
     * @param array $array
     * @return array
     */
    function array_divide(array $array): array
    {
        return Arr::divide($array);
    }
}

if (! function_exists('array_dot')) {
    /**
     * array_dot 用点符号展平多维关联数组
     * @param array $array
     * @param string $prepend
     * @return array
     */
    function array_dot(array $array, string $prepend = ''): array
    {
        return Arr::dot($array, $prepend);
    }
}

if (! function_exists('array_except')) {
    /**
     * array_except 获取给定数组中的所有元素，除了指定的键数组
     * @param array $array
     * @param array|string $keys
     * @return array
     */
    function array_except(array $array, $keys): array
    {
        return Arr::except($array, $keys);
    }
}

if (! function_exists('array_first')) {
    /**
     * array_first 返回通过给定真值测试的数组中的第一个元素
     * @param array $array
     * @param callable|null $callback
     * @param mixed $default
     * @return mixed
     */
    function array_first(array $array, callable $callback = null, $default = null)
    {
        return Arr::first($array, $callback, $default);
    }
}

if (! function_exists('array_flatten')) {
    /**
     * array_flatten 将多维数组展平成单个级别
     * @param array $array
     * @param int $depth
     * @return array
     */
    function array_flatten(array $array, int $depth = PHP_INT_MAX): array
    {
        return Arr::flatten($array, $depth);
    }
}

if (! function_exists('array_forget')) {
    /**
     * array_forget 使用 "dot" 符号从给定数组中删除一个或多个数组项
     * @param array $array
     * @param array|string $keys
     * @return void
     */
    function array_forget(array &$array, $keys): void
    {
        Arr::forget($array, $keys);
    }
}

if (! function_exists('array_get')) {
    /**
     * array_get 使用 "dot" 符号从数组中获取一个项
     * @param ArrayAccess|array $array
     * @param int|string $key
     * @param mixed|null $default
     * @return mixed
     */
    function array_get(ArrayAccess|array $array, int|string $key, mixed $default = null): mixed
    {
        return Arr::get($array, $key, $default);
    }
}

if (! function_exists('array_has')) {
    /**
     * array_has 使用 "dot" 符号检查一个或多个数组项是否存在于数组中
     * @param ArrayAccess|array $array
     * @param string|array $keys
     * @return bool
     */
    function array_has($array, $keys): bool
    {
        return Arr::has($array, $keys);
    }
}

if (! function_exists('array_last')) {
    /**
     * array_last 返回通过给定真值测试的数组中的最后一个元素
     * @param array $array
     * @param callable|null $callback
     * @param mixed $default
     * @return mixed
     */
    function array_last(array $array, callable $callback = null, $default = null)
    {
        return Arr::last($array, $callback, $default);
    }
}

if (! function_exists('array_only')) {
    /**
     * array_only 获取给定数组中指定键的子集
     * @param array $array
     * @param array|string $keys
     * @return array
     */
    function array_only(array $array, $keys): array
    {
        return Arr::only($array, $keys);
    }
}

if (! function_exists('array_pluck')) {
    /**
     * array_pluck 从数组中提取一个值数组
     * @param array $array
     * @param string|array $value
     * @param string|array|null $key
     * @return array
     */
    function array_pluck(array $array, $value, $key = null): array
    {
        return Arr::pluck($array, $value, $key);
    }
}

if (! function_exists('array_prepend')) {
    /**
     * array_prepend 将一个项目添加到数组的开头
     * @param array $array
     * @param mixed $value
     * @param mixed $key
     * @return array
     */
    function array_prepend(array $array, $value, $key = null): array
    {
        return Arr::prepend($array, $value, $key);
    }
}

if (! function_exists('array_pull')) {
    /**
     * array_pull 从数组中获取一个值，并将其移除
     * @param array $array
     * @param string $key
     * @param mixed $default
     * @return mixed
     */
    function array_pull(array &$array, string $key, $default = null)
    {
        return Arr::pull($array, $key, $default);
    }
}

if (! function_exists('array_random')) {
    /**
     * array_random 从数组中获取一个随机值
     * @param array $array
     * @param int|null $num
     * @return mixed
     */
    function array_random(array $array, int $num = null)
    {
        return Arr::random($array, $num);
    }
}

if (! function_exists('array_set')) {
    /**
     * array_set 使用 "dot" 符号将数组项设置为给定值
     * 如果没有给定键，则整个数组将被替换。
     * @param array $array
     * @param string $key
     * @param mixed $value
     * @return array
     */
    function array_set(array &$array, string $key, $value): array
    {
        return Arr::set($array, $key, $value);
    }
}

if (! function_exists('array_sort')) {
    /**
     * array_sort 根据给定的回调或属性名称对数组进行排序
     * @param array $array
     * @param callable|string|null $callback
     * @return array
     */
    function array_sort(array $array, $callback = null): array
    {
        return Arr::sort($array, $callback);
    }
}

if (! function_exists('array_sort_recursive')) {
    /**
     * array_sort_recursive 递归地对数组进行键和值排序
     * @param array $array
     * @return array
     */
    function array_sort_recursive(array $array): array
    {
        return Arr::sortRecursive($array);
    }
}

if (! function_exists('array_where')) {
    /**
     * array_where 使用给定的回调函数过滤数组
     * @param array $array
     * @param callable $callback
     * @return array
     */
    function array_where(array $array, callable $callback): array
    {
        return Arr::where($array, $callback);
    }
}

if (! function_exists('array_wrap')) {
    /**
     * array_wrap 如果给定的值不是数组，则将其包装在一个数组中
     * @param mixed $value
     * @return array
     */
    function array_wrap($value): array
    {
        return Arr::wrap($value);
    }
}

if (! function_exists('camel_case')) {
    /**
     * camel_case 将值转��为驼峰命名法
     * @param string $value
     * @return string
     */
    function camel_case(string $value): string
    {
        return Str::camel($value);
    }
}

if (! function_exists('ends_with')) {
    /**
     * ends_with 确定给定字符串是否以给定子字符串结尾
     * @param string $haystack
     * @param string|array $needles
     * @return bool
     */
    function ends_with(string $haystack, $needles): bool
    {
        return Str::endsWith($haystack, $needles);
    }
}

if (! function_exists('kebab_case')) {
    /**
     * kebab_case 将字符串转换为短横线命名法
     * @param string $value
     * @return string
     */
    function kebab_case(string $value): string
    {
        return Str::kebab($value);
    }
}

if (! function_exists('snake_case')) {
    /**
     * snake_case 将字符串转换为蛇形命名法
     * @param string $value
     * @param string $delimiter
     * @return string
     */
    function snake_case(string $value, string $delimiter = '_'): string
    {
        return Str::snake($value, $delimiter);
    }
}

if (! function_exists('starts_with')) {
    /**
     * starts_with 确定给定字符串是否以给定子字符串开头
     * @param string $haystack
     * @param string|array $needles
     * @return bool
     */
    function starts_with(string $haystack, $needles): bool
    {
        return Str::startsWith($haystack, $needles);
    }
}

if (! function_exists('str_after')) {
    /**
     * str_after 返回字符串在给定值之后的剩余部分
     * @param string $subject
     * @param string $search
     * @return string
     */
    function str_after(string $subject, string $search): string
    {
        return Str::after($subject, $search);
    }
}

if (! function_exists('str_before')) {
    /**
     * str_before 获取字符串在给定值之前的部分
     * @param string $subject
     * @param string $search
     * @return string
     */
    function str_before(string $subject, string $search): string
    {
        return Str::before($subject, $search);
    }
}

if (! function_exists('str_finish')) {
    /**
     * str_finish 用单个实例的给定值封闭字符串
     * @param string $value
     * @param string $cap
     * @return string
     */
    function str_finish(string $value, string $cap): string
    {
        return Str::finish($value, $cap);
    }
}

if (! function_exists('str_is')) {
    /**
     * str_is 确定给定字符串是否与给定模式匹配
     * @param string|array $pattern
     * @param string $value
     * @return bool
     */
    function str_is($pattern, string $value): bool
    {
        return Str::is($pattern, $value);
    }
}

if (! function_exists('str_limit')) {
    /**
     * str_limit 限制字符串中的字符数量
     * @param string $value
     * @param int $limit
     * @param string $end
     * @return string
     */
    function str_limit(string $value, int $limit = 100, string $end = '...'): string
    {
        return Str::limit($value, $limit, $end);
    }
}

if (! function_exists('str_plural')) {
    /**
     * str_plural 获取英文单词的复数形式
     * @param string $value
     * @param int $count
     * @return string
     */
    function str_plural(string $value, int $count = 2): string
    {
        return Str::plural($value, $count);
    }
}

if (! function_exists('str_random')) {
    /**
     * str_random 生成一个更真实的随机字母数字字符串
     * @param int $length
     * @return string
     *
     * @throws \RuntimeException
     */
    function str_random(int $length = 16): string
    {
        return Str::random($length);
    }
}

if (! function_exists('str_replace_array')) {
    /**
     * str_replace_array 使用数组顺序替换字符串中的给定��
     * @param string $search
     * @param array $replace
     * @param string $subject
     * @return string
     */
    function str_replace_array(string $search, array $replace, string $subject): string
    {
        return Str::replaceArray($search, $replace, $subject);
    }
}

if (! function_exists('str_replace_first')) {
    /**
     * str_replace_first 替换字符串中第一个出现的给定值
     * @param string $search
     * @param string $replace
     * @param string $subject
     * @return string
     */
    function str_replace_first(string $search, string $replace, string $subject): string
    {
        return Str::replaceFirst($search, $replace, $subject);
    }
}

if (! function_exists('str_replace_last')) {
    /**
     * str_replace_last 替换字符串中最后一个出现的给定值
     * @param string $search
     * @param string $replace
     * @param string $subject
     * @return string
     */
    function str_replace_last(string $search, string $replace, string $subject): string
    {
        return Str::replaceLast($search, $replace, $subject);
    }
}

if (! function_exists('str_singular')) {
    /**
     * str_singular 获取英文单词的单数形式
     * @param string $value
     * @return string
     */
    function str_singular(string $value): string
    {
        return Str::singular($value);
    }
}

if (! function_exists('str_slug')) {
    /**
     * str_slug 生成一个 URL 友好的 "slug" 字符串
     * @param string $title
     * @param string $separator
     * @param string $language
     * @return string
     */
    function str_slug(string $title, string $separator = '-', string $language = 'en'): string
    {
        return Str::slug($title, $separator, $language);
    }
}

if (! function_exists('str_start')) {
    /**
     * str_start 以给定值的单个实例开始字符串
     * @param string $value
     * @param string $prefix
     * @return string
     */
    function str_start(string $value, string $prefix): string
    {
        return Str::start($value, $prefix);
    }
}

if (! function_exists('studly_case')) {
    /**
     * studly_case 将值转换为驼峰命名法
     * @param string $value
     * @return string
     */
    function studly_case(string $value): string
    {
        return Str::studly($value);
    }
}

if (! function_exists('title_case')) {
    /**
     * title_case 将值转换为标题大小写
     * @param string $value
     * @return string
     */
    function title_case(string $value): string
    {
        return Str::title($value);
    }
}
