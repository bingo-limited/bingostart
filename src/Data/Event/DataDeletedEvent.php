<?php

namespace Bingo\Data\Event;

use Illuminate\Support\Facades\Event;
use Bingo\Core\Util\EventUtil;

class DataDeletedEvent
{
    public $data;

    public static function fire($data): void
    {
        $event = new static();
        $event->data = $data;
        EventUtil::fire($event);
    }

    public static function listen($callback): void
    {
        Event::listen(DataDeletedEvent::class, function (DataDeletedEvent $event) use ($callback) {
            call_user_func($callback, $event);
        });
    }
}
