<?php

namespace Bingo\Amis\Renderers;

/**
 * 控件
 * @method $this label($v)
 * @method $this description($v)
 * @method $this body($v)
 * @method $this size($v)
 * @method $this title($v)
 * @method $this data($v)
 * @method $this tabs($v)
 */
class Control extends BaseSchema
{
    public string $type = 'control';

}
