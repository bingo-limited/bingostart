<?php

namespace Bingo\Amis\Renderers;

/**
 * @method $this placeholder($v)
 * @method $this buttons($v)
 * @method $this label($v)
 */
class Operation extends BaseSchema
{
    public string $type = 'operation';
}
