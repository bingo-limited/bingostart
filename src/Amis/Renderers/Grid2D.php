<?php

namespace Bingo\Amis\Renderers;

/**
 * Grid2D类用于创建2D网格布局。
 *
 * @method $this type(string $v) 设置渲染器类型为grid-2d
 * @method $this gridClassName(string $v) 设置外层Dom的类名
 * @method $this gap(int|string $v) 设置格子间距，包括水平和垂直
 * @method $this cols(int $v) 设置格子水平划分为几个区域
 * @method $this rowHeight(int $v) 设置每个格子默认垂直高度
 * @method $this rowGap(int|string $v) 设置格子垂直间距
 * @method $this grids(array $v) 设置格子集合，每个元素都是一个配置了x, y, w, h等属性的SchemaNode对象
 *
 * 以下属性和方法适用于grids数组中的每个元素（SchemaNode对象）:
 * @method $this x(int $v) 设置格子起始位置的横坐标
 * @method $this y(int $v) 设置格子起始位置的纵坐标
 * @method $this w(int $v) 设置格子横跨几个宽度
 * @method $this h(int $v) 设置格子横跨几个高度
 * @method $this width(int|string $v) 设置格子所在列的宽度
 * @method $this height(int|string $v) 设置格子所在行的高度
 * @method $this align(string $v) 设置格子内容水平布局
 * @method $this valign(string $v) 设置格子内容垂直布局
 */
class Grid2D extends BaseSchema
{
    public string $type = 'grid-2d';
}
