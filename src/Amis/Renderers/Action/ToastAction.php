<?php

namespace Bingo\Amis\Renderers\Action;

use Bingo\Amis\Renderers\Button;

/**
 * @method $this toast($v)
 */
class ToastAction extends Button
{
    public string $actionType = 'toast';
}
