<?php

namespace Bingo\Amis\Renderers\Action;

use Bingo\Amis\Renderers\Button;

/**
 * @method $this blank($v)
 * @method $this url($v)
 */
class UrlAction extends Button
{
    public string $actionType = 'url';
}
