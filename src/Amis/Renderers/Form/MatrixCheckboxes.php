<?php

namespace Bingo\Amis\Renderers\Form;

/**
 * @method $this multiple($v)
 * @method $this singleSelectMode($v)
 * @method $this source($v)
 * @method $this columns($v)
 * @method $this rows($v)
 */
class MatrixCheckboxes extends FormBase
{
    public string $type = 'matrix-checkboxes';
}
