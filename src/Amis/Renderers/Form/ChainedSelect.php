<?php

namespace Bingo\Amis\Renderers\Form;

class ChainedSelect extends FormOptions
{
    public string $type = 'chained-select';
}
