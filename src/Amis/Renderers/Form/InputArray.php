<?php

namespace Bingo\Amis\Renderers\Form;

/**
 * @method $this items($v)
 */
class InputArray extends FormBase
{
    public string $type = 'input-array';
}
