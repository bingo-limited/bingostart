<?php

namespace Bingo\Amis\Renderers\Form;

/**
 * @method $this trueValue($v)
 * @method $this falseValue($v)
 * @method $this option($v)
 * @method $this onText($v)
 * @method $this offText($v)
 * @method $this width($v)
 */
class InputSwitch extends FormBase
{
    public string $type = 'switch';
}
