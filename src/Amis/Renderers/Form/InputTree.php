<?php

namespace Bingo\Amis\Renderers\Form;

/**
 * @method self hideRoot($v)
 * @method self rootLabel($v)
 * @method self rootValue($v)
 * @method self showIcon($v)
 * @method self autoCheckChildren($v)
 * @method self cascade($v)
 * @method self withChildren($v)
 * @method self onlyChildren($v)
 * @method self rootCreatable($v)
 * @method self enableNodePath($v)
 * @method self pathSeparator($v)
 * @method self showOutline($v)
 * @method self deferApi($v)
 * @method self heightAuto($v)
 * @method self inputOnly($v)
 * @method self autoComplete($v)  自动提示补全
 */
class InputTree extends FormOptions
{
    public string $type = 'input-tree';

}
