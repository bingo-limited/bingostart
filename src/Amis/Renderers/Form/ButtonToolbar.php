<?php

namespace Bingo\Amis\Renderers\Form;

use Bingo\Amis\Renderers\BaseSchema;

/**
 * @method $this buttons($v)
 */
class ButtonToolbar extends BaseSchema
{
    public string $type = 'button-toolbar';
}
