<?php

namespace Bingo\Amis\Renderers\Form;

/**
 * @method $this body($v)
 */
class InputGroup extends FormBase
{
    public string $type = 'input-group';
}
