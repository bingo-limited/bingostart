<?php

namespace Bingo\Amis\Renderers;

/**
 * 面包屑导航
 * @method $this itemClassName($v) 项的类名
 * @method $this separator($v) 导航条的分隔符
 * @method $this separatorClassName($v) 导航条的分隔符的类名
 * @method $this dropdownClassName($v) 下拉菜单的类名
 * @method $this dropdownItemClassName($v) 菜单项的类名
 * @method $this items($v) 面包屑导航的菜单项
 * @method $this labelMaxLength($v) 面包屑导航的菜单项的最大长度
 * @method $this tooltipPosition($v) 面包屑导航的菜单项的提示位置
 */
class Breadcrumb extends BaseSchema
{
    public string $type = 'breadcrumb';
}
