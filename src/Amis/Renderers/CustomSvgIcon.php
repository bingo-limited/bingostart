<?php

namespace Bingo\Amis\Renderers;

/**
 * @method $this icon($v)
 */
class CustomSvgIcon extends BaseSchema
{
    public string $type = 'custom-svg-icon';
}
