<?php

namespace Bingo\Amis\Renderers;

/**
 * BarCode 条形码渲染器
 * @method $this width($v) 宽度
 * @method $this height($v) 高度
 * @method $this options($v) 配置
 */
class BarCode extends BaseSchema
{
    public string $type = 'barcode';

}
