<?php

namespace Bingo\Amis\Renderers;

/**
 * @method $this rows($v)
 */
class VBox extends BaseSchema
{
    public string $type = 'vbox';
}
