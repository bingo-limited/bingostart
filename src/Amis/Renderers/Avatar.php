<?php

namespace Bingo\Amis\Renderers;

/**
 * Avatar 头像渲染器
 * @method $this className($v) 类名 (css类名，配置字符串，或者对象。)
 * @method $this style($v) 样式
 * @method $this badge($v) 角标
 * @method $this src($v) 图片地址
 * @method $this icon($v) 图标
 * @method $this fit($v) 图片填充方式
 * @method $this shape($v) 形状
 * @method $this size($v) 尺寸
 * @method $this text($v) 文本内容
 * @method $this gap($v) 间隔
 * @method $this alt($v) 图片无法显示时的替换文字地址
 * @method $this draggable($v) 是否可拖拽
 * @method $this crossOrigin($v) 跨域属性
 * @method $this onError($v) 图片加载失败的回调
 */
class Avatar extends BaseSchema
{
    public string $type = 'avatar';

    public function defaultAttr()
    {
        if (! $this->src) {
            $name = $this->name;
            $this->src('${'.$name.'}');
        }

    }

    public function getValue($value)
    {
        return admin_file_url($value);
    }
}
