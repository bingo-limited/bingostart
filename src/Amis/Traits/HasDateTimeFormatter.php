<?php

namespace Bingo\Amis\Traits;

use Carbon\Carbon;
use DateTimeInterface;

trait HasDateTimeFormatter
{
    protected function serializeDate(DateTimeInterface $date): string
    {
        return Carbon::parse($date)
            ->timezone(config('app.timezone'))
            ->format($this->getDateFormat());
    }
}
