<?php

namespace Bingo\Amis\Components\Grid;

use Bingo\Amis\Components\ExtraQueryParams;
use Bingo\Amis\Components\Form;
use Bingo\Amis\Components\Grid;
use Bingo\Amis\Renderers\Action\DialogAction;
use Bingo\Amis\Renderers\Service;

class DialogForm
{
    use ExtraQueryParams;

    protected Grid $grid;
    protected ?Form $form = null;

    protected DialogAction $createDialogAction;
    protected DialogAction $editDialogAction;

    protected mixed $size = null;

    public function __construct(Grid $grid)
    {
        $this->grid = $grid;
        $this->createDialogAction = DialogAction::make()->label(T('Add'))->level('primary')->icon('fa fa-add');
        $this->editDialogAction = DialogAction::make()->label(T('Edit'))->level('link')->icon('fa fa-edit icon-mr');
    }

    /**
     * 设置弹窗大小
     * @param mixed $size
     * @return DialogForm
     */
    public function size(mixed $size): DialogForm
    {
        $this->size = $size;
        return $this;
    }

    /**
     * 较小的弹框
     * @return void
     */
    public function sm(): void
    {
        $this->size("sm");
    }

    /**
     * 较大的弹框
     * @return void
     */
    public function lg(): void
    {
        $this->size("lg");
    }

    /**
     * 很大的弹框
     * @return void
     */
    public function xl(): void
    {
        $this->size("xl");
    }

    /**
     * 占满屏幕的弹框
     * @return void
     */
    public function full(): void
    {
        $this->size("full");
    }

    /**
     * 设置表单，添加操作时，不需要异步加载表单渲染配置
     * @param Form $form
     * @return DialogForm
     */
    public function form(Form $form): DialogForm
    {
        $this->form = $form;
        $this->form->dialog();
        return $this;
    }

    /**
     * 渲染弹窗
     * @param $api
     * @param bool $edit
     * @return DialogAction
     */
    public function render($api, bool $edit = false): DialogAction
    {

        if ($edit) {
            $this->editDialogAction->dialog([
                'title' => T('Edit'),
                'size' => $this->size,
                'body' => Service::make()->schemaApi($api),
            ]);
            return $this->editDialogAction;
        }
        $this->createDialogAction->dialog([
            'title' => T('Add'),
            'size' => $this->size,
            'body' => $this->form ?: Service::make()->schemaApi($api),
        ]);
        return $this->createDialogAction;
    }
}
