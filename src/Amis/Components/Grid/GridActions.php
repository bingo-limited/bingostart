<?php

namespace Bingo\Amis\Components\Grid;

use Bingo\Amis\Components\Grid;

trait GridActions
{
    protected Actions $actions;

    //禁用操作
    protected bool $disableAction = false;

    /**
     * 禁用所有操作
     * @return GridActions|Grid
     */
    public function disableAction(): self
    {
        $this->disableAction = true;
        return $this;
    }

    /**
     * 禁用删除
     * @return GridActions|Grid
     */
    public function disableDelete(): self
    {
        $this->actions->disableDelete();
        return $this;
    }

    /**
     * 禁用编辑
     * @return GridActions|Grid
     */
    public function disableEdit(): self
    {
        $this->actions->disableEdit();
        return $this;
    }

    /**
     * 禁用批量删除
     * @param bool $bool
     * @return GridActions|Grid
     */
    public function disableBulkDelete(bool $bool = true): self
    {
        $this->toolbar->disableBulkDelete($bool);
        return $this;
    }

    /**
     * 行操作
     * @param $fun
     * @return GridActions|Grid
     */
    public function actions($fun): self
    {
        $fun($this->actions);
        return $this;
    }

    public function renderAction(): array
    {
        $extraParams = $this->getExtraQueryParams();
        return $this->actions->render($extraParams);
    }
}
