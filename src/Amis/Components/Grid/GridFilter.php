<?php

namespace Bingo\Amis\Components\Grid;

use Bingo\Amis\Components\Grid;

trait GridFilter
{
    private Filter $filter;

    /**
     * 查询过滤器
     * @param $fun
     * @return Grid
     */
    public function filter($fun): Grid
    {
        $this->crud->filterTogglable(true);
        $fun($this->filter);
        return $this;
    }

    public function getFilterField(): array
    {
        return $this->filter->getFilterField();
    }


    private function buildFilter(): void
    {
        $this->filter->body($this->filter->renderBody());
        $this->filter->data($this->filter->getDefaultValue());
    }

    private function renderFilter()
    {
        $this->buildFilter();

        return $this->filter;
    }
}
