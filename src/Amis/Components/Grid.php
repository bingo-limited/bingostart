<?php

namespace Bingo\Amis\Components;

use Illuminate\Database\Eloquent\Builder;
use JsonSerializable;
use Bingo\Amis\Components\Grid\Actions;
use Bingo\Amis\Components\Grid\Filter;
use Bingo\Amis\Components\Grid\GridCRUD;
use Bingo\Amis\Components\Grid\GridData;
use Bingo\Amis\Components\Grid\GridDialogForm;
use Bingo\Amis\Components\Grid\GridToolbar;
use Bingo\Amis\Components\Grid\GridTree;
use Bingo\Amis\Components\Grid\Model;
use Bingo\Amis\Components\Grid\Toolbar;
use Bingo\Amis\Renderers\CRUD;
use Bingo\Amis\Renderers\Page;

class Grid implements JsonSerializable
{
    use GridCRUD, GridData, GridToolbar, ModelBase, GridTree, GridDialogForm, ExtraQueryParams;


    protected Page $page;

    protected string $routeName;
    protected Model $model;

    protected string $_action;

    public function __construct()
    {
        $this->page = Page::make()->title(T('List'));
        $this->crud = CRUD::make();
        $this->filter = new Filter();
        $this->actions = new Actions($this);
        $this->toolbar = new Toolbar($this);

        $this->crud->columnsTogglable(false);

        $this->crud->affixHeader(false);

        $this->_action = (string) request('_action');
    }

    public static function make(Builder $model, string $routeName, $fun, array $extraParams = []): Grid
    {
        $grid = new static();
        $grid->model = new Model($model, $grid);
        $grid->routeName = $routeName;
        $grid->setExtraQueryParams($extraParams); // 设置额外的查询参数
        $fun($grid);
        return $grid;
    }

    /**
     * 获取AmisPage实例
     * @return Page
     */
    public function usePage(): Page
    {
        return $this->page;
    }

    public function builder(): Builder
    {
        return $this->model->getBuilder();
    }


    // 获取API URL，并附加上额外的查询参数
    public function getApiUrlWithExtraParams(): string
    {
        $isMultiLanguageEnabled = env('MULTI_LANGUAGE_ENABLED', false);
        $param = ['_action' => 'getData'];
        if ($isMultiLanguageEnabled) {
            $param['locale'] = T_locale();
        }

        // 直接将额外的查询参数并入到$param中
        foreach ($this->extraQueryParams as $key => $value) {
            $param[$key] = $value;
        }

        return $this->getIndexUrl($param);
    }

    public function jsonSerialize()
    {
        //获取数据
        if ($this->_action === "getData") {
            return $this->buildData();
        }
        $this->page
            ->toolbar($this->toolbar->renderToolbar($this->extraQueryParams))
            ->body([
                $this->renderHeader(),
                $this->renderCRUD(),
                $this->renderFooter(),
            ]);

        return $this->page;
    }
}
