<?php

namespace Bingo\Amis\Components;

use Illuminate\Support\Collection;
use JsonSerializable;

class HeaderToolbar implements JsonSerializable
{
    protected Collection $left;
    protected Collection $right;

    public function __construct()
    {
        $this->left = collect([]);
        $this->right = collect([]);
    }

    public function left($element): HeaderToolbar
    {
        $this->left->add($element);
        return $this;
    }

    public function right($element): HeaderToolbar
    {
        $this->right->add($element);
        return $this;
    }



    public function jsonSerialize(): array
    {


        return [
            'left' => $this->left->toArray(),
            'right' => $this->right->toArray(),
        ];
    }
}
