<?php

namespace Bingo\Amis\Components;

trait ExtraQueryParams
{
    protected array $extraQueryParams = [];

    public function setExtraQueryParams(array $extraQueryParams): self
    {
        $this->extraQueryParams = $extraQueryParams;
        return $this;
    }

    public function getExtraQueryParams(): array
    {
        return $this->extraQueryParams;
    }
}
