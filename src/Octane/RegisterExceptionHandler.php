<?php

namespace Bingo\Octane;

use Bingo\Exceptions\Handler;
use Illuminate\Contracts\Debug\ExceptionHandler;

class RegisterExceptionHandler
{
    /**
     * Handle the event.
     *
     * @param  mixed  $event
     * @return void
     */
    public function handle(mixed $event): void
    {
        if (isRequestFromDashboard()) {
            $event->sandbox->singleton(ExceptionHandler::class, Handler::class);
        }
    }
}
