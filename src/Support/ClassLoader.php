<?php

namespace Bingo\Support;

use Bingo\Core\Filesystem\Filesystem;
use Exception;
use Throwable;

/**
 * ClassLoader 是 Bingo CMS 使用的一個自定義自動加載器，它使用文件夾名稱為小寫，文件名稱根據類名大寫。
 *
 */
class ClassLoader
{
    /**
     * @var Filesystem 文件實例
     */
    public Filesystem $files;

    /**
     * @var string 基礎路徑
     */
    public string $basePath;

    /**
     * @var string|null 清單路徑
     */
    public ?string $manifestPath;

    /**
     * @var array 已加載項目的清單
     */
    public array $manifest = [];

    /**
     * @var array 未知類的緩存
     */
    protected array $unknownClasses = [];

    /**
     * @var bool 清單是否需要寫入
     */
    protected bool $manifestDirty = false;

    /**
     * @var array 已註冊的命名空間
     */
    protected array $namespaces = [];

    /**
     * @var array 已註冊的目錄
     */
    protected array $directories = [];

    /**
     * @var bool 指示此類是否已註冊
     */
    protected bool $registered = false;

    /**
     * __construct 創建一個新的包清單實例
     */
    public function __construct(Filesystem $files, string $basePath)
    {
        $this->files = $files;
        $this->basePath = $basePath;
    }

    /**
     * 加載給定的類文件
     * @param string $class
     */
    public function load($class): bool
    {
        if (! str_contains($class, '\\')) {
            return false;
        }

        if (
            isset($this->manifest[$class]) &&
            is_file($fullPath = $this->basePath.DIRECTORY_SEPARATOR.$this->manifest[$class])
        ) {
            require $fullPath;
            return true;
        }

        if (isset($this->unknownClasses[$class])) {
            return false;
        }

        [$lowerClass, $upperClass] = $this->normalizeClass($class);

        // 加載命名空間
        foreach ($this->namespaces as $namespace => $directory) {
            if (str_starts_with($class, $namespace)) {
                if ($this->loadUpperOrLower($class, $directory, $upperClass, $lowerClass) === true) {
                    return true;
                }
            }
        }

        // 加載目錄
        foreach ($this->directories as $directory) {
            if ($this->loadUpperOrLower($class, $directory, $upperClass, $lowerClass) === true) {
                return true;
            }
        }

        $this->unknownClasses[$class] = true;

        return false;
    }

    /**
     * loadUpperOrLower 使用提供的大小寫類路徑加載目錄中的類。
     */
    protected function loadUpperOrLower(string $class, string $directory, string $upperClass, string $lowerClass): bool
    {
        if ($directory) {
            $directory .= DIRECTORY_SEPARATOR;
        }

        if ($this->isRealFilePath($path = $directory.$lowerClass)) {
            $this->includeClass($class, $path);
            return true;
        }

        if ($this->isRealFilePath($path = $directory.$upperClass)) {
            $this->includeClass($class, $path);
            return true;
        }

        return false;
    }

    /**
     * isRealFilePath 判斷相對路徑的文件是否存在且是真實的
     */
    protected function isRealFilePath(string $path): bool
    {
        return is_file(realpath($this->basePath.DIRECTORY_SEPARATOR.$path));
    }

    /**
     * includeClass 並添加到清單中
     */
    protected function includeClass(string $class, string $path): void
    {
        require $this->basePath.DIRECTORY_SEPARATOR.$path;

        $this->manifest[$class] = $this->files->normalizePath($path);

        $this->manifestDirty = true;
    }

    /**
     * 註冊給定的類加載器到自動加載堆棧
     */
    public function register(): void
    {
        if ($this->registered) {
            return;
        }

        $this->registered = spl_autoload_register(function ($class) {
            $this->load($class);
        });
    }

    /**
     * 構建清單並將其寫入磁盤
     */
    public function build(): void
    {
        if (! $this->manifestDirty) {
            return;
        }

        $this->write($this->manifest);
    }

    /**
     * initManifest 註冊後啟動清單緩存文件。
     */
    public function initManifest(string $manifestPath): void
    {
        $this->manifestPath = $manifestPath;

        $this->ensureManifestIsLoaded();
    }

    /**
     * 添加命名空間
     */
    public function addNamespace($namespace, $directory): void
    {
        $this->namespaces[$namespace] = $directory;
    }

    /**
     * 添加目錄到類加載器
     * @param array|string $directories
     */
    public function addDirectories(array|string $directories): void
    {
        $this->directories = array_merge($this->directories, (array) $directories);

        $this->directories = array_unique($this->directories);
    }

    /**
     * 從類加載器中刪除目錄
     * @param array|string|null $directories
     */
    public function removeDirectories(array|string $directories = null): void
    {
        if (is_null($directories)) {
            $this->directories = [];
        } else {
            $directories = (array) $directories;

            $this->directories = array_filter($this->directories, function ($directory) use ($directories) {
                return ! in_array($directory, $directories);
            });
        }
    }

    /**
     * 獲取已註冊的目錄
     */
    public function getDirectories(): array
    {
        return $this->directories;
    }

    /**
     * normalizeClass 獲取類的正常文件名稱
     */
    protected function normalizeClass(string $class): array
    {
        // 去掉第一個斜線
        if ($class[0] === '\\') {
            $class = substr($class, 1);
        }

        // 小寫文件夾
        $parts = explode('\\', $class);
        $file = array_pop($parts);
        $namespace = implode('\\', $parts);
        $directory = str_replace(['\\', '_'], DIRECTORY_SEPARATOR, $namespace);

        // 提供兩種替代方案
        $lowerClass = strtolower($directory).DIRECTORY_SEPARATOR.$file.'.php';
        $upperClass = $directory.DIRECTORY_SEPARATOR.$file.'.php';

        return [$lowerClass, $upperClass];
    }

    /**
     * ensureManifestIsLoaded 已加載到內存中
     */
    protected function ensureManifestIsLoaded(): void
    {
        $manifest = [];

        if (file_exists($this->manifestPath)) {
            try {
                $manifest = $this->files->getRequire($this->manifestPath);

                if (! is_array($manifest)) {
                    $manifest = [];
                }
            } catch (Throwable $ex) {
            }
        }

        $this->manifest += $manifest;
    }

    /**
     * 將給定的清單數組寫入磁盤
     * @throws Exception
     */
    protected function write(array $manifest): void
    {
        if ($this->manifestPath === null) {
            return;
        }

        if (! is_writable(dirname($this->manifestPath))) {
            throw new Exception('目錄 '.$this->manifestPath.' 必須存在且可寫。');
        }

        $this->files->put(
            $this->manifestPath,
            '<?php return '.var_export($manifest, true).';'
        );
    }
}
