<?php

namespace Bingo\Support\Captcha;

use Illuminate\Support\Facades\Facade;

/**
 * Class CaptchaFacade
 * @package Bingo\Misc\Captcha
 * @mixin Captcha
 */
class CaptchaFacade extends Facade
{
    /**
     * @return string
     */
    protected static function getFacadeAccessor()
    {
        return 'captcha';
    }

}
