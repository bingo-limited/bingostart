<?php

namespace Bingo\Support\Concern;

use Closure;

trait HasBuilder
{
    /**
     * @var Closure
     */
    private Closure $builder;

    /**
     * @param $builder
     * @return $this
     */
    public function builder($builder)
    {
        $this->builder = $builder;
        return $this;
    }

    private function runBuilder()
    {
        if ($this->builder) {
            call_user_func($this->builder, $this);
        }
    }
}
