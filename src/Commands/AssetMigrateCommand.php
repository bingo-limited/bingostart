<?php

namespace Bingo\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\File;

/**
 * 资产迁移命令
 *
 * 此命令用于将模块的资产文件从模块目录迁移到公共目录。
 * 可以选择单个模块或遍历所有模块进行迁移。
 *
 * 使用方法：
 * 1. 迁移单个模块的资产：
 *    php artisan bingo:asset:migrate {module}
 *    例如：php artisan bingo:asset:migrate Cms
 *
 * 2. 迁移所有模块的资产：
 *    php artisan bingo:asset:migrate
 */
class AssetMigrateCommand extends Command
{
    /**
     * 命令的签名（命令名及参数）
     *
     * @var string
     */
    protected $signature = 'bingo:asset:migrate {module?}';

    /**
     * 命令的描述
     *
     * @var string
     */
    protected $description = '迁移模块资产文件到公共目录';

    /**
     * 执行命令的处理逻辑
     *
     * @return void
     */
    public function handle(): void
    {
        $module = $this->argument('module');

        if ($module) {
            $this->migrateModuleAssets($module);
        } else {
            $this->migrateAllModulesAssets();
        }
    }

    /**
     * 迁移指定模块的资产文件
     *
     * @param string $module 模块名称
     * @return void
     */
    private function migrateModuleAssets(string $module): void
    {
        $sourcePath = base_path("Modules/{$module}/Asset");
        $destinationPath = public_path("Vendor/{$module}/Asset");

        if (! File::exists($sourcePath)) {
            $this->error("模块 {$module} 不存在或没有 Asset 目录。");
            return;
        }

        File::ensureDirectoryExists($destinationPath);
        File::copyDirectory($sourcePath, $destinationPath);

        $this->info("模块 {$module} 的资产文件已迁移到 {$destinationPath}");
    }

    /**
     * 遍历所有模块并迁移它们的资产文件
     *
     * @return void
     */
    private function migrateAllModulesAssets(): void
    {
        $modulesPath = base_path('Modules');
        $modules = File::directories($modulesPath);

        foreach ($modules as $modulePath) {
            $module = basename($modulePath);
            $this->migrateModuleAssets($module);
        }
    }
}
