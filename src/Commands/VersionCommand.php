<?php

namespace Bingo\Commands;

use Bingo\BingoStart;
use Illuminate\Console\Command;

class VersionCommand extends Command
{
    protected $signature = 'bingo:version';

    protected $description = 'show the version of BingoStart';

    public function handle(): void
    {
        $this->info(BingoStart::VERSION);
    }
}
