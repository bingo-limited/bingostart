<?php

declare(strict_types=1);

namespace Bingo\Commands\Create;

use Bingo\BingoStart;
use Bingo\Commands\BingoCommand;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Str;

class Model extends BingoCommand
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'bingo:make:model {module} {--model= : the model name } {--t= : the model of table names separated by commas} {--prefix=bingo_ : the table prefix to be removed}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create bingo models from tables';

    /**
     * 表前缀，可通过命令行参数传递
     *
     * @var string
     */
    protected string $prefix = 'bingo_';

    /**
     * 创建新命令实例
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * 执行控制台命令
     *
     * @return void
     */
    public function handle(): void
    {
        // 从命令行获取前缀参数，或使用默认前缀
        $this->prefix = $this->option('prefix') ?? $this->prefix;

        // 从命令行获取表名参数，并以逗号分隔为数组
        $tableNames = explode(',', $this->option('t') ?? '');

        // 遍历每个表名，创建对应的模型
        foreach ($tableNames as $tableName) {
            // 去除前缀后的表名
            $cleanTableName = Str::of($tableName)->replaceFirst($this->prefix, '')->toString();

            // 使用原始表名检查数据库中是否存在该表
            if (! Schema::hasTable($cleanTableName)) {
                $this->error("Schema [$cleanTableName] not found");
                continue;
            }

            $modelPath = BingoStart::getModuleModelPath($this->argument('module'));
            $modelFile = $modelPath.$this->getModelFile($cleanTableName);

            // 如果文件已存在，询问是否替换
            if (File::exists($modelFile)) {
                $answer = $this->ask("$modelFile 已经存在，是否替换？", 'Y');

                if (! Str::of($answer)->lower()->exactly('y')) {
                    continue;
                }
            }

            // 生成模型内容并写入文件
            File::put($modelFile, $this->getModelContent($cleanTableName, $cleanTableName));

            // 检查文件是否已成功创建
            if (File::exists($modelFile)) {
                $this->info("$modelFile 已创建");
            } else {
                $this->error("$modelFile 创建失败");
            }
        }
    }


    /**
     * 获取模型文件名
     *
     * @param string $tableName
     * @return string
     */
    protected function getModelFile(string $tableName): string
    {
        return $this->getModelName($tableName).'.php';
    }

    /**
     * 获取模型名称，使用大写字母和驼峰命名法
     *
     * @param string $tableName
     * @return string
     */
    protected function getModelName(string $tableName): string
    {
        if ($this->hasOption('model') && $this->option('model') !== null) {
            return Str::of($this->option('model'))->ucfirst()->toString();
        }

        return Str::of($tableName)
            ->camel()
            ->ucfirst()
            ->toString();
    }

    /**
     * 获取 stub 内容
     *
     * @return string
     */
    protected function getStubContent(): string
    {
        return File::get(dirname(__DIR__).DIRECTORY_SEPARATOR.'stubs'.DIRECTORY_SEPARATOR.'model.stub');
    }

    /**
     * 获取模型内容
     *
     * @param string $cleanTableName
     * @param string $tableName
     * @return string
     */
    protected function getModelContent(string $cleanTableName, string $tableName): string
    {
        return Str::of($this->getStubContent())
            ->replace(
                ['{namespace}', '{model}', '{table}', '{fillable}'],
                [$this->getModelNamespace(), $this->getModelName($cleanTableName), $tableName, $this->getFillable($tableName)]
            )
            ->toString();
    }

    /**
     * 获取模型命名空间
     *
     * @return string
     */
    protected function getModelNamespace(): string
    {
        return trim(BingoStart::getModuleModelNamespace($this->argument('module')), '\\');
    }

    /**
     * 获取填充字段列表
     *
     * @param string $tableName
     * @return string
     */
    protected function getFillable(string $tableName): string
    {
        $fillable = Str::of('');

        foreach (getTableColumns($tableName) as $column) {
            $fillable = $fillable->append("'{$column}', ");
        }

        return $fillable->trim(',')->toString();
    }
}
