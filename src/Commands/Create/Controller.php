<?php

declare(strict_types=1);

namespace Bingo\Commands\Create;

use Bingo\BingoStart;
use Bingo\Commands\BingoCommand;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Str;

;

class Controller extends BingoCommand
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'bingo:make:controller {module} {name}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'create bingo controller';


    public function handle(): void
    {
        $controllerPath = BingoStart::getModuleControllerPath($this->argument('module'));

        $file = $controllerPath.$this->getControllerFile();

        if (File::exists($file)) {
            $answer = $this->ask($file.' already exists, Did you want replace it?', 'Y');

            if (! Str::of($answer)->lower()->exactly('y')) {
                exit;
            }
        }

        File::put($file, Str::of($this->getStubContent())->replace([
            '{namespace}', '{controller}'
        ], [trim(BingoStart::getModuleControllerNamespace($this->argument('module')), '\\'), $this->getControllerName()])->toString());

        if (File::exists($file)) {
            $this->info($file.' has been created');
        } else {
            $this->error($file.' create failed');
        }
    }

    /**
     *
     *
     * @return string
     */
    protected function getControllerFile(): string
    {
        return $this->getControllerName().'.php';
    }

    /**
     *
     *
     * @return string
     */
    protected function getControllerName(): string
    {
        return  Str::of($this->argument('name'))
                    ->whenContains('Controller', function ($str) {
                        return $str;
                    }, function ($str) {
                        return $str->append('Controller');
                    })->ucfirst()->toString();
    }

    /**
     * get stub content
     *
     * @return string
     */
    protected function getStubContent(): string
    {
        return File::get(dirname(__DIR__).DIRECTORY_SEPARATOR.'stubs'.DIRECTORY_SEPARATOR.'controller.stub');
    }
}
