<?php

namespace Bingo\Commands;

use Bingo\Core\Events\ModuleUninstalledEvent;
use Bingo\Enums\Code;
use Bingo\Exceptions\BizException;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Event;
use Bingo\Core\Util\FileUtil;
use Bingo\Core\Util\VersionUtil;
use Bingo\Module\ModuleManager;

class ModuleUninstallCommand extends Command
{
    protected $signature = 'bingo:module:uninstall {module}';

    public function handle(): void
    {
        $module = $this->argument('module');
        $isExists = ModuleManager::isExists($module);
        $installs = ModuleManager::listAllInstalledModules();
        if ($isExists) {
            BizException::throwsIf(! isset($installs[$module]), Code::FAILED, T('Module not installed'));
            foreach ($installs as $one => $_) {
                $basic = ModuleManager::getModuleBasic($one);
                if (empty($basic)) {
                    break;
                }
                if (! empty($basic['require'])) {
                    foreach ($basic['require'] as $require) {
                        list($m, $v) = VersionUtil::parse($require);
                        BizException::throwsIf($module == $m, Code::FAILED, T('Module %s depend on %s, uninstall fail', $one, $module));
                    }
                }
            }
            ModuleManager::callHook($module, 'hookBeforeUninstall');
        }
        unset($installs[$module]);
        $this->unPublishAsset($module);
        $this->unPublishRoot($module);

        ModuleManager::saveUserInstalledModules($installs);

        $event = new ModuleUninstalledEvent();
        $event->name = $module;
        Event::dispatch($event);
    }

    private function unPublishAsset($module): void
    {
        $fs = $this->laravel['files'];
        $from = ModuleManager::path($module, 'Asset').'/';
        if (! file_exists($from)) {
            return;
        }
        $to = public_path("vendor/$module/");
        if (! file_exists($to)) {
            return;
        }
        $fs->deleteDirectory($to);
        $this->info("Module Asset UnPublish : $to");
    }

    private function unPublishRoot($module): void
    {
        $root = ModuleManager::path($module, 'ROOT');
        if (! file_exists($root)) {
            return;
        }
        $files = FileUtil::listAllFiles($root);
        $files = array_filter($files, function ($file) {
            return $file['isFile'];
        });
        $publishFiles = 0;
        foreach ($files as $file) {
            $relativePath = $file['filename'];
            $relativePathBackup = $relativePath.'._delete_.'.$module;
            $currentFile = base_path($relativePath);
            $currentFileBackup = $currentFile.'._delete_.'.$module;
            if (! file_exists($currentFile)) {
                continue;
            }
            if (
                (! file_exists($currentFileBackup) && md5_file($currentFile) == md5_file($file['pathname']))
                ||
                (file_exists($currentFileBackup))
            ) {
                unlink($currentFile);
                if (file_exists($currentFileBackup)) {
                    $content = trim(file_get_contents($currentFileBackup));
                    if ('__Bingo_EMPTY_FILE__' == $content) {
                        unlink($currentFileBackup);
                        $this->info("Module Root Publish : $relativePath");
                    } else {
                        rename($currentFileBackup, $currentFile);
                        $this->info("Module Root Publish : $relativePath <- $relativePathBackup");
                    }
                }
                $publishFiles++;
            }
        }
        $this->info("Module Root UnPublish : $publishFiles item(s)");
    }

}
