<?php

namespace Bingo\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Artisan;
use Bingo\Core\Util\FileUtil;

class ModuleRefreshAllCommand extends Command
{
    protected $signature = 'bingo:module:refresh-all';

    public function handle(): void
    {
        $this->info("ModuleRefreshAll Start");

        $this->info(">>> CleanOldAsset");
        FileUtil::rm(public_path('asset'));
        $this->info(">>> Finished\n");

        $this->info(">>> Publish Asset");
        $exitCode = $this->call('vendor:publish', [
            '--provider' => 'Bingo\BingoServiceProvider',
        ]);
        $output = trim(Artisan::output());
        $this->info("ExitCode -> ".$exitCode);
        $this->info($output);
        $this->info(">>> Finished\n");

        $this->info(">>> Reinstall Modules");
        $exitCode = $this->call('Bingo:module:install-all');
        $output = trim(Artisan::output());
        $this->info("ExitCode -> ".$exitCode);
        $this->info($output);
        $this->info(">>> Finished\n");

        $this->warn("ModuleRefreshAll Finished");
    }

}
