<?php

namespace Bingo\Commands;

use Bingo\Core\Events\ModuleDisabledEvent;
use Bingo\Enums\Code;
use Bingo\Exceptions\BizException;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Event;
use Bingo\Module\ModuleManager;

class ModuleDisableCommand extends Command
{
    protected $signature = 'bingo:module:disable {module}';

    public function handle(): void
    {
        $module = $this->argument('module');
        BizException::throwsIf(! ModuleManager::isExists($module), Code::FAILED, T('Module Invalid'));
        $installs = ModuleManager::listAllInstalledModules();
        $basic = ModuleManager::getModuleBasic($module);
        BizException::throwsIf(! $basic, Code::FAILED, 'Module basic empty');

        ModuleManager::callHook($module, 'hookBeforeDisable');

        $installs[$module]['enable'] = false;
        ModuleManager::saveUserInstalledModules($installs);
        //        Bingo::clearCache();
        $event = new ModuleDisabledEvent();
        $event->name = $module;
        Event::dispatch($event);
        $this->info('Module Disable Success');
    }

}
