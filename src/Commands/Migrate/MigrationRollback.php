<?php

namespace Bingo\Commands\Migrate;

use Bingo\BingoStart;
use Bingo\Commands\BingoCommand;
use Illuminate\Support\Facades\File;

class MigrationRollback extends BingoCommand
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'bingo:migrate:rollback {module} {--force}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'rollback module tables';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return void
     */
    public function handle(): void
    {
        $module = $this->argument('module');

        if (! File::isDirectory(BingoStart::getModuleMigrationPath($module))) {
            $this->call('migration:rollback', [
                '--path' => BingoStart::getModuleRelativePath(BingoStart::getModuleMigrationPath($module)),

                '--force' => $this->option('force')
            ]);
        } else {
            $this->error('No migration files in module');
        }
    }
}
