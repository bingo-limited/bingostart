<?php

namespace Bingo\Commands\Migrate;

use Bingo\BingoStart;
use Bingo\Commands\BingoCommand;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Str;

class MigrateRun extends BingoCommand
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'bingo:migrate {module} {--force}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'migrate bingo module';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return void
     */
    public function handle(): void
    {

        $module = $this->argument('module');

        if (File::isDirectory(BingoStart::getModuleMigrationPath($module))) {

            foreach (File::files(BingoStart::getModuleMigrationPath($module)) as $file) {

                $path = Str::of(BingoStart::getModuleRelativePath(BingoStart::getModuleMigrationPath($module)))

                    ->remove('.')->append($file->getFilename());

                $this->call('migrate', [
                    '--path' => $path,

                    '--force' => $this->option('force')
                ]);
                $output = trim(Artisan::output());
                $this->info($output);
            }
            $this->info("Module [$module] migrate success");
        } else {
            $this->error('No migration files in module');
        }
    }
}
