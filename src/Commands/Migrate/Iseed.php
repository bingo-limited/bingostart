<?php

namespace Bingo\Commands\Migrate;

use Illuminate\Filesystem\Filesystem;
use Illuminate\Support\Collection;
use Illuminate\Support\Composer;
use Illuminate\Support\Facades\File;
use RuntimeException;

class Iseed
{
    /**
     * Name of the database upon which the seed will be executed.
     *
     * @var string
     */
    protected string $databaseName;

    /**
     * New line character for seed files.
     * Double quotes are mandatory!
     *
     * @var string
     */
    private string $newLineCharacter = PHP_EOL;

    /**
     * Desired indent for the code.
     * For tabulator use \t
     * Double quotes are mandatory!
     *
     * @var string
     */
    private string $indentCharacter = "    ";

    /**
     * @var Composer
     */
    private Composer $composer;

    public function __construct(Filesystem $filesystem = null, Composer $composer = null)
    {
        $this->files = $filesystem ?: new Filesystem();
        $this->composer = $composer ?: new Composer($this->files);
    }

    public function readStubFile($file): string
    {
        $buffer = file($file, FILE_IGNORE_NEW_LINES);
        return implode(PHP_EOL, $buffer);
    }

    /**
     * Generates a seed file.
     * @param string $table
     * @param null $file
     * @param null $database
     * @param int $max
     * @param int $chunkSize
     * @param null $exclude
     * @param null $preRunEvent
     * @param null $postRunEvent
     * @param bool $dumpAuto
     * @param bool $indexed
     * @param null $orderBy
     * @param string $direction
     * @return bool
     */
    public function generateSeed(string $table, $file = null, $database = null, int $max = 0, int $chunkSize = 0, $exclude = null, $preRunEvent = null, $postRunEvent = null, bool $dumpAuto = true, bool $indexed = true, $orderBy = null, string $direction = 'ASC'): bool
    {
        if (! $database) {
            $database = config('database.default');
        }

        $this->databaseName = $database;

        // Check if table exists
        if (! $this->hasTable($table)) {
            throw new RuntimeException("Table $table was not found.");
        }

        // Get the data
        $data = $this->getData($table, $max, $exclude, $orderBy, $direction);

        // Repack the data
        $dataArray = $this->repackSeedData($data);

        // Get template for a seed file contents
        $stub = $this->readStubFile(dirname(__DIR__).DIRECTORY_SEPARATOR.'stubs'.DIRECTORY_SEPARATOR.'iseeder.stub');


        // Get a populated stub file
        $seedContent = $this->populateStub(
            $stub,
            $table,
            (string) $dataArray,
            $chunkSize,
            $preRunEvent,
            $postRunEvent,
            $indexed
        );

        // Save a populated stub
        $this->files->put($file, $seedContent);

        // Run composer dump-auto
        if ($dumpAuto) {
            $this->composer->dumpAutoloads();
        }
    }


    /**
     * Get the Data
     * @param string $table
     * @param $max
     * @param null $exclude
     * @param null $orderBy
     * @param string $direction
     * @return Collection
     */
    public function getData(string $table, $max, $exclude = null, $orderBy = null, string $direction = 'ASC'): Collection
    {
        $result = \DB::connection($this->databaseName)->table($table);

        if (! empty($exclude)) {
            $allColumns = \DB::connection($this->databaseName)->getSchemaBuilder()->getColumnListing($table);
            $result = $result->select(array_diff($allColumns, $exclude));
        }

        if ($orderBy) {
            $result = $result->orderBy($orderBy, $direction);
        }

        if ($max) {
            $result = $result->limit($max);
        }

        return $result->get();
    }

    /**
     * Repacks data read from the database
     * @param object|array $data
     * @return array
     */
    public function repackSeedData(object|array $data): array
    {
        if (! is_array($data)) {
            $data = $data->toArray();
        }
        $dataArray = [];
        if (! empty($data)) {
            foreach ($data as $row) {
                $rowArray = [];
                foreach ($row as $columnName => $columnValue) {
                    $rowArray[$columnName] = $columnValue;
                }
                $dataArray[] = $rowArray;
            }
        }
        return $dataArray;
    }

    /**
     * Checks if a database table exists
     * @param string $table
     * @return boolean
     */
    public function hasTable(string $table): bool
    {
        return \Schema::connection($this->databaseName)->hasTable($table);
    }



    /**
     * Populate the place-holders in the seed stub.
     * @param string $stub
     * @param string $table
     * @param string $data
     * @param null $chunkSize
     * @param null $preRunEvent
     * @param null $postRunEvent
     * @param bool $indexed
     * @return string
     */
    public function populateStub(string $stub, string $table, string $data, $chunkSize = null, $preRunEvent = null, $postRunEvent = null, bool $indexed = true): string
    {
        $chunkSize = $chunkSize ?: config('bingo.config.chunk_size');

        $inserts = '';
        $chunks = array_chunk((array) $data, $chunkSize);
        foreach ($chunks as $chunk) {
            $this->addNewLines($inserts);
            $this->addIndent($inserts, 2);
            $inserts .= sprintf(
                "\DB::table('%s')->insert(%s);",
                $table,
                $this->prettifyArray($chunk, $indexed)
            );
        }

        $preRunEventInsert = '';
        if ($preRunEvent) {
            $preRunEventInsert .= "\$response = Event::until(new $preRunEvent());";
            $this->addNewLines($preRunEventInsert);
            $this->addIndent($preRunEventInsert, 2);
            $preRunEventInsert .= 'if ($response === false) {';
            $this->addNewLines($preRunEventInsert);
            $this->addIndent($preRunEventInsert, 3);
            $preRunEventInsert .= 'throw new Exception("Prerun event failed, seed wasn\'t executed!");';
            $this->addNewLines($preRunEventInsert);
            $this->addIndent($preRunEventInsert, 2);
            $preRunEventInsert .= '}';
        }

        $stub = str_replace(
            '{{preRun_event}}',
            $preRunEventInsert,
            $stub
        );

        if (! is_null($table)) {
            $stub = str_replace('{{table}}', $table, $stub);
        }

        $postRunEventInsert = '';
        if ($postRunEvent) {
            $postRunEventInsert .= "\$response = Event::until(new $postRunEvent());";
            $this->addNewLines($postRunEventInsert);
            $this->addIndent($postRunEventInsert, 2);
            $postRunEventInsert .= 'if ($response === false) {';
            $this->addNewLines($postRunEventInsert);
            $this->addIndent($postRunEventInsert, 3);
            $postRunEventInsert .= 'throw new Exception("Seed was executed but the postrun event failed!");';
            $this->addNewLines($postRunEventInsert);
            $this->addIndent($postRunEventInsert, 2);
            $postRunEventInsert .= '}';
        }

        $stub = str_replace(
            '{{postRun_event}}',
            $postRunEventInsert,
            $stub
        );

        return str_replace('{{insert_statements}}', $inserts, $stub);
    }

    /**
     * Create the full path name to the seed file.
     * @param string $name
     * @param string $path
     * @return string
     */
    public function getPath(string $name, string $path): string
    {
        return $path.'/'.$name.'.php';
    }

    /**
     * Prettify a var_export of an array
     * @param array $array
     * @param bool $indexed
     * @return string
     */
    protected function prettifyArray(array $array, bool $indexed = true): string
    {
        $content = ($indexed)
            ? var_export($array, true)
            : preg_replace("/[0-9]+ /i", '', var_export($array, true));

        $lines = explode("\n", $content);

        $inString = false;
        $tabCount = 3;
        for ($i = 1; $i < count($lines); $i++) {
            $lines[$i] = ltrim($lines[$i]);

            //Check for closing bracket
            if (str_contains($lines[$i], ')')) {
                $tabCount--;
            }

            //Insert tab count
            if ($inString === false) {
                for ($j = 0; $j < $tabCount; $j++) {
                    $lines[$i] = substr_replace($lines[$i], $this->indentCharacter, 0, 0);
                }
            }

            for ($j = 0; $j < strlen($lines[$i]); $j++) {
                //skip character right after an escape \
                if ($lines[$i][$j] == '\\') {
                    $j++;
                } //check string open/end
                elseif ($lines[$i][$j] == '\'') {
                    $inString = ! $inString;
                }
            }

            //check for opening bracket
            if (str_contains($lines[$i], '(')) {
                $tabCount++;
            }
        }

        return implode("\n", $lines);
    }

    /**
     * Adds new lines to the passed content variable reference.
     *
     * @param string $content
     * @param int $numberOfLines
     */
    private function addNewLines(string &$content, int $numberOfLines = 1): void
    {
        while ($numberOfLines > 0) {
            $content .= $this->newLineCharacter;
            $numberOfLines--;
        }
    }

    /**
     * Adds indentation to the passed content reference.
     *
     * @param string $content
     * @param int $numberOfIndents
     */
    private function addIndent(string &$content, int $numberOfIndents = 1): void
    {
        while ($numberOfIndents > 0) {
            $content .= $this->indentCharacter;
            $numberOfIndents--;
        }
    }


}
