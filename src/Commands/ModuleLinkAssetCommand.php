<?php

namespace Bingo\Commands;

use Bingo\Exceptions\BizException;
use Illuminate\Console\Command;
use Bingo\Core\Util\FileUtil;
use Bingo\Module\ModuleManager;

class ModuleLinkAssetCommand extends Command
{
    protected $signature = 'bingo:module:link-asset {module}';

    public function handle(): void
    {
        $module = $this->argument('module');

        BizException::throwsIf('模块不存在', ! ModuleManager::isExists($module));
        $linkFromRelative = ModuleManager::relativePath($module, 'Asset');
        $linkFrom = ModuleManager::path($module, 'Asset');
        $linkToRelative = "vendor/$module";
        $linkTo = public_path($linkToRelative);

        if (file_exists($linkTo)) {
            $this->error("The [$linkToRelative] link already exists.");
            return;
        }

        FileUtil::link($linkFrom, $linkTo);
        $this->info("The [$linkToRelative] link has been connected to [$linkFromRelative]");
    }

}
