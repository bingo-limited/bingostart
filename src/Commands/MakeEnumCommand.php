<?php

namespace Bingo\Commands;

use Illuminate\Support\Facades\File;

class MakeEnumCommand extends BingoCommand
{
    protected $signature = 'bingo:enum {name} {--type=int}';

    protected $description = 'Create a backed enum';

    protected string $type = 'Enum';

    protected function getStub(): string
    {
        $stubPath = dirname(__DIR__).DIRECTORY_SEPARATOR.'stubs'.DIRECTORY_SEPARATOR.'enum'.DIRECTORY_SEPARATOR.$this->option('type').'.stub';
        return File::exists($stubPath) ? $stubPath : dirname(__DIR__, 3)."/stubs/enum/{$this->option('type')}.stub";
    }

    protected function getDefaultNamespace($rootNamespace): string
    {
        return $rootNamespace.'\Enums';
    }
}
