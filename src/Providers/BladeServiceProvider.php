<?php

namespace Bingo\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Blade;
use Bingo\Core\Services\ShortcodeService;

/**
 * BladeServiceProvider 服务提供者，用于注册和配置 Blade 扩展。
 */
class BladeServiceProvider extends ServiceProvider
{
    /**
     * 启动 Blade 扩展服务。
     *
     * @return void
     */
    public function boot(): void
    {
        Blade::directive('shortcode', function ($expression) {
            return "<?php echo app('".ShortcodeService::class."')->parse($expression); ?>";
        });
    }

    public function register(): void
    {
    }
}
