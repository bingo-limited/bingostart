<?php

namespace Bingo\Providers;

use Bingo\App\Core\CurrentApp;
use Bingo\BingoStart;
use Bingo\Contracts\BingoPackage;
use Bingo\Core\Config\SettingsManager;
use Bingo\Core\Mail\MailsManager;
use Bingo\Core\Permission\PermissionManager;
use Bingo\Core\Services\NavigationService;
use Bingo\Core\Widgets\WidgetManager;
use Illuminate\Contracts\Container\BindingResolutionException;
use Illuminate\Support\Facades\Event;
use Psr\Container\ContainerExceptionInterface;
use Psr\Container\NotFoundExceptionInterface;
use Symfony\Component\Finder\Finder;
use Modules\Iam\Models\IamApplications;
use Illuminate\Support\ServiceProvider;
use Bingo\Core\Job\JobManager;

/**
 * Bingo模組服務提供者抽象類
 */
abstract class BingoModuleServiceProvider extends ServiceProvider implements BingoPackage
{
    /**
     * @var array<string, string> 事件監聽器陣列
     */
    protected array $events = [];

    /**
     * 註冊服務提供者
     *
     * @return void
     * @throws NotFoundExceptionInterface
     * @throws ContainerExceptionInterface
     */
    public function register(): void
    {
        foreach ($this->events as $event => $listener) {
            Event::listen($event, $listener);
        }

        $this->app->singleton('mail.template', function ($app) {
            return new MailsManager();
        });

        $this->loadMiddlewares();
        $this->loadModuleRoute();
        $this->loadConfig();

        $this->app->singleton('job.manager', function ($app) {
            return new JobManager();
        });
    }

    /**
     * 載入中間件
     *
     * @throws ContainerExceptionInterface
     * @throws NotFoundExceptionInterface
     */
    protected function loadMiddlewares(): void
    {
        if (!empty($middlewares = $this->middlewares())) {
            $route = $this->app['config']->get('bingo.route', [
                'middlewares' => []
            ]);

            $route['middlewares'] = array_merge($route['middlewares'], $middlewares);

            $this->app['config']->set('bingo.route', $route);
        }
    }

    /**
     * 載入模組配置
     */
    protected function loadConfig(): void
    {
        if (!is_dir($configPath = $this->configPath())) {
            return;
        }

        $files = [];
        foreach (Finder::create()->files()->name('*.php')->in($configPath) as $file) {
            $files[str_replace('.php', '', $file->getBasename())] = $file->getRealPath();
        }

        foreach ($files as $name => $file) {
            $this->app->make('config')->set(sprintf('%s.%s', $this->moduleName(), $name), require $file);
        }
    }

    /**
     * 獲取中間件列表
     *
     * @return array<string>
     */
    protected function middlewares(): array
    {
        return [];
    }

    /**
     * 載入模組路由
     *
     * @throws ContainerExceptionInterface
     * @throws NotFoundExceptionInterface
     */
    protected function loadModuleRoute(): void
    {
        $routes = $this->app['config']->get('bingo.module.routes', []);
        $lists = CurrentApp::getList();
        foreach ($lists as $apiType) {
            if (file_exists($route = BingoStart::getModuleRoutePathMultiple($this->moduleName(), $apiType))) {
                $routes[] = ["type" => $apiType, "route" => $route];
            }

            if (file_exists($route = BingoStart::getAppRoutePath($apiType))) {
                $routes[] = ["type" => $apiType, "route" => $route];
            }
        }
        $this->app['config']->set('bingo.module.routes', $routes);
    }

    /**
     * 註冊 WidgetManager 並載入小部件
     */
    protected function registerWidgetManager(): void
    {
        $widgets = $this->registerReportWidgets();
        $widgetManager = $this->app->make(WidgetManager::class);
        foreach ($widgets as $widgetClass => $widgetInfo) {
            $widgetManager->registerWidget($widgetClass, $widgetInfo);
        }
    }

    /**
     * 註冊報表小部件 (在子類中覆蓋)
     *
     * @return array<string, mixed>
     */
    public function registerReportWidgets(): array
    {
        return [];
    }

    /**
     * 獲取模組名稱
     *
     * @return string|array<string>
     */
    abstract protected function moduleName(): string|array;

    /**
     * 註冊設置項
     *
     * @return array<string, mixed>
     */
    abstract public function registerSettings(): array;

    /**
     * 獲取模組配置路徑
     *
     * @return string
     */
    protected function configPath(): string
    {
        return BingoStart::getModulePath($this->moduleName()) . 'config' . DIRECTORY_SEPARATOR;
    }

    /**
     * 註冊導航項
     *
     * @return array<string, mixed>
     * @throws BindingResolutionException
     */
    public function registerNavigation(): array
    {
        $navigationService = $this->app->make(NavigationService::class);
        $navigationItems = $this->navigation();
        $navigationService->registerNavigation($navigationItems);
        return [];
    }

    /**
     * 註冊模組設置
     */
    private function registerModuleSettings(): void
    {
        $settingsManager = app(SettingsManager::class);
        $settingsManager->registerSettingItems($this->moduleName(), $this->registerSettings());
    }

    /**
     * 註冊模組權限
     */
    protected function registerModulePermissions(): void
    {
        $permissions = $this->registerPermissions();
        $permissionManager = $this->app->make(PermissionManager::class);
        $applications = IamApplications::getAllCached();  // 使用缓存版本

        foreach ($applications as $application) {
            if (isset($permissions[$application->app_code])) {
                $permissionManager->registerPermissions($this->moduleName(), $permissions[$application->app_code], $application->app_code);
            }
        }
    }

    /**
     * 獲取導航項
     *
     * @return array<string, mixed>
     */
    protected function navigation(): array
    {
        return [];
    }

    /**
     * 註冊組件
     *
     * @return array<string, mixed>
     */
    public function registerComponents(): array
    {
        return [];
    }

    /**
     * 註冊頁面片段
     *
     * @return array<string, mixed>
     */
    public function registerPageSnippets(): array
    {
        return [];
    }

    /**
     * 註冊內容字段
     *
     * @return array<string, mixed>
     */
    public function registerContentFields(): array
    {
        return [];
    }

    /**
     * 註冊權限
     *
     * @return array<string, mixed>
     */
    public function registerPermissions(): array
    {
        return [];
    }

    /**
     * 註冊表單小部件
     *
     * @return array<string, mixed>
     */
    public function registerFormWidgets(): array
    {
        return [];
    }

    /**
     * 註冊過濾器小部件
     *
     * @return array<string, mixed>
     */
    public function registerFilterWidgets(): array
    {
        return [];
    }

    /**
     * 註冊列表列類型
     *
     * @return array<string, mixed>
     */
    public function registerListColumnTypes(): array
    {
        return [];
    }

    /**
     * 註冊郵件模板到管理器
     */
    protected function registerModuleMailTemplates(): void
    {
        $templates = $this->registerMailTemplates();
        if (!empty($templates)) {
            $mailsManager = $this->app->make(MailsManager::class);
            $mailsManager->loadRegisteredTemplates();
        }
    }

    /**
     * 註冊郵件模板
     *
     * @return array<string, mixed>
     */
    public function registerMailTemplates(): array
    {
        return [];
    }

    /**
     * 注册调度任务
     *
     * @return array<string, array>
     */
    public function registerSchedule(): array
    {
        return [];
    }

    /**
     * 註冊一個新的控制台（artisan）命令
     *
     * @param string $key
     * @param string $class
     */
    protected function registerConsoleCommand(string $key, string $class): void
    {
        if (!$this->app->runningInConsole()) {
            return;
        }
        $key = 'command.' . $key;

        $this->app->singleton($key, function () use ($class) {
            return $this->app->make($class);
        });

        $this->commands($key);
    }

    /**
     * 获取模板内容
     */
    protected function getTemplateContent(string $view): string
    {
        try {
            $viewPath = BingoStart::getModulePath($this->moduleName()) . 'views/' . str_replace('.', '/', $view) . '.blade.php';
            if (!file_exists($viewPath)) {
                \Log::error("View file does not exist: [{$viewPath}]");
                return '';
            }

            // 提供默认的宏变量值
            $defaultData = [
                'user_email' => 'user@example.com',
                'user_name' => 'User Name',
                'app_name' => config('app.name') ?: 'BWMS',
                'client_name' => config('client.name') ?: 'Client',
                'code' => '123456',
                'password' => 'example_password',
                'link' => 'https://example.com/reset-password',
                'title' => '邮件标题',
                'message' => '邮件内容',
                'changed_at' => now()->format('Y-m-d H:i:s'),
                'expiry_date' => now()->addDays(7)->format('Y-m-d'),
                'invitation_link' => 'https://example.com/invitation',
                'birth_date' => now()->format('Y-m-d'),
                'footer' => '如有任何问题，请随时联系我们。'
            ];

            return view()->file($viewPath)
                ->with($defaultData)
                ->render();

        } catch (\Exception $e) {
            \Log::error("Failed to get template content: " . $e->getMessage(), [
                'view' => $view,
                'module' => $this->moduleName(),
                'viewPath' => $viewPath ?? 'not set',
                'exception' => $e
            ]);
            return '';
        }
    }

    /**
     * 注册模块计划任务
     */
    protected function registerModuleSchedule(): void
    {
        $jobs = $this->registerSchedule();
        if (!empty($jobs)) {
            $jobManager = app('job.manager');
            $jobManager->registerJobs($this->moduleName(), $jobs);
            $jobManager->loadRegisteredJobs();
        }
    }

}
