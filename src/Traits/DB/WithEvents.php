<?php

declare(strict_types=1);

namespace Bingo\Traits\DB;

use Closure;

/**
 * base operate
 */
trait WithEvents
{
    protected ?Closure $beforeGetList = null;


    protected ?Closure $afterFirstBy = null;

    /**
     *
     * @param Closure $closure
     * @return $this
     */
    public function setBeforeGetList(Closure $closure): static
    {
        $this->beforeGetList = $closure;

        return $this;
    }

    /**
     *
     * @param Closure $closure
     * @return $this
     */
    public function setAfterFirstBy(Closure $closure): static
    {
        $this->afterFirstBy = $closure;

        return $this;
    }
}
