<?php

namespace Bingo\App\Admin\Model;

use Illuminate\Database\Eloquent\Model;

class Config extends Model
{
    protected $table = 'config';
}
