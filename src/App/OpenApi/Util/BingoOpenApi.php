<?php

namespace Bingo\App\OpenApi\Util;

use Bingo\Core\Input\Response;
use Bingo\Core\Util\CurlUtil;
use Bingo\Core\Util\SerializeUtil;
use Bingo\Core\Util\SignUtil;
use Bingo\Enums\Code;
use Bingo\Exceptions\BizException;
use Exception;
use Illuminate\Support\Facades\Log;

class BingoOpenApi
{
    /**
     * 接口基础路径
     * @var string
     */
    private string $apiBase;

    /**
     * 接口Key
     * @var string
     */
    private string $key;

    /**
     * 接口Secret
     * @var string
     */
    private string $secret;

    /**
     * 创建一个新的 BingoOpenApi 实例
     *
     * @param string $apiBase
     * @param string|null $key
     * @param string|null $secret
     * @return BingoOpenApi
     */
    public static function create(string $apiBase, ?string $key = null, ?string $secret = null): BingoOpenApi
    {
        $api = new BingoOpenApi();
        return $api
            ->setApiBase($apiBase)
            ->setKey($key)
            ->setSecret($secret);
    }

    /**
     * 设置 API 基础路径
     *
     * @param string $apiBase
     * @return static
     */
    public function setApiBase(string $apiBase): static
    {
        $this->apiBase = $apiBase;
        return $this;
    }

    /**
     * 设置 API Key
     *
     * @param string $key
     * @return static
     */
    public function setKey(string $key): static
    {
        $this->key = $key;
        return $this;
    }

    /**
     * 设置 API Secret
     *
     * @param string $secret
     * @return static
     */
    public function setSecret(string $secret): static
    {
        $this->secret = $secret;
        return $this;
    }

    /**
     * 发送 POST 请求
     *
     * @param string $url
     * @param array $data
     * @return array
     * @throws Exception
     */
    public function post(string $url, array $data): array
    {
        $timestamp = time();
        $params = [
            'key' => $this->key,
            'timestamp' => $timestamp,
        ];
        $params['sign'] = SignUtil::common($params, $this->secret);

        if (strpos($url, '?') !== false) {
            $fullUrl = $this->apiBase . $url . '&' . http_build_query($params);
        } else {
            $fullUrl = $this->apiBase . $url . '?' . http_build_query($params);
        }

        try {

            $result = CurlUtil::post($fullUrl, SerializeUtil::jsonEncode($data), [
                'header' => [
                    'Content-Type: application/json',
                    'Accept: application/json',
                ],
                'timeout' => 10,  // 增加超时时间到60秒
            ]);


            if (empty($result['body'])) {
                if (!empty($result['error'])) {
                    BizException::throws(Code::FAILED, 'CURL error: ' . $result['error']);
                } else {
                    BizException::throws(Code::FAILED, 'Empty response from server');
                }
            }

            $response = json_decode($result['body'], true);

            if (json_last_error() !== JSON_ERROR_NONE) {
                BizException::throws(Code::FAILED, 'Invalid JSON response: ' . json_last_error_msg());
            }

            if (!isset($response['code'])) {
                BizException::throws(Code::FAILED, 'Invalid API response structure');
            }

            if ($response['code'] !== 200) {  // 修改这里，因为成功的响应code是200
                BizException::throws(Code::FAILED, $response['message'] ?? 'Unknown error');
            }

            return $response['data'] ?? [];
        } catch (Exception $e) {
            Log::error('Failed to send POST request to BingoOpenApi', [
                'url' => $fullUrl,
                'exception' => $e,
            ]);

            BizException::throws(Code::FAILED, 'Failed to send POST request: ' . $e->getMessage());
        }
    }
}
