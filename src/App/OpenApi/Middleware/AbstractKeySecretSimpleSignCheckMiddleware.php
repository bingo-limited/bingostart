<?php

declare(strict_types=1);

namespace Bingo\App\OpenApi\Middleware;

use Closure;
use Illuminate\Http\Request;
use Bingo\Exceptions\BizException;
use Bingo\Enums\Code;
use Illuminate\Support\Facades\Log;
use Bingo\Core\Util\SignUtil;

/**
 * 抽象的密钥与签名检查中间件
 * 负责处理接口的基本加密验证逻辑
 */
abstract class AbstractKeySecretSimpleSignCheckMiddleware
{
    /**
     * 获取密钥
     *
     * @param string $key 应用ID
     * @return string|null 返回对应的密钥，或为空
     */
    abstract protected function getSecretByKey(string $key): ?string;

    /**
     * 中间件处理方法
     *
     * @param Request $request
     * @param Closure $next
     * @return mixed
     * @throws BizException 如果验证失败
     */
    public function handle(Request $request, Closure $next): mixed
    {
        $key = $request->query('key');
        $sign = $request->query('sign');
        $timestamp = $request->query('timestamp');

        if (empty($key)) {
            BizException::throws(Code::FAILED, 'Key is empty');
        }

        $secret = $this->getSecretByKey($key);
        if (empty($secret)) {
            BizException::throws(Code::FAILED, 'Invalid key');
        }

        if (empty($timestamp)) {
            BizException::throws(Code::FAILED, 'Timestamp is empty');
        }

        $currentTimestamp = time();
        if (abs($currentTimestamp - (int)$timestamp) > 1800) {
            BizException::throws(Code::FAILED, 'Timestamp not valid');
        }

        if (empty($sign)) {
            BizException::throws(Code::FAILED, 'Sign is empty');
        }

        $params = [
            'timestamp' => $timestamp,
            'key' => $key,
        ];

        if (!SignUtil::check($sign, $params, $secret)) {
            Log::info('Bingo.SignatureMismatch', ['provided' => $sign]);
            BizException::throws(Code::FAILED, 'Signature mismatch');
        }

        return $next($request);
    }
}
