<?php

declare(strict_types=1);

namespace Bingo\App\OpenApi\Middleware;

use Modules\Open\Services\AppCredentialService;

/**
 * 具体的密钥与签名检查中间件
 * 处理特定接口的加密验证逻辑
 */
class ApiKeySecretSignCheckMiddleware extends AbstractKeySecretSimpleSignCheckMiddleware
{
    public function __construct(
        private readonly AppCredentialService $appCredentialService
    ) {

    }

    /**
     * 根据应用ID获取密钥
     *
     * @param string $key 应用ID
     * @return string|null 返回对应的密钥，或为空
     */
    protected function getSecretByKey(string $key): ?string
    {
        $credential = $this->appCredentialService->getByAppId($key);
        return $credential?->app_secret;
    }
}
