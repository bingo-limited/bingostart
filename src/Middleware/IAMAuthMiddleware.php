<?php

namespace Bingo\Middleware;

use Bingo\Enums\Code;
use Bingo\Events\User as UserEvent;
use Bingo\Exceptions\FailedException;
use Closure;
use Exception;
use Illuminate\Auth\AuthenticationException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Event;
use Throwable;
use Modules\Iam\Services\TokenService;
use Modules\Iam\Enums\TokenType;
use Modules\Iam\Models\IamUsers;

class IAMAuthMiddleware
{
    protected $tokenService;

    public function __construct(TokenService $tokenService)
    {
        $this->tokenService = $tokenService;
    }

    /**
     * @throws FailedException
     */
    public function handle(Request $request, Closure $next)
    {
        try {
            // 获取 URL 查询参数中的 token
            $token = $request->query('token');

            if ($token) {
                // 将 token 加入到请求的 Authorization 头中
                $request->headers->set('Authorization', 'Bearer '.$token);
            }

            // 尝试使用 iam_web guard（会话认证）
            if (Auth::guard('iam_web')->check()) {
                $user = Auth::guard('iam_web')->user();
            } elseif (Auth::guard('iam')->check()) {
                $user = Auth::guard('iam')->user();
            }
            // 如果会话认证失败，尝试使用自定义的令牌验证
            else {
                $token = $request->bearerToken();
                if (!$token) {
                    throw new AuthenticationException('No token provided');
                }

                // 使用新的getUserByToken方法一次性获取token数据和用户信息
                $result = $this->tokenService->getUserByToken($token, TokenType::ACCESS);
                if (!$result['tokenData']) {
                    throw new AuthenticationException('Invalid token');
                }
                if (!$result['user']) {
                    throw new AuthenticationException('User not found');
                }

                $user = $result['user'];
                // 将用户设置到当前请求的认证中
                Auth::setUser($user);
            }

            if (!$user) {
                throw new AuthenticationException('User not authenticated');
            }

            // 将认证用户设置到请求中，以便后续使用
            $request->setUserResolver(function () use ($user) {
                return $user;
            });

            Event::dispatch(new UserEvent($user));

            return $next($request);
        } catch (AuthenticationException $e) {
            throw new FailedException(Code::LOST_LOGIN->description('code').": {$e->getMessage()}", Code::LOST_LOGIN);
        } catch (Exception|Throwable $e) {
            throw new FailedException(Code::LOST_LOGIN->description('code').": {$e->getMessage()}", Code::LOST_LOGIN);
        }
    }
}