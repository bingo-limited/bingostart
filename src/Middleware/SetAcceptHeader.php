<?php

namespace Bingo\Middleware;

use Closure;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Str;

class SetAcceptHeader
{
    /**
     * Handle an incoming request.
     *
     * @param Request $request
     * @param Closure $next
     * @param string $type
     * @return Response
     */
    public function handle(Request $request, Closure $next, string $type = 'json'): Response
    {
        Str::contains($request->header('Accept'), $contentType = "application/$type") or
        $request->headers->set('Accept', $contentType);

        return $next($request);
    }
}
