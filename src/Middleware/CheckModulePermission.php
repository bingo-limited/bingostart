<?php

namespace Bingo\Middleware;

use Bingo\Core\Permission\PermissionManager;
use Bingo\Enums\Code;
use Bingo\Exceptions\FailedException;
use Closure;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Modules\Iam\Services\ApplicationService;
use Throwable;

class CheckModulePermission
{
    /**
     * 權限管理器
     *
     * @var PermissionManager
     */
    protected PermissionManager $permissionManager;

    /**
     * 應用服務
     *
     * @var ApplicationService
     */
    protected ApplicationService $applicationService;

    /**
     * 建構函數
     *
     * @param PermissionManager $permissionManager
     * @param ApplicationService $applicationService
     */
    public function __construct(PermissionManager $permissionManager, ApplicationService $applicationService)
    {
        $this->permissionManager = $permissionManager;
        $this->applicationService = $applicationService;
    }

    /**
     * 處理傳入的請求
     *
     * @param Request $request
     * @param Closure $next
     * @return mixed
     * @throws FailedException
     */
    public function handle(Request $request, Closure $next): mixed
    {
        try {
            if (!Auth::check()) {
                throw new FailedException(Code::LOST_LOGIN->description('code'), Code::LOST_LOGIN);
            }

            $user = Auth::user();
            $appId = $this->applicationService->getAppIdFromRequest($request);
            $appCode = $this->applicationService->getAppCodeFromAppId($appId);

            if (!$appCode) {
                throw new FailedException(Code::PERMISSION_FORBIDDEN->description('code'), Code::PERMISSION_FORBIDDEN);
            }

            $route = $request->route();
            $action = $route->getAction();
            $controller = class_basename($action['controller']);
            $method = $action['method'] ?? null;

            if ($controller && $method) {
                $permissionMark = str_replace('Controller', '', $controller) . '@' . $method;

                if (!$this->checkPermission($user, $permissionMark, $appCode, $appId)) {
                    throw new FailedException(Code::PERMISSION_FORBIDDEN->description('code'), Code::PERMISSION_FORBIDDEN);
                }
            }

            return $next($request);
        } catch (Exception|Throwable $e) {
            if ($e instanceof FailedException) {
                throw $e;
            }
            throw new FailedException(Code::PERMISSION_FORBIDDEN->description('code') . ": {$e->getMessage()}", Code::PERMISSION_FORBIDDEN);
        }
    }

    /**
     * 檢查用戶權限
     *
     * @param mixed $user 用戶對象
     * @param string $permissionMark 權限標記
     * @param string $appCode 應用代碼
     * @param string $appId 應用ID
     * @return bool
     */
    protected function checkPermission($user, string $permissionMark, string $appCode, string $appId): bool
    {
        if (!$this->permissionManager->hasPermission($permissionMark, $appCode)) {
            return true; // 如果權限未定義，默認允許訪問
        }

        return $user->hasPermissionForApplication($permissionMark, $appId);
    }
}