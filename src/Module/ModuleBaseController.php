<?php

namespace Bingo\Module;

use Illuminate\Routing\Controller;
use Bingo\Core\View\ModuleResponsiveViewTrait;

class ModuleBaseController extends Controller
{
    use ModuleResponsiveViewTrait;
}
