<?php

namespace Bingo\Module;

use Bingo\Core\Type\BaseType;

class ModuleStatus implements BaseType
{
    public const INSTALLED = 'installed';
    public const NOT_INSTALLED = 'notInstalled';

    public static function getList(): array
    {
        return [
            self::INSTALLED => T('Installed'),
            self::NOT_INSTALLED => T('Not Installed'),
        ];
    }


}
